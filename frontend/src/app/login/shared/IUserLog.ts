export interface IUserLog {
  Userlog_Id?;
  User_Id?;
  Username?;
  Appversionno?;
  Logintime?;
  Logouttime?;
  Logout_Type?;
  sessionid?;
  Macid?;
  Machinename?;
  currentdate?;
}
