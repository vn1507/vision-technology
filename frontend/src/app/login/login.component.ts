import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthLoginService } from '../auth-guard/auth-login.service';
import { ILogin } from './shared/ilogin';
import { IUserLog } from './shared/IUserLog';
import { environment } from '../../environments/environment';
import { ShowMessageService } from '../shared-service/show-message.service';
import { IUserDetails } from './shared/IUserDetails';
import { EncryptDecryptService } from '../shared-service/encrypt-decrypt.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public versionNo: any;
  public user: ILogin = {};

  constructor(private authLoginService: AuthLoginService, private router: Router,
    private showMessageService: ShowMessageService, private http: HttpClient) { }

  ngOnInit() {    
  }

  //Login User
  userLogin() {
    try {
      this.authLoginService.userLogin(this.user)
        .subscribe((res: any) => {
          localStorage.setItem('token', res.token); 
            this.router.navigate(['layout/dashboard']);
          // }
        },
          err => {
            this.showMessageService.Error("Login", "Invalid User");
          });
    }
    catch (err) {
      this.showMessageService.Error("userLogin", "Error while login" + err);
    }
  }

}
