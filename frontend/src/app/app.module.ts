import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import { LoginModule } from './login/login.module';
import { LayoutModule } from './layout/layout.module';

import { SharedModule } from './shared-module/shared.module';
import { SharedComponentsModule } from './shared-components/shared-components.module';

import { AppComponent } from './app.component';

import { AuthGuard } from './auth-guard/auth.guard';
import { AuthLoginService } from './auth-guard/auth-login.service';
import { TokenInterceptorService } from './auth-guard/token-interceptor.service';
import { DialogService } from 'primeng/dynamicdialog';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ShowMessageService } from './shared-service/show-message.service';




@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,    
    HttpClientModule,
    SharedModule,    
    AppRoutingModule,
    LoginModule,
    LayoutModule,
    SharedComponentsModule
  ],
  providers: [
    AuthGuard,
    AuthLoginService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
    DialogService, 
    ConfirmationService,
    MessageService,
    ShowMessageService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
