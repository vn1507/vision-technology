import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { SharedModule } from '../../shared-module/shared.module';
import { SharedComponentsModule } from '../../shared-components/shared-components.module';

import { DashboardComponent } from './dashboard/dashboard.component';



@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
    SharedComponentsModule
  ],
  declarations: [DashboardComponent],  
})
export class DashboardModule { }
