import { Component, OnInit } from '@angular/core';
import { LayoutComponent } from '../../../layout/layout.component';

import { IDashBoard } from './shared/IDashboard';
import { DashboardService } from './shared/dashboard.service'
import { ShowMessageService } from '../../../shared-service/show-message.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public DashboardTielCount: IDashBoard[];

  constructor(private layoutComponent: LayoutComponent, private dashboardService: DashboardService,
    private showMessageService: ShowMessageService) {
    this.layoutComponent.moduleName = "Dashboard";
   }

  ngOnInit(): void {
    this.getTotalCount();
  }

  //Method to get Dashboard tile list
  getTotalCount(){
    try{
      this.dashboardService.getCount().subscribe((res: any) => {        
        this.DashboardTielCount = res as IDashBoard[];
      });
    }
    catch (err) {
      this.showMessageService.Error("userLogin", "Error while login" + err);
    }
  }

}
