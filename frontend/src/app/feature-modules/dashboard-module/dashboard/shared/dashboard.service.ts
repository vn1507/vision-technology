import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IDashBoard } from './IDashboard';
import { environment } from '../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) { }

  //method to get count for customer, olt and spliter
  getCount():Observable<IDashBoard>{
    return this.http.get<IDashBoard>(environment.webAPIURL + '/api/Dashboard/getTotalData')
    .pipe(
      catchError((err: any) => {
        return throwError(err)
      })
    );
  }
}
