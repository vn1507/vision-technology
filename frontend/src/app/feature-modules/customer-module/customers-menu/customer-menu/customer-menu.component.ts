import { Component, OnInit } from '@angular/core';
import { LayoutComponent } from '../../../../layout/layout.component';

@Component({
  selector: 'app-customer-menu',
  templateUrl: './customer-menu.component.html',
  styleUrls: ['./customer-menu.component.scss']
})
export class CustomerMenuComponent implements OnInit {

  constructor(private layoutComponent: LayoutComponent) {
    this.layoutComponent.moduleName = "Customers"; }

  ngOnInit(): void {
  }

}
