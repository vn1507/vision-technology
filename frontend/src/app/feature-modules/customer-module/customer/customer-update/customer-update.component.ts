import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../shared/customer.service';
import { ICustomer } from '../shared/ICustomer';
import { CountryService } from '../../../admin-module/country/shared/country.service';
import { ICountry } from '../../../admin-module/country/shared/ICountry';
import { StateService } from '../../../admin-module/state/shared/state.service';
import { IState } from '../../../admin-module/state/shared/IState';
import { DistrictService } from '../../../admin-module/district/shared/district.service';
import { IDistrict } from '../../../admin-module/district/shared/IDistrict';
import { CityService } from '../../../admin-module/city/shared/city.service';
import { ICity } from '../../../admin-module/city/shared/ICity';
import { AreaService } from '../../../admin-module/area/shared/area.service';
import { IArea } from '../../../admin-module/area/shared/IArea';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-customer-update',
  templateUrl: './customer-update.component.html',
  styleUrls: ['./customer-update.component.scss']
})
export class CustomerUpdateComponent implements OnInit {

  public selectedCustomer: ICustomer = {};

  public customerTypeList = [];
  public selectedCustomerType:ICustomer = {};

  public usageCodeList = [];
  public selectedUsageCode:ICustomer = {};

  public titleList = [];
  public selectedTitle:ICustomer = {};

  public countryList: ICountry[];
  public selectedCountry: ICountry = {};

  public stateList: IState[];
  public selectedState: IState = {};
  
  public districtList: IDistrict[];
  public selectedDistrict: IDistrict = {};

  public cityList: ICity[];
  public selectedCity: ICity = {};

  public areaList: IArea[];
  public selectedArea: IArea = {};

  public disableState : boolean = true;
  public disableDistrict : boolean = true;
  public disableCity : boolean = true;
  public disableArea : boolean = true;

  public datePipe = new DatePipe('en-US');
  
  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, private config: DynamicDialogConfig, 
    private customerService: CustomerService, private countryService: CountryService, private stateService: StateService,
    private cityService: CityService, private districtService: DistrictService, private areaService: AreaService) { }

  ngOnInit(): void {
    this.selectedCustomer = this.config.data;
    this.selectedCustomer.registerDate = new Date(this.datePipe.transform(this.selectedCustomer.registerDate, 'dd-MMM-yyyy'));
    this.getCompanyType();
    this.getUsageCode();
    this.getTitle();
    this.getCountryNames();
    this.getStateNames();
    this.getDistrictNames();
    this.getCityNames();
    this.getAreaNames();    
  }

   //method to fetch customerType from service (Hardcoded)
   getCompanyType(){
    try{
      this.customerTypeList = this.customerService.customerType;
      this.selectedCustomerType.customerType = this.selectedCustomer.customerType;
    }
    catch(error){
      this.showMessageService.Error("Customer Type", 'Error while feteching data : ' + error);
    }
  }

  //method to fetch usageCode from service (Hardcoded)
  getUsageCode(){
    try{
      this.usageCodeList = this.customerService.usageCode;
      this.selectedUsageCode.usageCode = this.selectedCustomer.usageCode;
    }
    catch(error){
      this.showMessageService.Error("Usage Code", 'Error while feteching data : ' + error);
    }
  }

  //method to fetch title from service (Hardcoded)
  getTitle(){
    try{
      this.titleList = this.customerService.title;
      this.selectedTitle.title = this.selectedCustomer.title;
      
    }
    catch(error){
      this.showMessageService.Error("Title.", 'Error while feteching data : ' + error);
    }
  }

  // Validation of maxLength for number input field  
  onlyNumberKey(event) {
    if (event.target.value.length == 0 && event.which == 48) {
      return false;
    }
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 46 && event.charCode <= 57;
  }

  //method to fetch countryname
  getCountryNames(){
    try{
      this.countryService.getCountryList().subscribe((res: any) => {
        this.countryList = res as ICountry[];
        this.selectedCountry.countryId = this.selectedCustomer.countryId;
        this.selectedCountry.countryName = this.selectedCustomer.countryName;
      });
    }
    catch(error){
      this.showMessageService.Error("Country", 'Error while feteching data : ' + error);
    }
  }

  //method to fetch statename
  getStateNames(){
    try{
      this.stateService.getStateData().subscribe((res: any) => {
        this.stateList = res as IState[];
        this.selectedState.stateId = this.selectedCustomer.stateId;
        this.selectedState.stateName = this.selectedCustomer.stateName;
      });
    }
    catch(error){
      this.showMessageService.Error("State", 'Error while feteching data : ' + error);
    }
  }

  //method to fetch districtname
  getDistrictNames(){
    try{
      this.districtService.getDistrictData().subscribe((res: any) => {
        this.districtList = res as IDistrict[];
        this.selectedDistrict.districtId = this.selectedCustomer.districtId;
        this.selectedDistrict.districtName = this.selectedCustomer.districtName;

      });
    }
    catch(error){
      this.showMessageService.Error("District", 'Error while feteching data : ' + error);
    }
  }

  //method to fetch cityname
  getCityNames(){
    try{
      this.cityService.getCityData().subscribe((res: any) => {
        this.cityList = res as ICity[];
        this.selectedCity.cityId = this.selectedCustomer.cityId;
        this.selectedCity.cityName = this.selectedCustomer.cityName;

      });
    }
    catch(error){
      this.showMessageService.Error("City", 'Error while feteching data : ' + error);
    }
  }  

   //method to fetch cityname
   getAreaNames(){
    try{
      this.areaService.getAreaData().subscribe((res: any) => {
        this.areaList = res as IArea[];
        this.selectedArea.areaId = this.selectedCustomer.areaId;
        this.selectedArea.areaName = this.selectedCustomer.areaName;
      });
    }
    catch(error){
      this.showMessageService.Error("Area", 'Error while feteching data : ' + error);
    }
  }
  

  onCountryChange(countryId: number) {          
    if (countryId) {
      this.stateService.getStateListByCountryId(countryId).subscribe((res: any) => {
          this.stateList = res as IState[];
          this.disableState = false;
          this.districtList = null;
          this.cityList = null;    
        }
      );
    } else {
      this.disableState = true;
      this.stateList = null;
      this.districtList = null;
      this.cityList = null;      
    }
  }

  onStateChange(stateId: number) {    
    if (stateId) {
      this.districtService.getDistrictListByStateId(stateId).subscribe((res: any) => {        
        this.disableDistrict = false;
        this.districtList = res as IDistrict[];
        this.cityList = null;
      }
      );
    } else {
      this.disableDistrict = true;
      this.cityList = null;
    }
  }

  onDistrictChange(districtId: number) {    
    if (districtId) {
      this.cityService.getCityListByDistrictId(districtId).subscribe((res: any) => {                
        this.cityList = res as ICity[];
        this.disableCity = false;
      }
      );
    } else {
      this.cityList = null;   
      this.disableCity = true;   
    }
  }

  //method to enable city
  onCityChange(cityId: number) {    
    if (cityId) {
      this.areaService.getAreaListByCityId(cityId).subscribe((res: any) => {                
        this.areaList = res as IArea[];
        this.disableArea = false;
      }
      );
    } else {
      this.areaList = null;   
      this.disableArea = true;   
    }
  }
  
  //method to save data
  UpdateCustomer(){
    try{
      this.selectedCustomer.customerType = this.selectedCustomerType.customerType;
      this.selectedCustomer.usageCode = this.selectedUsageCode.usageCode;
      this.selectedCustomer.title = this.selectedTitle.title;
      this.selectedCustomer.countryId = this.selectedCountry.countryId;
      this.selectedCustomer.stateId = this.selectedState.stateId;
      this.selectedCustomer.districtId = this.selectedDistrict.districtId;
      this.selectedCustomer.cityId = this.selectedCity.cityId;
      this.selectedCustomer.areaId = this.selectedArea.areaId;
      this.selectedCustomer.status = 'enquiry';
      this.selectedCustomer.registerDate = new Date(this.datePipe.transform(this.selectedCustomer.registerDate, 'yyyy-MM-dd'));
      this.selectedCustomer.entryDate = new Date(this.datePipe.transform(this.selectedCustomer.entryDate, 'yyyy-MM-dd'));
      this.customerService.UpdateCustomer(this.selectedCustomer).subscribe((res: any) => {
        this.showMessageService.Success("Customer", res);
              this.selectedCustomer = {};
              this.ref.close(true);
      }, err => {
              if(err.status == 409){
                this.showMessageService.Error("Duplicate Customer" ,  "Customer already exists");  
              } else{
                this.showMessageService.Error("Customer Update", 'Error : ' + err.error);  
              }
      });
    }
    catch(error){
      this.showMessageService.Error("Customer", 'Error while adding data : ' + error);
    }
  }


}
