import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../shared/customer.service';
import { ICustomer } from '../shared/ICustomer';
import { CountryService } from '../../../admin-module/country/shared/country.service';
import { ICountry } from '../../../admin-module/country/shared/ICountry';
import { StateService } from '../../../admin-module/state/shared/state.service';
import { IState } from '../../../admin-module/state/shared/IState';
import { DistrictService } from '../../../admin-module/district/shared/district.service';
import { IDistrict } from '../../../admin-module/district/shared/IDistrict';
import { CityService } from '../../../admin-module/city/shared/city.service';
import { ICity } from '../../../admin-module/city/shared/ICity';
import { IArea } from '../../../admin-module/area/shared/IArea';
import { AreaService } from '../../../admin-module/area/shared/area.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-customer-save',
  templateUrl: './customer-save.component.html',
  styleUrls: ['./customer-save.component.scss']
})
export class CustomerSaveComponent implements OnInit {

  public customer: ICustomer = {};

  public customerTypeList = [];
  public selectedCustomerType:ICustomer = {};

  public usageCodeList = [];
  public selectedUsageCode:ICustomer = {};

  public titleList = [];
  public selectedTitle:ICustomer = {};

  public countryList: ICountry[];
  public selectedCountry: ICountry = {};

  public stateList: IState[];
  public selectedState: IState = {};
  
  public districtList: IDistrict[];
  public selectedDistrict: IDistrict = {};

  public cityList: ICity[];
  public selectedCity: ICity = {};

  public areaList: IArea[];
  public selectedArea: IArea = {};

  public disableState : boolean = true;
  public disableDistrict : boolean = true;
  public disableCity : boolean = true;
  public disableArea : boolean = true;

  public datePipe = new DatePipe('en-US');
  
  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, 
    private customerService: CustomerService, private countryService: CountryService, private stateService: StateService,
    private cityService: CityService, private districtService: DistrictService, private areaService: AreaService) { }

  ngOnInit(): void {
    this.getCompanyType();
    this.getUsageCode();
    this.getTitle();
    this.getCountryNames();
    this.getStateNames();
    this.getDistrictNames();
    this.getCityNames();
    this.getAreaNames();
  }

  // Validation of maxLength for number input field  
  onlyNumberKey(event) {
    if (event.target.value.length == 0 && event.which == 48) {
      return false;
    }
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 46 && event.charCode <= 57;
  }

  //method to fetch customerType from service (Hardcoded)
  getCompanyType(){
    try{
      this.customerTypeList = this.customerService.customerType;
    }
    catch(error){
      this.showMessageService.Error("Customer Type", 'Error while feteching data : ' + error);
    }
  }

  //method to fetch usageCode from service (Hardcoded)
  getUsageCode(){
    try{
      this.usageCodeList = this.customerService.usageCode;
    }
    catch(error){
      this.showMessageService.Error("Usage Code", 'Error while feteching data : ' + error);
    }
  }

  //method to fetch title from service (Hardcoded)
  getTitle(){
    try{
      this.titleList = this.customerService.title;
    }
    catch(error){
      this.showMessageService.Error("Title.", 'Error while feteching data : ' + error);
    }
  }

  //method to fetch countryname
  getCountryNames(){
    try{
      this.countryService.getCountryList().subscribe((res: any) => {
        this.countryList = res as ICountry[];
      });
    }
    catch(error){
      this.showMessageService.Error("Country", 'Error while feteching data : ' + error);
    }
  }

  //method to fetch statename
  getStateNames(){
    try{
      this.stateService.getStateData().subscribe((res: any) => {
        this.stateList = res as IState[];
      });
    }
    catch(error){
      this.showMessageService.Error("State", 'Error while feteching data : ' + error);
    }
  }

  //method to fetch districtname
  getDistrictNames(){
    try{
      this.districtService.getDistrictData().subscribe((res: any) => {
        this.districtList = res as IDistrict[];
      });
    }
    catch(error){
      this.showMessageService.Error("District", 'Error while feteching data : ' + error);
    }
  }

  //method to fetch cityname
  getCityNames(){
    try{
      this.cityService.getCityData().subscribe((res: any) => {
        this.cityList = res as ICity[];
      });
    }
    catch(error){
      this.showMessageService.Error("City", 'Error while feteching data : ' + error);
    }
  }

  //method to fetch cityname
  getAreaNames(){
    try{
      this.areaService.getAreaData().subscribe((res: any) => {
        this.areaList = res as IArea[];
      });
    }
    catch(error){
      this.showMessageService.Error("Area", 'Error while feteching data : ' + error);
    }
  }
  

  //method to enable state
  onCountryChange(countryId: number) {          
    if (countryId) {
      this.stateService.getStateListByCountryId(countryId).subscribe((res: any) => {
          this.stateList = res as IState[];
          this.disableState = false;
          this.districtList = null;
          this.cityList = null;    
          this.areaList = null;
        }
      );
    } else {
      this.disableState = true;
      this.stateList = null;
      this.districtList = null;
      this.cityList = null;    
      this.areaList = null;  
    }
  }

  //method to enable district
  onStateChange(stateId: number) {    
    if (stateId) {
      this.districtService.getDistrictListByStateId(stateId).subscribe((res: any) => {        
        this.disableDistrict = false;
        this.districtList = res as IDistrict[];
        this.cityList = null;
        this.areaList = null;
      }
      );
    } else {
      this.disableDistrict = true;
      this.cityList = null;
      this.areaList = null;
    }
  }

  //method to enable city
  onDistrictChange(districtId: number) {    
    if (districtId) {
      this.cityService.getCityListByDistrictId(districtId).subscribe((res: any) => {                
        this.cityList = res as ICity[];
        this.disableCity = false;
        this.areaList = null;
      }
      );
    } else {
      this.cityList = null;   
      this.disableCity = true;
      this.areaList = null;   
    }
  }

  //method to enable city
  onCityChange(cityId: number) {    
    if (cityId) {
      this.areaService.getAreaListByCityId(cityId).subscribe((res: any) => {                
        this.areaList = res as IArea[];
        this.disableArea = false;
      }
      );
    } else {
      this.areaList = null;   
      this.disableArea = true;   
    }
  }
  
  //method to save data
  SaveCustomer(){
    try{      
      this.customer.customerType = this.selectedCustomerType.customerType;
      this.customer.usageCode = this.selectedUsageCode.usageCode;
      this.customer.title = this.selectedTitle.title;
      this.customer.countryId = this.selectedCountry.countryId;
      this.customer.stateId = this.selectedState.stateId;
      this.customer.districtId = this.selectedDistrict.districtId;
      this.customer.cityId = this.selectedCity.cityId;
      this.customer.areaId = this.selectedArea.areaId;
      this.customer.status = 'enquiry';
      this.customer.registerDate = new Date(this.datePipe.transform(this.customer.registerDate, 'yyyy-MM-dd'));
      this.customer.entryDate = new Date(this.datePipe.transform(this.customer.entryDate, 'yyyy-MM-dd'));
      this.customerService.InsertCustomer(this.customer).subscribe((res: any) => {
        this.showMessageService.Success("Customer", res);
              this.customer = {};
              this.ref.close(true);
      }, err => {
              if(err.status == 409){
                this.showMessageService.Error("Duplicate Customer" ,  "Customer already exists");  
              } else{
                this.showMessageService.Error("Customer Save", 'Error : ' + JSON.stringify(err.error));  
              }
      });
    }
    catch(error){
      this.showMessageService.Error("Customer", 'Error while adding data : ' + error);
    }
  }

}
