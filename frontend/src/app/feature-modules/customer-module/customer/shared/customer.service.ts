import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICustomer } from '../shared/ICustomer';
import { environment } from '../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  public customerType: any = [{customerType:'CORPORATE'},{customerType:'INDIVIDUAL'}];
  public usageCode: any = [{usageCode:'BUSINESS'},{usageCode:'RESIDENTIAL'},{usageCode:'As per CA Data'}];
  public title: any = [{title:'MISS'},{title:'MR'},{title:'MS'},{title:'MR AND MRS'},{title:'DR'},{title:'M/S'},{title:'COMPANY'},{title:'As per CA Data'}];

  constructor(private http: HttpClient) { }

  //Method to get response
  getCustomerList(): Observable<ICustomer> {
    return this.http.get<ICustomer>(environment.webAPIURL + '/api/Customer/getCustomerList')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }  

  //Method to get customer nams and id
  getCustomerData(): Observable<ICustomer> {
    return this.http.get<ICustomer>(environment.webAPIURL + '/api/Customer/getCustomerData')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to add
  InsertCustomer(customer: ICustomer): Observable<ICustomer> {
    return this.http.post<ICustomer>(environment.webAPIURL + '/api/Customer/postCustomer', customer)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to update
  UpdateCustomer(selectedCustomer: ICustomer): Observable<ICustomer> {
    return this.http.post<ICustomer>(environment.webAPIURL + '/api/Customer/updateCustomer', selectedCustomer)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

}
