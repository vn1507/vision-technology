export interface ICustomer {
    customerId?;
    customerType ?;
    usageCode ?;
    title ?;

    customerName?;
    firstName?;
    middleName?;
    lastName?;
    mobileNo1?;
    emailId?;
    fullAddress?;
    countryId?;
    countryName?;

    stateId?;
    stateName?;
    districtId?;
    districtName?;
    cityId?;
    cityName?;
    areaId ?;
    areaName ?;    
    registerDate ?;
    entryDate?;
    status?;
}