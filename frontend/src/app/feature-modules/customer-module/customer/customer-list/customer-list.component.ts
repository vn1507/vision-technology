import { Component, OnInit } from '@angular/core';

import { ICustomer } from '../shared/ICustomer';
import { CustomerService } from '../shared/customer.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { PrimengService } from '../../../../shared-service/primeng.service';
import { CustomerSaveComponent } from '../customer-save/customer-save.component';
import { CustomerUpdateComponent } from '../customer-update/customer-update.component';


@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss'],
  providers: [PrimengService],
  entryComponents: [CustomerSaveComponent, CustomerUpdateComponent]
})
export class CustomerListComponent implements OnInit {

  public customerList: ICustomer[];
  public columns: any[];
  public loading: boolean;
  public status: any;

  constructor(private showMessageService: ShowMessageService, 
    private primengService: PrimengService, private customerService: CustomerService) {
    this.columns = [
      { field: 'firstName', header: 'First Name' },
      { field: 'mobileNo1', header: 'Mobile No.' },
      { field: 'emailId', header: 'Email ID' },
      { field: 'countryName', header: 'Country' },
      { field: 'stateName', header: 'State' },     
      { field: 'districtName', header: 'District' },     
      { field: 'cityName', header: 'Locality' },     
      { field: 'areaName', header: 'Sub Locality' },     
      { field: 'registerDate', header: 'Booked Date' }, 
      { field: 'entryDate', header: 'Entry Date' }, 
      { field: 'status', header: 'Status' },     
      { field: 'customerId', header: '' },
    ];
   }

  ngOnInit(): void {
    this.getCustomerList();
  }

  //method to get data
  getCustomerList(){
    try{
      this.loading = true;
      this.customerService.getCustomerList().subscribe((res : any) => {
        this.customerList = res as ICustomer[];
        this.loading = false;        
      });
    }
    catch(error){
      this.showMessageService.Error("Customer", 'Error while getting data : ' + error);
    }
  }

  //method to open add dailog box
  AddDialog() {
    try {

      const ref = this.primengService.dialogService.open(CustomerSaveComponent, {
        header: 'Add Customer',
        width: '40%',
        contentStyle: {"overflow": "visible"}        
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getCustomerList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Customer", 'Error while opening add dialog Customer : ' + error);
    }
  }

  //method to open update dailog box
  UpdateDialog(customer: ICustomer) {
    try {
      let selectedCustomer = {
        customerId: customer.customerId,
        customerType: customer.customerType,
        usageCode: customer.usageCode,
        title: customer.title,
        firstName: customer.firstName,
        middleName: customer.middleName,
        lastName: customer.lastName,
        mobileNo1: customer.mobileNo1,
        emailId: customer.emailId,
        fullAddress: customer.fullAddress,
        countryId: customer.countryId,
        countryName: customer.countryName,
        stateId: customer.stateId,
        stateName: customer.stateName,
        cityId: customer.cityId,
        cityName: customer.cityName,
        areaId: customer.areaId,
        areaName: customer.areaName,
        districtId: customer.districtId,
        districtName: customer.districtName,
        registerDate: customer.registerDate,
        status: customer.status,
      };      

      const ref = this.primengService.dialogService.open(CustomerUpdateComponent, {
        header: 'Update Customer',
        data: selectedCustomer,
        width: '40%',
        contentStyle: {"overflow": "visible"}
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getCustomerList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Customer", 'Error while opening update dialog Customer : ' + error);
    }
  }

  //method to open delete dailog box
  DeleteDialog(customerId: number){

  }  

}
