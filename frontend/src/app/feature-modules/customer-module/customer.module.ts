import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerRoutingModule } from './customer-routing.module';
import { SharedModule } from '../../shared-module/shared.module';
import { SharedComponentsModule } from '../../shared-components/shared-components.module';

import { CustomerListComponent } from './customer/customer-list/customer-list.component';
import { CustomerSaveComponent } from './customer/customer-save/customer-save.component';
import { CustomerUpdateComponent } from './customer/customer-update/customer-update.component';
import { CustomerMenuComponent } from './customers-menu/customer-menu/customer-menu.component';
import { CustomerWithParameterComponent } from './customer-with-parameter/customer-with-parameter/customer-with-parameter.component';
import { ServeyAddComponent } from './customer-with-parameter/servey/servey-add/servey-add.component';
import { FormProcessAddComponent } from './customer-with-parameter/form-process/form-process-add/form-process-add.component';
import { IdGenerateAddComponent } from './customer-with-parameter/id-generate/id-generate-add/id-generate-add.component';


@NgModule({  
  imports: [
    CommonModule,
    CustomerRoutingModule,
    SharedModule,
    SharedComponentsModule
  ],
  declarations: [    
  CustomerListComponent,    
  CustomerSaveComponent,    
  CustomerUpdateComponent, CustomerMenuComponent, CustomerWithParameterComponent, ServeyAddComponent, FormProcessAddComponent, IdGenerateAddComponent
]
})
export class CustomerModule { }
