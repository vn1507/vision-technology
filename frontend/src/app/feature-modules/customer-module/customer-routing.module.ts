import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerMenuComponent } from './customers-menu/customer-menu/customer-menu.component';

import { CustomerListComponent } from './customer/customer-list/customer-list.component';
import { CustomerWithParameterComponent } from './customer-with-parameter/customer-with-parameter/customer-with-parameter.component';

const routes: Routes = [
  {
    path: '', component: CustomerMenuComponent,
    children: [
      {path:'', redirectTo:'customer', pathMatch: 'full'},
      {path: 'customer', component: CustomerListComponent},
      {path: 'customer/:value', component: CustomerWithParameterComponent},
      {path: 'customer/:value', component: CustomerWithParameterComponent},
      {path: 'customer/:value', component: CustomerWithParameterComponent},
      {path: 'customer/:value', component: CustomerWithParameterComponent},
      {path: 'customer/:value', component: CustomerWithParameterComponent},
      {path: 'customer/:value', component: CustomerWithParameterComponent},
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }
