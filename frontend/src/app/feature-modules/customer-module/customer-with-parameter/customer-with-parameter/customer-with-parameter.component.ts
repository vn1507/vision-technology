import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { CustomerWithParameterService } from '../shared/customer-with-parameter.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { PrimengService } from '../../../../shared-service/primeng.service';

import { ServeyAddComponent } from '../servey/servey-add/servey-add.component';
import { FormProcessAddComponent } from '../form-process/form-process-add/form-process-add.component';
import { IdGenerateAddComponent } from '../id-generate/id-generate-add/id-generate-add.component';
import { IServey } from '../servey/shared/IServey';


@Component({
  selector: 'app-customer-with-parameter',
  templateUrl: './customer-with-parameter.component.html',
  styleUrls: ['./customer-with-parameter.component.scss'],
  providers: [PrimengService],
  entryComponents: [ServeyAddComponent]
})
export class CustomerWithParameterComponent implements OnInit {

  public selectedParametercustomerList: any[];
  public columns: any[];
  public loading: boolean;

  public getValue: any;
  public setValue: any;
  public status: any;

  constructor(private customerWithParameterService: CustomerWithParameterService, private activatedRoute: ActivatedRoute,
    private primengService: PrimengService, private showMessageService: ShowMessageService) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.getValue = params;
      this.setValue = this.getValue.value;
      this.getCustomerListStatusWise(this.setValue);
    })
  }

  getCustomerListStatusWise(setValue: string) {
    try {

      this.customerWithParameterService.getCustomerListStatusWise(setValue).subscribe((res: any) => {
        switch (setValue) {
          case 'enquiry' || 'AssignToEmployee':
            this.columns = [
              { field: 'firstName', header: 'Name' },
              { field: 'entryDate', header: 'Entry Date' },
              { field: 'mobileNo1', header: 'Mobile No.' },              
              { field: 'cityName', header: 'Locality' },
              { field: 'areaName', header: 'Sub Locality' },
              { field: 'status', header: 'Status' },
            ];
            this.selectedParametercustomerList = res as any[];
            console.log('enquiry', this.selectedParametercustomerList);
            this.loading = false;
            break;
          case 'servey':
            this.columns = [
              { field: 'firstName', header: 'Customer Name' },
              { field: 'serveyDate', header: 'Servey Date' },
              { field: 'status', header: 'Status' }
            ];
            this.selectedParametercustomerList = res as any[];
            console.log('servey', this.selectedParametercustomerList);
            this.loading = false;
            break;
          case 'formProcess':
            this.columns = [
              { field: 'firstName', header: 'Customer Name' },
              { field: 'formProcessDate', header: 'Form Process Date' },
              { field: 'status', header: 'Status' }
            ];
            this.selectedParametercustomerList = res as any[];
            console.log('formProcess', this.selectedParametercustomerList);
            this.loading = false;
            break;
          case 'idGenerate':
            this.columns = [
              { field: 'firstName', header: 'Customer Name' },
              { field: 'oltName', header: 'OLT Name' },
              { field: 'spliterName', header: 'Spliter Name' },
              { field: 'assignDate', header: 'Assign Date' },
              { field: 'fiberNo', header: 'Fiber No.' },
              { field: 'ontType', header: 'ONT type' },
              { field: 'phoneNo', header: 'Phone No.' },
              { field: 'status', header: 'Status' },
            ];
            this.selectedParametercustomerList = res as any[];
            console.log('idGenerate', this.selectedParametercustomerList);
            this.loading = false;
            break;
          case 'complete':
            this.columns = [
              { field: 'firstName', header: 'Name' },
              { field: 'mobileNo1', header: 'Mobile No.' },
              { field: 'registerDate', header: 'Register Date' },
              { field: 'customerType', header: 'Customer Type' },
              { field: 'countryName', header: 'Country Name' },
              { field: 'status', header: 'Status' },
              { field: 'customerId', header: '' },
            ];
            this.selectedParametercustomerList = res as any[];
            console.log('complete', this.selectedParametercustomerList);
            this.loading = false;
            break;
          case 'cancel':
            this.columns = [
              { field: 'firstName', header: 'Name' },
              { field: 'mobileNo1', header: 'Mobile No.' },
              { field: 'registerDate', header: 'Register Date' },
              { field: 'customerType', header: 'Customer Type' },
              { field: 'countryName', header: 'Country Name' },
              { field: 'status', header: 'Status' },
              { field: 'customerId', header: '' },
            ];
            this.selectedParametercustomerList = res as any[];
            console.log('cancel', this.selectedParametercustomerList);
            this.loading = false;
            break;
        }
      });


    }

    catch (error) {
      this.showMessageService.Error("selectedParametercustomer", 'Error while getting data : ' + error);
    }
  }


  //onRowSelect get details of customer
  enquirySelect(event: any) {
    let selectedCustomer = event;
    try {
      const ref = this.primengService.dialogService.open(ServeyAddComponent, {
        header: 'Add Customer Servey',
        width: '40%',
        contentStyle: { "overflow": "visible" },
        data: selectedCustomer
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getCustomerListStatusWise(this.setValue);
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Servey Add", 'Error while opening add dialog Customer : ' + error);
    }
  }

  //onRowSelect get details of customer
  serveySelect(event: any) {
    let selectedCustomer = event;
    try {
      const ref = this.primengService.dialogService.open(FormProcessAddComponent, {
        header: 'Add Customer Form Process',
        width: '30%',
        contentStyle: { "overflow": "visible" },
        data: selectedCustomer
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getCustomerListStatusWise(this.setValue);
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Servey Add", 'Error while opening add dialog Customer : ' + error);
    }
  }

  //onRowSelect get details of customer
  processSelect(event: any) {
    let selectedCustomer = event;
    try {
      const ref = this.primengService.dialogService.open(IdGenerateAddComponent, {
        header: 'Add ID Generated Customer',
        width: '30%',
        contentStyle: { "overflow": "visible" },
        data: selectedCustomer
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getCustomerListStatusWise(this.setValue);
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Servey Add", 'Error while opening add dialog Customer : ' + error);
    }
  }

  UpdateDialog() {

  }
}
