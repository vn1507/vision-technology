import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerWithParameterComponent } from './customer-with-parameter.component';

describe('CustomerWithParameterComponent', () => {
  let component: CustomerWithParameterComponent;
  let fixture: ComponentFixture<CustomerWithParameterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerWithParameterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerWithParameterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
