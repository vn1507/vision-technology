import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IDGenerate } from './IDGenerate';
import { environment } from '../../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class IdGenerateService {

  constructor(private http: HttpClient) { }

  //Method to post response
  InsertIDGenerate(idGenerate: IDGenerate): Observable<IDGenerate[]> {       
    return this.http.post<IDGenerate[]>(environment.webAPIURL + '/api/CustomerTechnicalDetails/postCustomerTechnicalDetails', idGenerate)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }
}
