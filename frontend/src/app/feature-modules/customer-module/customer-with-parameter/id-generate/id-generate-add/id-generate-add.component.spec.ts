import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdGenerateAddComponent } from './id-generate-add.component';

describe('IdGenerateAddComponent', () => {
  let component: IdGenerateAddComponent;
  let fixture: ComponentFixture<IdGenerateAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdGenerateAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdGenerateAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
