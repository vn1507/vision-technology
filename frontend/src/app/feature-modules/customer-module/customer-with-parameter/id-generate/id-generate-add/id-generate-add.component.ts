import { Component, OnInit } from '@angular/core';
import { IDGenerate } from '../shared/IDGenerate';
import { IdGenerateService } from '../shared/id-generate.service';

import { IOLT } from '../../../../admin-module/olt/shared/IOlt';
import { OtlService } from '../../../../admin-module/olt/shared/olt.service';

import { ISpliter } from '../../../../admin-module/spliter/shared/ISpliter';
import { SpliterService } from '../../../../admin-module/spliter/shared/spliter.service';

import { ShowMessageService } from '../../../../../shared-service/show-message.service';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-id-generate-add',
  templateUrl: './id-generate-add.component.html',
  styleUrls: ['./id-generate-add.component.scss']
})
export class IdGenerateAddComponent implements OnInit {

  public idGenerate: IDGenerate = {};

  public customerId: any;
  public customerName: any;
  
  public oltList: IOLT[];
  public selectedOlt: IOLT = {};

  public spliterList: ISpliter[];
  public selectedSpliter: ISpliter = {};

  public enableSpliter : boolean = true;
  
  public datePipe = new DatePipe('en-US');

  public ontTypeList: any = [
    {value:'OWN'},
    {value:'BSNL'}
  ];
  public selectedOntType: any = {};

  constructor(private otlService: OtlService, private spliterService: SpliterService, private config: DynamicDialogConfig, private ref: DynamicDialogRef,
    private idGenerateService: IdGenerateService, private showMessageService: ShowMessageService) { }

  ngOnInit(): void {
    this.customerId = this.config.data.customerId;
    this.customerName = this.config.data.firstName+' '+this.config.data.lastName;
    this.getOltNames();
  }

   //method to fetch olt
   getOltNames(){
    try{
      this.otlService.getOLTNames().subscribe((res: any) => {
        this.oltList = res as IOLT[];
      });
    }
    catch(error){
      this.showMessageService.Error("OLT Name", 'Error while feteching data : ' + error);
    }
  }

  //method to fetch olt
  getSpliterNames(){
    try{
      this.spliterService.getSpliterList().subscribe((res: any) => {
        this.spliterList = res as ISpliter[];
      });
    }
    catch(error){
      this.showMessageService.Error("Spliter Name", 'Error while feteching data : ' + error);
    }
  }

  //method to enable state
  onOLTChange(oltId: number) {          
    if (oltId) {
      this.spliterService.getSpliterByOltIdWise(oltId).subscribe((res: any) => {
        this.spliterList = res as ISpliter[];
          this.enableSpliter = false;
        }
      );
    } else {
      this.enableSpliter = true;
    }
  }
  //method to save the record
  SaveIDGenerateCustomer(){
    try{      
      this.idGenerate.customerId = this.customerId;
      this.idGenerate.oltId = this.selectedOlt.oltId;
      this.idGenerate.spliterId = this.selectedSpliter.spliterId;
      this.idGenerate.assignDate = new Date(this.datePipe.transform(this.idGenerate.assignDate, 'yyyy-MM-dd'));
      this.idGenerate.ontType = this.selectedOntType.value;
      this.idGenerate.status = "idGenerate";
        this.idGenerateService.InsertIDGenerate(this.idGenerate).subscribe((res: any) => {
            this.showMessageService.Success("ID Generate Save", res);
            this.idGenerate = {};
            this.ref.close(true);
          }, err => {
            this.showMessageService.Error("ID Generate Save", 'Error : ' + JSON.stringify(err.error));
          });              
    }
    catch (error) {
      this.showMessageService.Error("ID Generate", 'Error while saving data : ' + error);
    }
  }

}
