export interface IServey{
    serveyId?;
    customerId?;
    customerName?;
    employeeId?;
    employeeName?;
    serveyComment?;
    serveyDate?;
    serveyEndDate?;
    statusId?;
    statusName?;
    status?;
}