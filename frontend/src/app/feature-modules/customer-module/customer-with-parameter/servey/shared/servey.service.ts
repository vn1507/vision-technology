import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IServey } from './IServey';
import { environment } from '../../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ServeyService {

  constructor(private http: HttpClient) { }

  //Method to get servey by id
  getCustomerServeyById(customerId: number): Observable<IServey>{    
    return this.http.get<IServey>(environment.webAPIURL + '/api/CustomerServey/getCustomerServeyByCustomerId?CustomerId=' + customerId)
    .pipe(
      catchError((error: any) => {
        return throwError(error);
      })
    );
  }

  //Method to post response
  InsertServey(survey: IServey): Observable<IServey> {       
    return this.http.post<IServey>(environment.webAPIURL + '/api/CustomerServey/postCustomerServey', survey)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to update response
  updateCustomerServey(selectedSurvey: IServey): Observable<IServey> {       
    return this.http.post<IServey>(environment.webAPIURL + '/api/CustomerServey/updateCustomerServey', selectedSurvey)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

}
