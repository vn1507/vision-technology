import { Component, OnInit } from '@angular/core';
import { IServey } from '../shared/IServey';
import { ServeyService } from '../shared/servey.service';

import { IEmployee } from '../../../../employee-module/employee/shared/IEmployee';
import { EmployeeService } from '../../../../employee-module/employee/shared/employee.service';
import { CustomerWithParameterService } from '../../shared/customer-with-parameter.service';
import { ShowMessageService } from '../../../../../shared-service/show-message.service';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-servey-add',
  templateUrl: './servey-add.component.html',
  styleUrls: ['./servey-add.component.scss']
})
export class ServeyAddComponent implements OnInit {

  public servey: IServey = {};
  public customerId: any;
  public customerName: any;
  public status: any;

  public employeeList: IEmployee[];
  public selectedEmployee: IEmployee = {};

  public datePipe = new DatePipe('en-US');

  public StatusList: IServey[];
  public selectedStatus: IServey = {};

  public selectedStatusList: IServey;

  public assignedEmployee: boolean = true;
  public lableName: any;


  constructor(private employeeService: EmployeeService, private config: DynamicDialogConfig, private ref: DynamicDialogRef,
    private serveyService: ServeyService, private showMessageService: ShowMessageService, private customerWithParameterService: CustomerWithParameterService) { }

  ngOnInit(): void {    
    this.status = this.config.data.status;
    this.customerId = this.config.data.customerId;
    this.customerName = this.config.data.firstName + ' ' + this.config.data.lastName;
    this.getValuesById(this.customerId, this.status);
  }

  //method to fetch employees
  getEmployeeNames() {
    try {
      this.employeeService.getEmployeeData().subscribe((res: any) => {
        this.employeeList = res as IEmployee[];
        console.log('this.employeeList ', this.employeeList);
        this.selectedEmployee.employeeId = this.selectedStatusList[0].employeeId;
        this.selectedEmployee.employeeName = this.selectedStatusList[0].employeeName;
      });
    }
    catch (error) {
      this.showMessageService.Error("Employee Name", 'Error while feteching data : ' + error);
    }
  }

  //method to fetch employees
  getEmployeeNamess() {
    try {
      this.employeeService.getEmployeeData().subscribe((res: any) => {
        this.employeeList = res as IEmployee[];
        console.log('this.employeeList ', this.employeeList);
      });
    }
    catch (error) {
      this.showMessageService.Error("Employee Name", 'Error while feteching data : ' + error);
    }
  }

  //method to fetch status
  getStatusList(hardcodeValue: number) {
    try {
      this.customerWithParameterService.getStatusList(hardcodeValue).subscribe((res: any) => {
        this.StatusList = res as IServey[];
        console.log('this.StatusList ', this.StatusList);
        this.selectedStatus.statusId = (this.selectedStatus.statusId != 'undefined') ? this.selectedStatusList[0].statusId : this.selectedStatus.statusId;
        this.selectedStatus.statusName = (this.selectedStatus.statusName != 'undefined') ? this.selectedStatusList[0].statusName : this.selectedStatus.statusName;
      });
    }
    catch (error) {
      this.showMessageService.Error("Employee Name", 'Error while feteching data : ' + error);
    }
  }

  //method to fetch status
  getStatusLists(hardcodeValue: number) {
    try {
      this.customerWithParameterService.getStatusList(hardcodeValue).subscribe((res: any) => {
        this.StatusList = res as IServey[];
        console.log('this.StatusList ', this.StatusList);
      });
    }
    catch (error) {
      this.showMessageService.Error("Employee Name", 'Error while feteching data : ' + error);
    }
  }

  //method on change event
  enableServeyEndDate(event) {
    try {
      console.log(event);
      if (event.statusName == 'Servey' || event.statusName == 'Cancel') {
        this.assignedEmployee = false;
      } else {
        this.assignedEmployee = true;
      }
    } catch (error) {
      this.showMessageService.Error("enableServeyEndDate", 'Error while displaying data : ' + error);
    }
  }

  getValuesById(customerId: number, status: string) {
    try {      
      if (status == 'enquiry') {
        this.servey.employeeId = null;
        this.servey.employeeName = null;
        this.servey.statusId = null;
        this.servey.statusName = null;
        this.servey.serveyDate = null;
        this.servey.serveyEndDate = null;
        this.servey.serveyComment = null;
        this.getEmployeeNamess();
        this.getStatusLists(1);
        this.lableName = 'Save';
      } else {
        this.serveyService.getCustomerServeyById(customerId).subscribe((res: any) => {
          this.selectedStatusList = res as IServey;          
          this.servey.serveyId = (this.servey.serveyId != 'undefined') ? this.selectedStatusList[0].serveyId : this.servey.serveyId;
          this.servey.employeeId = (this.servey.employeeId != 'undefined') ? this.selectedStatusList[0].employeeId : null;
          this.servey.employeeName = (this.servey.employeeName != 'undefined') ? this.selectedStatusList[0].employeeName : this.servey.employeeName;
          this.servey.statusId = (this.servey.statusId != 'undefined') ? this.selectedStatusList[0].statusId : this.servey.statusId;
          this.servey.statusName = (this.servey.statusName != 'undefined') ? this.selectedStatusList[0].statusName : this.servey.statusName;
          this.servey.serveyDate = new Date(this.datePipe.transform(this.selectedStatusList[0].serveyDate, 'dd-MMM-yyyy'));
          this.servey.serveyEndDate = (this.servey.serveyEndDate == null) ? null : new Date(this.datePipe.transform(this.selectedStatusList[0].serveyEndDate, 'dd-MMM-yyyy'));
          this.servey.serveyComment = (this.servey.serveyComment != 'undefined') ? this.selectedStatusList[0].serveyComment : this.servey.serveyComment;
          this.getEmployeeNames();
          this.getStatusList(1);
          this.lableName = 'Update';
        });
      }
    }
    catch (error) {
      this.showMessageService.Error("Form Process by customer id", 'Error while getting data : ' + error);
    }
  }

  //Method to save and update data
  CustomerServey() {
    try {      
      if (this.lableName == 'Save') {
        this.servey.customerId = this.customerId;
        this.servey.employeeId = this.selectedEmployee.employeeId;
        this.servey.statusId = this.selectedStatus.statusId;
        this.servey.status = this.selectedStatus.statusName;
        this.servey.serveyDate = new Date(this.datePipe.transform(this.servey.serveyDate, 'yyyy-MM-dd'));
        this.servey.serveyEndDate = (this.servey.serveyEndDate == null) ? this.servey.serveyDate : new Date(this.datePipe.transform(this.servey.serveyEndDate, 'yyyy-MM-dd'));
        this.serveyService.InsertServey(this.servey).subscribe((res: any) => {
          this.showMessageService.Success("Servey", res);
          this.servey = {};
          this.ref.close(true);
        }, err => {
          this.showMessageService.Error("Servey Save", 'Error : ' + JSON.stringify(err.error));
        });
      } else {        
        this.servey.customerId = this.customerId;
        this.servey.employeeId = this.selectedEmployee.employeeId;
        this.servey.statusId = this.selectedStatus.statusId;
        this.servey.status = this.selectedStatus.statusName;
        this.servey.serveyDate = new Date(this.datePipe.transform(this.servey.serveyDate, 'yyyy-MM-dd'));
        this.servey.serveyEndDate = (this.servey.serveyEndDate == null) ? this.servey.serveyDate : new Date(this.datePipe.transform(this.servey.serveyEndDate, 'yyyy-MM-dd'));
        this.serveyService.updateCustomerServey(this.servey).subscribe((res: any) => {
          this.showMessageService.Success("Servey Updated", res);
          this.servey = {};
          this.ref.close(true);
        }, err => {
          this.showMessageService.Error("Servey Save", 'Error : ' + JSON.stringify(err.error));
        });
      }

    }
    catch (error) {
      this.showMessageService.Error("Servey", 'Error while saving data : ' + error);
    }
  }

}
