import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServeyAddComponent } from './servey-add.component';

describe('ServeyAddComponent', () => {
  let component: ServeyAddComponent;
  let fixture: ComponentFixture<ServeyAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServeyAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServeyAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
