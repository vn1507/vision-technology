import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomerWithParameterService {

  constructor(private http: HttpClient) { }

  //Method to get response
  getCustomerListStatusWise(value: string): Observable<any[]> {    
    return this.http.get<any[]>(environment.webAPIURL + '/api/Customer/getCustomerListStatusWise?Status=' + value)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to get servey response
  getStatusList(hardcodeValue: number): Observable<any[]> {        
    return this.http.get<any[]>(environment.webAPIURL + '/api/status/getStatusList?hardcode=' + hardcodeValue)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

}
