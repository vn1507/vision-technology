import { TestBed } from '@angular/core/testing';

import { CustomerWithParameterService } from './customer-with-parameter.service';

describe('CustomerWithParameterService', () => {
  let service: CustomerWithParameterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CustomerWithParameterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
