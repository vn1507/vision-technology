import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormProcessAddComponent } from './form-process-add.component';

describe('FormProcessAddComponent', () => {
  let component: FormProcessAddComponent;
  let fixture: ComponentFixture<FormProcessAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormProcessAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormProcessAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
