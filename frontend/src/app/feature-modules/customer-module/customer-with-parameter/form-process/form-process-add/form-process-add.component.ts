import { Component, OnInit } from '@angular/core';
import { IFormProcess } from '../shared/IFormProcess';
import { FormProcessService } from '../shared/form-process.service';

import { CustomerWithParameterService } from '../../shared/customer-with-parameter.service';
import { ShowMessageService } from '../../../../../shared-service/show-message.service';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-form-process-add',
  templateUrl: './form-process-add.component.html',
  styleUrls: ['./form-process-add.component.scss']
})
export class FormProcessAddComponent implements OnInit {

  public process: IFormProcess = {};
  public customerId: any;
  public customerName: any;
  public status: any;

  public StatusList: IFormProcess[];
  public selectedStatus: IFormProcess = {};

  public selectedStatusList: IFormProcess;

  public selectedProcess: IFormProcess[];
  public selectedProcessByIdStatus: IFormProcess = {};

  public datePipe = new DatePipe('en-US');

  public lableName: any;


  constructor(private config: DynamicDialogConfig, private ref: DynamicDialogRef,
    private formProcessService: FormProcessService, private showMessageService: ShowMessageService, private customerWithParameterService: CustomerWithParameterService) { }

  ngOnInit(): void {
    this.status = this.config.data.status;
    this.customerId = this.config.data.customerId;
    this.customerName = this.config.data.firstName + ' ' + this.config.data.lastName;
    this.getValuesById(this.customerId, this.status);
  }

  //method to fetch status
  getStatusList(hardcodeValue: number) {
    try {
      this.customerWithParameterService.getStatusList(hardcodeValue).subscribe((res: any) => {
        this.StatusList = res as IFormProcess[];
        this.selectedStatus.statusId = (this.selectedStatus.statusId != 'undefined') ? this.selectedStatusList.statusId : this.selectedStatus.statusId;
        this.selectedStatus.statusName = (this.selectedStatus.statusName != 'undefined') ? this.selectedStatusList.statusName : this.selectedStatus.statusName;
      });
    }
    catch (error) {
      this.showMessageService.Error("Employee Name", 'Error while feteching data : ' + error);
    }
  }

  //method to fetch status
  getStatusLists(hardcodeValue: number) {
    try {
      this.customerWithParameterService.getStatusList(hardcodeValue).subscribe((res: any) => {
        this.StatusList = res as IFormProcess[];
      });
    }
    catch (error) {
      this.showMessageService.Error("Employee Name", 'Error while feteching data : ' + error);
    }
  }

  getValuesById(customerId: number, status: string) {
    try {
      debugger
      if (status == 'Servey') {
        this.process.statusId = null;
        this.process.statusName = null;
        this.process.formProcessDate = null;
        this.getStatusLists(2);
        this.lableName = 'Save';
      } else {
        this.formProcessService.getCustomerFormProcessByCustomerId(customerId).subscribe((res: any) => {
          this.selectedStatusList = res as IFormProcess;
          console.log('this.selectedStatusList ',this.selectedStatusList );
          debugger
          this.process.formProcessId = (this.process.formProcessId != 'undefined') ? this.selectedStatusList.formProcessId : this.process.formProcessId;
          this.process.statusId = (this.process.statusId != 'undefined') ? this.selectedStatusList.statusId : this.process.statusId;
          this.process.statusName = (this.process.statusName != 'undefined') ? this.selectedStatusList.statusName : this.process.statusName;
          this.process.formProcessDate = new Date(this.datePipe.transform(this.selectedStatusList.formProcessDate, 'dd-MMM-yyyy'));
          this.getStatusList(2);
          this.lableName = 'Update';
        });
      }
    }
    catch (error) {
      this.showMessageService.Error("Form Process by customer id", 'Error while getting data : ' + error);
    }
  }

  // getStatus(){
  //   try{
  //     let allStatus = this.formProcessStatusList;
  //     for(let i = 0; allStatus.length > i; i++){
  //       let currectStatus = this.selectedProcess[0].status;
  //       console.log('currectStatus',currectStatus);
  //       this.selectedformProcessStatus.status = currectStatus;
  //       console.log('currectStatus',this.selectedformProcessStatus);
  //       break;
  //     }


  //     //this.selectedformProcessStatus.status = currectStatus;
  //     //console.log('this.selectedformProcessStatus.status', this.selectedformProcessStatus);
  //   }catch (error) {
  //     this.showMessageService.Error("Form Process status", 'Error while saving data : ' + error);
  //   }
  // }


  CustomerFormProcess() {
    try {
      debugger
      if (this.lableName == 'Save') {
        this.process.customerId = this.customerId;
        this.process.statusId = this.selectedStatus.statusId;
        this.process.status = this.selectedStatus.statusName;
        this.process.formProcessDate = new Date(this.datePipe.transform(this.process.formProcessDate, 'yyyy-MM-dd'));        
        this.formProcessService.InsertFormProcess(this.process).subscribe((res: any) => {
          this.showMessageService.Success("Form Process Save", res);
          this.process = {};
          this.ref.close(true);
        }, err => {
          this.showMessageService.Error("Form Process Save", 'Error : ' + err.error);
        });
      } else {
        this.process.customerId = this.customerId;
        this.process.statusId = this.selectedStatus.statusId;
        this.process.status = this.selectedStatus.statusName;
        this.process.formProcessDate = new Date(this.datePipe.transform(this.process.formProcessDate, 'yyyy-MM-dd'));        
        this.formProcessService.UpdateFormProcess(this.process).subscribe((res: any) => {
            this.showMessageService.Success("Form Process Update", res);
            this.process = {};
            this.ref.close(true);
          }, err => {
            this.showMessageService.Error("Form Process Update", 'Error : ' + JSON.stringify(err.error));
          });        
      }
    }
    catch (error) {
      this.showMessageService.Error("Form Process", 'Error while saving and updating data : ' + error);
    }
  }

}
