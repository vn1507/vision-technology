export interface IFormProcess{
    formProcessId?;
    customerId?;
    customerName?;
    formProcessDate?;
    status?;
    statusId?;
    statusName?;
}