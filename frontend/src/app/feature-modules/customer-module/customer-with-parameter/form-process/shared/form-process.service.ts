import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IFormProcess } from './IFormProcess';
import { environment } from '../../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FormProcessService {

  constructor(private http: HttpClient) { }

  //Method to get response by id
  getCustomerFormProcessList(): Observable<IFormProcess[]> {        
    return this.http.get<IFormProcess[]>(environment.webAPIURL + '/api/CustomerFormProcess/getCustomerFormProcessList')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to get response by id
  getCustomerFormProcessByCustomerId(value: number): Observable<IFormProcess[]> {      
    return this.http.get<IFormProcess[]>(environment.webAPIURL + '/api/CustomerFormProcess/getCustomerFormProcessByCustomerId?CustomerId='+value)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to post response
  InsertFormProcess(formProcess: IFormProcess): Observable<IFormProcess[]> {       
    return this.http.post<IFormProcess[]>(environment.webAPIURL + '/api/CustomerFormProcess/postCustomerFormProcess', formProcess)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to update response
  UpdateFormProcess(selectedFormProcess: IFormProcess): Observable<IFormProcess[]> {       
    return this.http.post<IFormProcess[]>(environment.webAPIURL + '/api/CustomerFormProcess/updateCustomerFormProcess', selectedFormProcess)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

}
