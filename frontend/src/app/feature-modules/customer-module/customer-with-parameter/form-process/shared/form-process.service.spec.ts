import { TestBed } from '@angular/core/testing';

import { FormProcessService } from './form-process.service';

describe('FormProcessService', () => {
  let service: FormProcessService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormProcessService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
