export interface IEmployee{
    employeeId ?;
    employeeName ?;
    employeeAddress ?;
    mobileNo ?;
    emailId ?;
    aadharNo ?;
    designationId ?;
    designationName ?;
    status ?;
}