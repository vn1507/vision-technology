import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IEmployee } from '../shared/IEmployee';
import { environment } from '../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }

  //Method to get response
  getEmployeeList(): Observable<IEmployee> {
    return this.http.get<IEmployee>(environment.webAPIURL + '/api/Employee/getEmployeeList')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to get names and id's
  getEmployeeData(): Observable<IEmployee> {
    return this.http.get<IEmployee>(environment.webAPIURL + '/api/Employee/getEmployeeData')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to add
  InsertEmployee(employee: IEmployee): Observable<IEmployee> {
    return this.http.post<IEmployee>(environment.webAPIURL + '/api/Employee/postEmployee', employee)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to update
  UpdateEmployee(selectedEmployee: IEmployee): Observable<IEmployee> {
    return this.http.post<IEmployee>(environment.webAPIURL + '/api/Employee/updateEmployee', selectedEmployee)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

}
