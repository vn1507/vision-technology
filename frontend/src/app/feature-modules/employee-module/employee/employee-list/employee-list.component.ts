import { Component, OnInit } from '@angular/core';
import { LayoutComponent } from '../../../../layout/layout.component';

import { IEmployee } from '../shared/IEmployee';
import { EmployeeService } from '../shared/employee.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { PrimengService } from '../../../../shared-service/primeng.service';
import { EmployeeSaveComponent } from '../employee-save/employee-save.component';
import { EmployeeUpdateComponent } from '../employee-update/employee-update.component';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss'],
  providers: [PrimengService],
  entryComponents: [EmployeeSaveComponent, EmployeeUpdateComponent]
})
export class EmployeeListComponent implements OnInit {

  public employeeList: IEmployee[];
  public columns: any[];
  public loading: boolean;

  constructor(public layoutComponent: LayoutComponent,private showMessageService: ShowMessageService, 
    private primengService: PrimengService, private employeeService: EmployeeService) {
    this.layoutComponent.moduleName = "Employee's";
    this.columns = [
      { field: 'employeeName', header: 'Employee Name' },
      { field: 'employeeAddress', header: 'Address' },
      { field: 'mobileNo', header: 'Mobile No.' },
      { field: 'emailId', header: 'Email ID' },
      { field: 'aadharNo', header: 'Aadhar No.' },
      { field: 'designationName', header: 'Designation' },     
      { field: 'employeeId', header: '' },
    ];
   }

  ngOnInit(): void {
    this.getEmployeeList();
  }

  //method to get data
  getEmployeeList(){
    try{
      this.loading = true;
      this.employeeService.getEmployeeList().subscribe((res : any) => {
        this.employeeList = res as IEmployee[];
        this.loading = false;
      });
    }
    catch(error){
      this.showMessageService.Error("Employee", 'Error while getting data : ' + error);
    }
  }

  //method to open add dailog box
  AddDialog() {
    try {

      const ref = this.primengService.dialogService.open(EmployeeSaveComponent, {
        header: 'Add Employee',
        width: '35%'
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getEmployeeList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Employee", 'Error while opening add dialog Employee : ' + error);
    }
  }

  //method to open update dailog box
  UpdateDialog(employee: IEmployee) {
    try {
      let selectedEmployee = {
        employeeId: employee.employeeId,
        employeeName: employee.employeeName,
        designationId: employee.designationId,
        designationName: employee.designationName,
        mobileNo: employee.mobileNo,
        emailId: employee.emailId,
        aadharNo: employee.aadharNo,
        employeeAddress: employee.employeeAddress,
      };      

      const ref = this.primengService.dialogService.open(EmployeeUpdateComponent, {
        header: 'Update Employee',
        data: selectedEmployee,
        width: '35%'
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getEmployeeList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Employee", 'Error while opening update dialog Employee : ' + error);
    }
  }

  //method to open delete dailog box
  DeleteDialog(employeeId: number){

  }  

}
