import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../shared/employee.service';
import { IEmployee } from '../shared/IEmployee';
import { DesignationService } from '../../../admin-module/designation/shared/designation.service';
import { IDesignation } from '../../../admin-module/designation/shared/IDesignation';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-employee-save',
  templateUrl: './employee-save.component.html',
  styleUrls: ['./employee-save.component.scss']
})
export class EmployeeSaveComponent implements OnInit {

  public employee: IEmployee = {};

  public designationList: IDesignation[];
  public selectedDesignation: IDesignation = {};
  
  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, 
    private employeeService: EmployeeService, private designationService: DesignationService) { }

  ngOnInit(): void {
    this.getDesignations();
    // let aadharNo = '123456789012';
    // let formattedAadharNo = aadharNo.slice(0, 4) + "-" + aadharNo.slice(4, 8) + "-" + aadharNo.slice(8);
    // console.log(formattedAadharNo);
  }

  // Validation of maxLength for number input field  
  onlyNumberKey(event) {
    if (event.target.value.length == 0 && event.which == 48) {
      return false;
    }
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 46 && event.charCode <= 57;
  }

  //method to fetch designations
  getDesignations(){
    try{
      this.designationService.getDesignationList().subscribe((res: any) => {
        this.designationList = res as IDesignation[];
      });
    }
    catch(error){
      this.showMessageService.Error("Designation", 'Error while feteching data : ' + error);
    }
  }

  //method to save data
  SaveEmployee(){
    try{
      this.employee.designationId = this.selectedDesignation.designationId;
      this.employeeService.InsertEmployee(this.employee).subscribe((res: any) => {
        this.showMessageService.Success("Employee", res);
              this.employee = {};
              this.ref.close(true);
      }, err => {
              if(err.status == 409){
                this.showMessageService.Error("Duplicate Employee" ,  "Employee already exists");  
              } else{
                this.showMessageService.Error("Employee Save", 'Error : ' + err.error);  
              }
      });
    }
    catch(error){
      this.showMessageService.Error("Employee", 'Error while adding data : ' + error);
    }
  }

}
