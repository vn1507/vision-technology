import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeRoutingModule } from './employee-routing.module';
import { SharedModule } from '../../shared-module/shared.module';
import { SharedComponentsModule } from '../../shared-components/shared-components.module';

import { EmployeeListComponent } from './employee/employee-list/employee-list.component';
import { EmployeeSaveComponent } from './employee/employee-save/employee-save.component';
import { EmployeeUpdateComponent } from './employee/employee-update/employee-update.component';



@NgModule({
  imports: [
    CommonModule,
    EmployeeRoutingModule,
    SharedModule,
    SharedComponentsModule
  ],
  declarations: [EmployeeListComponent, EmployeeSaveComponent, EmployeeUpdateComponent],
})
export class EmployeeModule { }
