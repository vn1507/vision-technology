import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerReportDetailComponent } from './customer-report-detail.component';

describe('CustomerReportDetailComponent', () => {
  let component: CustomerReportDetailComponent;
  let fixture: ComponentFixture<CustomerReportDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerReportDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerReportDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
