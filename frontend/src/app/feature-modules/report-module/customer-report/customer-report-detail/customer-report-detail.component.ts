import { Component, OnInit } from '@angular/core';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ICustomerReport } from '../shared/ICustomerReport';
import { CustomerReportService } from '../shared/customer-report.service';

@Component({
  selector: 'app-customer-report-detail',
  templateUrl: './customer-report-detail.component.html',
  styleUrls: ['./customer-report-detail.component.scss']
})
export class CustomerReportDetailComponent implements OnInit {

  public CustomerReportDetail: ICustomerReport[];
  public customerId: any;

  constructor(private config: DynamicDialogConfig, private ref: DynamicDialogRef,
    private showMessageService: ShowMessageService, private customerReportService: CustomerReportService) { }

  ngOnInit(): void {
    this.customerId = this.config.data.customerId;
    this.getValuesById();
  }

  //method to get report using customerid
  getValuesById() {
    try {
      this.customerReportService.getCustomerReportById(this.customerId).subscribe((res: any) => {
        this.CustomerReportDetail = res as ICustomerReport[];
        console.log('this.CustomerReportDetail ', this.CustomerReportDetail);
      });
    }
    catch (error) {
      this.showMessageService.Error("Customer Report By ID", 'Error while getting data : ' + error);
    }
  }

  //export Table to excel
  exportToExcel(tableID, filename = '') {
    let downloadLink;
    let dataType = 'application/vnd.ms-excel';
    let tableSelect = document.getElementById(tableID);
    let tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

    // Specify file name
    filename = filename ? filename + '.xls' : 'excel_data.xls';

    // Create download link element
    downloadLink = document.createElement("a");

    document.body.appendChild(downloadLink);

    if (navigator.msSaveOrOpenBlob) {
      let blob = new Blob(['\ufeff', tableHTML], {
        type: dataType
      });
      navigator.msSaveOrOpenBlob(blob, filename);
    } else {
      // Create a link to the file
      downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

      // Setting the file name
      downloadLink.download = filename;

      //triggering the function
      downloadLink.click();
    }
  }

}
