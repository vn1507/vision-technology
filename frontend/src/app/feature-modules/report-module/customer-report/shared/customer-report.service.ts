import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICustomerReport } from '../shared/ICustomerReport';
import { environment } from '../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomerReportService {

  constructor(private http: HttpClient) { }

  //Method to get response
  getCustomerReportById(customerId: number): Observable<ICustomerReport> {
    return this.http.get<ICustomerReport>(environment.webAPIURL + '/api/CustomerReport/getCustomerById?CustomerId='+customerId)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }
}
