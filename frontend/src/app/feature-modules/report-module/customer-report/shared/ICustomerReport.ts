export interface ICustomerReport{
        customerId ?;
        customerType ?;
        usageCode ?;
        title ?;

        customerName ?;
        firstName?;
        middleName?;
        lastName?;
        mobileNo1 ?;
        emailId ?;
        fullAddress ?;
        countryId ?;
        countryName ?;

        stateId ?;
        stateName ?;
        districtId ?;
        districtName ?;
        cityId ?;
        cityName ?;
        areaId ?;
        areaName ?;
        registerDate ?;
        status ?;

        serveyId ?;
        employeeId ?;
        employeeName ?;
        serveyComment ?;
        serveyDate ?;
        serveyStatus ?;

        formProcessId ?;
        formProcessDate ?;
        formProcessStatus ?;

        technicalId ?;
        phoneNo ?;
        orderNo ?;
        vLanNo ?;
        oltId ?;
        oltName ?;
        spliterId ?;
        spliterName ?;
        ponsNo ?;
        assignDate ?;
        fiberNo ?;
        frameNo ?;
        assignIpAddress ?;
        oltPortNo ?;
        ontType ?;
        technicalStatus ?;
}