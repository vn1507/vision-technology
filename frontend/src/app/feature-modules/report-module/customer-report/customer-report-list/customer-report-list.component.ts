import { Component, OnInit } from '@angular/core';
import { LayoutComponent } from '../../../../layout/layout.component';

import { ICustomerReport } from '../shared/ICustomerReport';
import { CustomerService } from '../../../customer-module/customer/shared/customer.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { PrimengService } from '../../../../shared-service/primeng.service';
import { CustomerReportDetailComponent } from '../customer-report-detail/customer-report-detail.component';

@Component({
  selector: 'app-customer-report-list',
  templateUrl: './customer-report-list.component.html',
  styleUrls: ['./customer-report-list.component.scss'],
  providers: [PrimengService],
  entryComponents: [CustomerReportDetailComponent]
})
export class CustomerReportListComponent implements OnInit {

  public customerReportList: ICustomerReport[];
  public columns: any[];
  public loading: boolean;

  constructor(public layoutComponent: LayoutComponent,private showMessageService: ShowMessageService, 
    private primengService: PrimengService, private customerService: CustomerService) {
    this.layoutComponent.moduleName = "Reports";
    this.columns = [
      { field: 'customerType', header: 'Customer Type' },
      { field: 'usageCode', header: 'Usage Code' },
      { field: 'title', header: 'Title' },
      { field: 'firstName', header: 'First Name' },
      { field: 'mobileNo1', header: 'Mobile No.' },
      { field: 'emailId', header: 'Email ID' },
      { field: 'countryName', header: 'Country' },
      { field: 'stateName', header: 'State' },     
      { field: 'districtName', header: 'District' },     
      { field: 'cityName', header: 'City' },     
      { field: 'stateName', header: 'State' },     
      { field: 'registerDate', header: 'Register Date' },      
    ];
   }

   ngOnInit(): void {
    this.getCustomerList();
  }

  //method to get data
  getCustomerList(){
    try{
      this.loading = true;
      this.customerService.getCustomerList().subscribe((res : any) => {
        this.customerReportList = res as ICustomerReport[];
        this.loading = false;
      });
    }
    catch(error){
      this.showMessageService.Error("Customer Report", 'Error while getting data : ' + error);
    }
  }

  //onRowSelect get details of customer
  customerSelect(customerReport: ICustomerReport){
    let selectedCustomer = customerReport;
    try{
      const ref = this.primengService.dialogService.open(CustomerReportDetailComponent,{
        header: 'Details',
        contentStyle: {"overflow": "visible"},
        data: selectedCustomer
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getCustomerList()
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("customer", 'Error while opening add dialog Customer : ' + error);
    }
  }

}
