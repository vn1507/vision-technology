import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportRoutingModule } from './report-routing.module';
import { SharedModule } from '../../shared-module/shared.module';
import { SharedComponentsModule } from '../../shared-components/shared-components.module';

import { CustomerReportListComponent } from './customer-report/customer-report-list/customer-report-list.component';
import { CustomerReportDetailComponent } from './customer-report/customer-report-detail/customer-report-detail.component';

@NgModule({
  imports: [
    CommonModule,
    ReportRoutingModule,
    SharedModule,
    SharedComponentsModule
  ],
  declarations: [CustomerReportListComponent, CustomerReportDetailComponent],
})
export class ReportModule { }
