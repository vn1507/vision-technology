import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomerReportListComponent } from './customer-report/customer-report-list/customer-report-list.component';

const routes: Routes = [
  {
    path: '', component: CustomerReportListComponent,    
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRoutingModule { }
