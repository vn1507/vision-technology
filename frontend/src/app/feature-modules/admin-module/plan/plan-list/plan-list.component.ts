import { Component, OnInit } from '@angular/core';
import { IPlan } from '../shared/IPlan';
import { PlanService } from '../shared/plan.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { PrimengService } from '../../../../shared-service/primeng.service';
import { PlanSaveComponent } from '../plan-save/plan-save.component';
import { PlanUpdateComponent } from '../plan-update/plan-update.component';

@Component({
  selector: 'app-plan-list',
  templateUrl: './plan-list.component.html',
  styleUrls: ['./plan-list.component.scss'],
  providers:[PrimengService],
  entryComponents:[PlanSaveComponent, PlanUpdateComponent]
})
export class PlanListComponent implements OnInit {

  public planList: IPlan[];
  public columns: any[];
  public loading: boolean;

  constructor(private showMessageService: ShowMessageService, private primengService: PrimengService, 
    private planService: PlanService) { 
    this.columns = [
      { field: 'planName', header: 'Plan Name' },     
      { field: 'discription', header: 'Description ' },
      { field: 'planId', header: '' },
    ];    
  }

  ngOnInit(): void {
    this.getPlanList();
  }

  //method to get data
  getPlanList(){
    try{
      this.loading = true;
      this.planService.getPlanList().subscribe((res : any) => {
        this.planList = res as IPlan[];
        this.loading = false;
      });
    }
    catch(error){
      this.showMessageService.Error("Plan", 'Error while getting data : ' + error);
    }
  }  

  //method to open add dailog box
  AddDialog() {
    try {

      const ref = this.primengService.dialogService.open(PlanSaveComponent, {
        header: 'Add Plan',
        width: '20%'
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getPlanList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Plan", 'Error while opening add dialog  : ' + error);
    }
  }

  //method to open update dailog box
  UpdateDialog(plan: IPlan) {
    try {
      let selectedPlan = {
        planId: plan.planId,
        planName: plan.planName,
        discription: plan.discription,
      };      

      const ref = this.primengService.dialogService.open(PlanUpdateComponent, {
        header: 'Update Plan',
        width: '20%',
        data: selectedPlan
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getPlanList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Plan", 'Error while opening add dialog : ' + error);
    }
  }

  //method to open delete dailog box
  DeleteDialog(planId: number){

  }

}
