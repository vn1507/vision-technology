import { Component, OnInit } from '@angular/core';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { IPlan } from '../shared/IPlan';
import { PlanService } from '../shared/plan.service';

@Component({
  selector: 'app-plan-save',
  templateUrl: './plan-save.component.html',
  styleUrls: ['./plan-save.component.scss']
})
export class PlanSaveComponent implements OnInit {

  public plan: IPlan = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, 
    private planService: PlanService) { }

  ngOnInit(): void {
  }

  //method to save data
  SavePlan(){
    try{      
      this.planService.InsertPlan(this.plan).subscribe((res: any) => {
        this.showMessageService.Success("Plan", res);
              this.plan = {};
              this.ref.close(true);
      }, err => {
              this.showMessageService.Error("Plan Save", 'Error : ' + err.error);  
      });
    }
    catch(error){
      this.showMessageService.Error("Plan", 'Error while adding data : ' + error);
    }
  }

}
