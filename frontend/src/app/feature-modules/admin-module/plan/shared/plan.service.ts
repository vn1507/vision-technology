import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IPlan } from '../shared/IPlan';
import { environment } from '../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PlanService {

  constructor(private http: HttpClient) { }

  //Method to get response
  getPlanList(): Observable<IPlan> {
    return this.http.get<IPlan>(environment.webAPIURL + '/api/Plan/getPlanList')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to get plan names and id's
  getPlanData(): Observable<IPlan> {
    return this.http.get<IPlan>(environment.webAPIURL + '/api/Plan/getPlanData')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to add
  InsertPlan(plan: IPlan): Observable<IPlan> {
    return this.http.post<IPlan>(environment.webAPIURL + '/api/Plan/postPlan', plan)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to Update
  UpdatePlan(selectedPlan: IPlan): Observable<IPlan> {    
    return this.http.post<IPlan>(environment.webAPIURL + '/api/Plan/updatePlan', selectedPlan)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

}
