import { Component, OnInit } from '@angular/core';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { IPlan } from '../shared/IPlan';
import { PlanService } from '../shared/plan.service';

@Component({
  selector: 'app-plan-update',
  templateUrl: './plan-update.component.html',
  styleUrls: ['./plan-update.component.scss']
})
export class PlanUpdateComponent implements OnInit {

  public selectedPlan: IPlan = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, 
    private config: DynamicDialogConfig, private planService: PlanService) { }


  ngOnInit(): void {
    this.selectedPlan = this.config.data;
  }

  //method to update data
  UpdatePlan(){
    try{      
      this.planService.UpdatePlan(this.selectedPlan).subscribe((res: any) => {
        this.showMessageService.Success("Plan", res);
              this.selectedPlan = {};
              this.ref.close(true);
      }, err => {
              this.showMessageService.Error("Plan Update", 'Error : ' + err.error);  
      });
    }
    catch(error){
      this.showMessageService.Error("Plan", 'Error while updating data : ' + error);
    }
  }

}
