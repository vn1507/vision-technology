import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { IOLT } from "../shared/IOlt";
import { environment } from '../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OtlService {

  constructor(private http: HttpClient) { }

  //Method to get response
  getOLTList(): Observable<IOLT> {
    return this.http.get<IOLT>(environment.webAPIURL + '/api/OLT/getOLTList')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to get response only names and id's
  getOLTNames(): Observable<IOLT> {    
    return this.http.get<IOLT>(environment.webAPIURL + '/api/OLT/getOLTData')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to add
  InsertOLT(OLT: IOLT): Observable<IOLT> {
    return this.http.post<IOLT>(environment.webAPIURL + '/api/OLT/postOLT', OLT)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to Update
  UpdateOLT(selectedOLT: IOLT): Observable<IOLT> {    
    return this.http.post<IOLT>(environment.webAPIURL + '/api/OLT/updateOLT', selectedOLT)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

}
