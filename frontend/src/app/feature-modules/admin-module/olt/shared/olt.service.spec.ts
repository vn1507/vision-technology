import { TestBed } from '@angular/core/testing';

import { OtlService } from './olt.service';

describe('OtlService', () => {
  let service: OtlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OtlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
