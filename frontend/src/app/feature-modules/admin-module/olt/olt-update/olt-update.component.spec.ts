import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OltUpdateComponent } from './olt-update.component';

describe('OltUpdateComponent', () => {
  let component: OltUpdateComponent;
  let fixture: ComponentFixture<OltUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OltUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OltUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
