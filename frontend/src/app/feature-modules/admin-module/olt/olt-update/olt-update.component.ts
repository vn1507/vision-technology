import { Component, OnInit } from '@angular/core';
import { IOLT } from '../shared/IOlt';
import { OtlService } from '../shared/olt.service';
import { ICompany } from '../../company/shared/ICompany';
import { CompanyService } from '../../company/shared/company.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-olt-update',
  templateUrl: './olt-update.component.html',
  styleUrls: ['./olt-update.component.scss']
})
export class OltUpdateComponent implements OnInit {

  public selectedOLT: IOLT = {};

  public companyList : ICompany[];
  public selectedCompany: ICompany = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef,
    private companyService :CompanyService, private otlService: OtlService, private config: DynamicDialogConfig) { }

  ngOnInit(): void {
    this.selectedOLT = this.config.data;
    this.getCompanyNames();    
  }

  //Method to get company names
  getCompanyNames(){
    try{
      this.companyService.getCompanyList().subscribe((res: any) => {
        this.companyList = res as ICompany[];
        this.selectedCompany.companyId = this.selectedOLT.companyId;
        this.selectedCompany.companyName = this.selectedOLT.companyName;
      });
    }
    catch(error){
      this.showMessageService.Error("getCompanyNames", 'Error while adding data : ' + error);
    }
  }

  //Method to add data
  UpdateOLT(){
    try{      
      this.selectedOLT.companyId = this.selectedCompany.companyId;
      this.otlService.UpdateOLT(this.selectedOLT).subscribe((res : any) => {
        this.showMessageService.Success("OLT", res);
              this.selectedOLT = {};
              this.ref.close(true);
      }, err => {
              this.showMessageService.Error("OLT Update", 'Error : ' + err.error);  
      });
    }
    catch(error){
      this.showMessageService.Error("OLT", 'Error while adding data : ' + error);
    }
  }

}
