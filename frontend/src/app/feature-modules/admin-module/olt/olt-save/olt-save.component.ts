import { Component, OnInit } from '@angular/core';
import { IOLT } from '../shared/IOlt';
import { OtlService } from '../shared/olt.service';
import { ICompany } from '../../company/shared/ICompany';
import { CompanyService } from '../../company/shared/company.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-olt-save',
  templateUrl: './olt-save.component.html',
  styleUrls: ['./olt-save.component.scss']
})
export class OltSaveComponent implements OnInit {

  public olt: IOLT = {};

  public companyList : ICompany[];
  public selectedCompany: ICompany = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef,
    private companyService :CompanyService, private otlService: OtlService) { }

  ngOnInit(): void {
    this.getCompanyNames();    
  }

   //Method to get company names
   getCompanyNames(){
    try{
      this.companyService.getCompanyList().subscribe((res: any) => {
        this.companyList = res as ICompany[];
      });
    }
    catch(error){
      this.showMessageService.Error("getCompanyNames", 'Error while adding data : ' + error);
    }
  }

  //Method to add data
  AddOLT(){
    try{      
      this.olt.companyId = this.selectedCompany.companyId;
      this.otlService.InsertOLT(this.olt).subscribe((res : any) => {
        this.showMessageService.Success("OLT", res);
              this.olt = {};
              this.ref.close(true);
      }, err => {
              this.showMessageService.Error("OLT Save", 'Error : ' + err.error);  
      });
    }
    catch(error){
      this.showMessageService.Error("OLT", 'Error while adding data : ' + error);
    }
  }

}
