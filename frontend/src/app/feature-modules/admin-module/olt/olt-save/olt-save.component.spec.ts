import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OltSaveComponent } from './olt-save.component';

describe('OltSaveComponent', () => {
  let component: OltSaveComponent;
  let fixture: ComponentFixture<OltSaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OltSaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OltSaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
