import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OltListComponent } from './olt-list.component';

describe('OtlListComponent', () => {
  let component: OltListComponent;
  let fixture: ComponentFixture<OltListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OltListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OltListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
