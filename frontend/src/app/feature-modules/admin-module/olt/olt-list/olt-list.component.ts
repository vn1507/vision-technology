import { Component, OnInit } from '@angular/core';
import { IOLT } from '../shared/IOlt';
import { OtlService } from '../shared/olt.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { PrimengService } from '../../../../shared-service/primeng.service';
import { OltSaveComponent } from '../olt-save/olt-save.component';
import { OltUpdateComponent } from '../olt-update/olt-update.component';

@Component({
  selector: 'app-olt-list',
  templateUrl: './olt-list.component.html',
  styleUrls: ['./olt-list.component.scss'],
  providers:[PrimengService],
  entryComponents:[OltSaveComponent, OltUpdateComponent]
})
export class OltListComponent implements OnInit {

  public OLTList: IOLT[];
  public columns: any[];
  public loading: boolean;

  constructor(private showMessageService: ShowMessageService, private primengService: PrimengService, private otlService: OtlService) { 
    this.columns = [
      { field: 'companyName', header: 'Company Name' },
      { field: 'oltName', header: 'OLT Name ' },
      { field: 'location', header: 'Location' },
      { field: 'modelNo', header: 'Model No' },
      { field: 'ipAddress', header: 'IP Address ' },
      { field: 'macAddress', header: 'MAC Address ' },
      { field: 'sno', header: 'Sno ' },
      { field: 'oltId', header: '' },
    ];    
  }

  ngOnInit(): void {
    this.getOLTList();
  }

  //method to get data
  getOLTList(){
    try{
      this.loading = true;
      this.otlService.getOLTList().subscribe((res : any) => {
        this.OLTList = res as IOLT[];
        this.loading = false;
      });
    }
    catch(error){
      this.showMessageService.Error("OLT", 'Error while getting data : ' + error);
    }
  }  

  //method to open add dailog box
  AddDialog() {
    try {

      const ref = this.primengService.dialogService.open(OltSaveComponent, {
        header: 'Add OLT',
        width: '35%'
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getOLTList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("OLT", 'Error while opening add dialog  : ' + error);
    }
  }

  //method to open update dailog box
  UpdateDialog(olt: IOLT) {
    try {
      let selectedOLT = {
        oltId: olt.oltId,
        oltName: olt.oltName,
        companyId: olt.companyId,
        companyName: olt.companyName,
        ipAddress: olt.ipAddress,
        location: olt.location,
        macAddress: olt.macAddress,
        modelNo: olt.modelNo,
        sno: olt.sno,
        status: olt.status,
      };      

      const ref = this.primengService.dialogService.open(OltUpdateComponent, {
        header: 'Update OLT',
        width: '35%',
        data: selectedOLT
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getOLTList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("OLT", 'Error while opening add dialog : ' + error);
    }
  }

  //method to open delete dailog box
  DeleteDialog(oltId: number){

  }

}
