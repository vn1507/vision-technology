export interface IItemInward{
    inwardId ?;
    inwardDate ?;
    partyId ?;
    partyName ?;
    inwardDetailsList ?;
    inwardDetailList?;
    totalAmount ?;
    yearId?;
    yearName?;
}