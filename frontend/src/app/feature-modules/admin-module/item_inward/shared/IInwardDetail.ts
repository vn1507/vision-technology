export interface IItemInwardDetail{
    inwardId ?;
    srNo ?;
    companyId ?;
    companyName ?;
    productId ?;
    productName ?;
    rate ?;
    qty ?;
    GST ?;
    total ?;
}