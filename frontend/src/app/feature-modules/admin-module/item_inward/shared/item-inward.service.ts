import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IItemInward } from '../shared/IInward';
import { environment } from '../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ItemInwardService {

  constructor(private http: HttpClient) { }

  //Method to get response
  getInwardList(): Observable<IItemInward> {
    return this.http.get<IItemInward>(environment.webAPIURL + '/api/Inward/getInwardList')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  } 
  
  //Method to get qty by company ID and product ID
  getInwardByCompanyAndProductIdWise(companyId: number, productID: number, YearId: number): Observable<IItemInward> {    
    let availableQty = "companyId="+companyId + "&"+ "productId="+productID + "&"+ "YearId="+YearId;
    return this.http.get<IItemInward>(environment.webAPIURL + '/api/Inward/getInwardByCompanyAndProductIdWise?'+ availableQty)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  } 

  //Method to add
  InsertInward(inward: IItemInward): Observable<IItemInward> {        
    return this.http.post<IItemInward>(environment.webAPIURL + '/api/Inward/postInward', inward)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to update
  UpdateInward(selectedInward: IItemInward): Observable<IItemInward> {   
    debugger 
    return this.http.post<IItemInward>(environment.webAPIURL + '/api/Inward/updateInward', selectedInward)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

}
