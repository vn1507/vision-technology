import { TestBed } from '@angular/core/testing';

import { ItemInwardService } from './item-inward.service';

describe('ItemInwardService', () => {
  let service: ItemInwardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItemInwardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
