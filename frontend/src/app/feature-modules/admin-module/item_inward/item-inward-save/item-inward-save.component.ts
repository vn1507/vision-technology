import { Component, OnInit } from '@angular/core';
import { IItemInward } from '../shared/IInward';
import { IItemInwardDetail } from '../shared/IInwardDetail';
import { ItemInwardService } from '../shared/item-inward.service';
import { PartyService } from '../../party/shared/party.service';
import { IParty } from '../../party/shared/IParty';
import { CompanyService } from '../../company/shared/company.service';
import { ICompany } from '../../company/shared/ICompany';
import { ProductService } from '../../product/shared/product.service';
import { IProduct } from '../../product/shared/IProduct';
import { YearService } from '../../year/shared/year.service';
import { IYear } from '../../year/shared/IYear';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-item-inward-save',
  templateUrl: './item-inward-save.component.html',
  styleUrls: ['./item-inward-save.component.scss']
})
export class ItemInwardSaveComponent implements OnInit {

  public itemInward: IItemInward = {};

  public itemInwardDetailList = [];
  public itemInwardDetail: IItemInwardDetail = {};

  public displayList: boolean;

  public partyList: IParty[];
  public selectedParty: IParty = {};

  public companyList: ICompany[];
  public selectedCompany: ICompany = {};

  public productList: IProduct[];
  public selectedProduct: IProduct = {};
  
  public currentYear: IYear = {};
  public currentDate: any;

  public datePipe = new DatePipe('en-US');

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef,
    private partyService: PartyService, private companyService: CompanyService, private yearService: YearService,
    private productService: ProductService, private itemInwardService: ItemInwardService) { }

  ngOnInit(): void {
    this.getPartNames();
    this.getCompanyNames();
    this.getProductNames();
    this.itemInwardDetail.srNo = 1;    
    this.currentDate = new Date();
    this.currentDate = this.datePipe.transform(this.currentDate, 'yyyy-MM-dd');
    this.getYearByDate();  
  }

  //Method to get party names
  getPartNames() {
    try {
      this.partyService.getPartyData().subscribe((res: any) => {
        this.partyList = res as IParty[];
      });
    }
    catch (error) {
      this.showMessageService.Error("getPartNames", 'Error while adding data : ' + error);
    }
  }

  //Method to get year ID
  getYearByDate() {
    try {
      this.yearService.getYearListByDate(this.currentDate).subscribe((res: any) => {        
        this.currentYear = res as IYear;
      });
    }
    catch (error) {
      this.showMessageService.Error("getYearByDate", 'Error while getting data : ' + error);
    }
  }

  //Method to get company names
  getCompanyNames() {
    try {
      this.companyService.getCompanyList().subscribe((res: any) => {
        this.companyList = res as ICompany[];
      });
    }
    catch (error) {
      this.showMessageService.Error("getCompanyNames", 'Error while adding data : ' + error);
    }
  }

  //Method to get product names
  getProductNames() {
    try {
      this.productService.getProductData().subscribe((res: any) => {
        this.productList = res as IProduct[];
      });
    }
    catch (error) {
      this.showMessageService.Error("getProductNames", 'Error while adding data : ' + error);
    }
  }

  //Method to add item details
  addItem() {
    try {
      this.displayList = true;
      this.itemInwardDetail.companyId = this.selectedCompany.companyId;
      this.itemInwardDetail.companyName = this.selectedCompany.companyName;
      this.itemInwardDetail.productId = this.selectedProduct.productId;
      this.itemInwardDetail.productName = this.selectedProduct.productName;

      let itmeTotalWithoutGST = this.itemInwardDetail.qty * this.itemInwardDetail.rate;
      let totalWithGST = (itmeTotalWithoutGST * this.itemInwardDetail.GST) / 100;

      this.itemInwardDetail.total = totalWithGST + itmeTotalWithoutGST;

      this.itemInwardDetailList.push(this.itemInwardDetail);

      this.itemInwardDetail = {};
      this.itemInwardDetail.srNo = this.itemInwardDetailList.length + 1;


    }
    catch (error) {
      this.showMessageService.Error("addItem", 'Error while adding data : ' + error);
    }
  }

  //Method to remove item
  removeItem(srNo: number) {
    try {
      for (let i = 0; i < this.itemInwardDetailList.length; i++) {
        if (this.itemInwardDetailList[i]["srNo"] == srNo) {
          this.itemInwardDetailList.splice(i, 1);
        }
      }

      for (let i = 0; i < this.itemInwardDetailList.length; i++) {
        this.itemInwardDetailList[i]["srNo"] = (i + 1);
      }
      this.itemInwardDetail.srNo = this.itemInwardDetailList.length + 1;

    }
    catch (error) {
      this.showMessageService.Error("removeItem", 'Error while removing data : ' + error);
    }
  }

  // Validation of maxLength for number input field  
  onlyNumberKey(event) {
    if (event.target.value.length == 0 && event.which == 48) {
      return false;
    }
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 46 && event.charCode <= 57;
  }

  //Method to add data
  AddInward() {
    try {
      debugger
      this.itemInward.inwardDate = new Date(this.datePipe.transform(this.itemInward.inwardDate, 'yyyy-MM-dd'));
      this.itemInward.partyId = this.selectedParty.partyId;
      this.itemInward.yearId = this.currentYear[0].yearId;
      this.itemInward.inwardDetailsList = this.itemInwardDetailList;
      this.itemInwardService.InsertInward(this.itemInward).subscribe((res: any) => {
        this.showMessageService.Success("Inward", res);
        this.itemInward = {};
        this.ref.close(true);
      }, err => {
        this.showMessageService.Error("Inward Save", 'Error : ' + err.error);
      });
    }
    catch (error) {
      this.showMessageService.Error("Inward", 'Error while adding data : ' + error);
    }
  }
}
