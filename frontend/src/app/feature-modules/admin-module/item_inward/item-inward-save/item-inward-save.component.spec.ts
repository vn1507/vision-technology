import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemInwardSaveComponent } from './item-inward-save.component';

describe('ItemInwardSaveComponent', () => {
  let component: ItemInwardSaveComponent;
  let fixture: ComponentFixture<ItemInwardSaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemInwardSaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemInwardSaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
