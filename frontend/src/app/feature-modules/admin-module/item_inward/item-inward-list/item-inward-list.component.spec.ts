import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemInwardListComponent } from './item-inward-list.component';

describe('ItemInwardListComponent', () => {
  let component: ItemInwardListComponent;
  let fixture: ComponentFixture<ItemInwardListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemInwardListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemInwardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
