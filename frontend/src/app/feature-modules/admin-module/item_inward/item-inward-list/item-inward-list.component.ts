import { Component, OnInit } from '@angular/core';
import { IItemInward } from '../shared/IInward';
import { ItemInwardService } from '../shared/item-inward.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { PrimengService } from '../../../../shared-service/primeng.service';
import { ItemInwardSaveComponent } from '../item-inward-save/item-inward-save.component';
import { ItemInwardUpdateComponent } from '../item-inward-update/item-inward-update.component';

@Component({
  selector: 'app-item-inward-list',
  templateUrl: './item-inward-list.component.html',
  styleUrls: ['./item-inward-list.component.scss'],  
  providers: [PrimengService],
  entryComponents: [ItemInwardSaveComponent, ItemInwardUpdateComponent]
})
export class ItemInwardListComponent implements OnInit {

  public itemInwardList: IItemInward[];
  public columns: any[];
  public loading: boolean;

  constructor(private showMessageService: ShowMessageService, private primengService: PrimengService,
              private itemInwardService: ItemInwardService) { 
    this.columns = [
      { field: 'inwardId', header: 'Inward ID' },
      { field: 'inwardDate', header: 'Date Time' },
      { field: 'partyName', header: 'Party Name' },
      { field: 'yearName', header: 'Year Name' },
      { field: 'yearId', header: 'Year ID' },
      { field: 'inwardId', header: '' },
    ];
  }

  ngOnInit(): void {
    this.getInwardList();
  }

  //method to get data
  getInwardList(){
    try{
      this.loading = true;
      this.itemInwardService.getInwardList().subscribe((res : any) => {
        this.itemInwardList = res as IItemInward[];
        this.loading = false;        
      });
    }
    catch(error){
      this.showMessageService.Error("Inward", 'Error while getting data : ' + error);
    }
  }

  //method to open add dailog box
  AddDialog() {
    try {
      const ref = this.primengService.dialogService.open(ItemInwardSaveComponent, {
        header: 'Add Inward',
        width: '70%',
        contentStyle: { "overflow": "visible" },
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getInwardList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Inward", 'Error while opening add dialog : ' + error);
    }
  }

  //method to open update dailog box
  UpdateDialog(itemInward: IItemInward) {
    try {
      let selectedIward = {
        inwardId: itemInward.inwardId,
        inwardDate: itemInward.inwardDate,
        partyId: itemInward.partyId,
        partyName: itemInward.partyName,
        yearId: itemInward.yearId,
        yearName: itemInward.yearName,
        inwardDetailList: itemInward.inwardDetailsList
      };
      const ref = this.primengService.dialogService.open(ItemInwardUpdateComponent, {
        header: 'Update Inward',
        width: '70%',
        data: selectedIward,
        contentStyle: { "overflow": "visible" },
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getInwardList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Inward", 'Error while opening add dialog : ' + error);
    }
  }

   //method to open delete dailog box
   DeleteDialog(inwardId: number){

  }

}
