import { Component, OnInit } from '@angular/core';
import { IItemInward } from '../shared/IInward';
import { IItemInwardDetail } from '../shared/IInwardDetail';
import { ItemInwardService } from '../shared/item-inward.service';
import { PartyService } from '../../party/shared/party.service';
import { IParty } from '../../party/shared/IParty';
import { CompanyService } from '../../company/shared/company.service';
import { ICompany } from '../../company/shared/ICompany';
import { ProductService } from '../../product/shared/product.service';
import { IProduct } from '../../product/shared/IProduct';
import { YearService } from '../../year/shared/year.service';
import { IYear } from '../../year/shared/IYear';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-item-inward-update',
  templateUrl: './item-inward-update.component.html',
  styleUrls: ['./item-inward-update.component.scss']
})
export class ItemInwardUpdateComponent implements OnInit {

  public selectedItemInward: IItemInward = {};
  
  public selectedIItemInwardDetailList = [];
  public itemInwardDetail: IItemInwardDetail = {};

  public displayList: boolean;

  public partyList : IParty[];
  public selectedParty: IParty = {};

  public companyList : ICompany[];
  public selectedCompany: ICompany = {};

  public productList : IProduct[];
  public selectedProduct: IProduct = {};

  public currentYear: IYear = {};
  public currentDate: any;

  public datePipe = new DatePipe('en-US');

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, private config:DynamicDialogConfig, 
    private partyService: PartyService, private companyService :CompanyService,  private yearService: YearService,
    private productService: ProductService, private itemInwardService: ItemInwardService) { }

  ngOnInit(): void {        
    this.selectedItemInward = this.config.data;
    console.log('this.selectedItemInward',this.selectedItemInward);
    this.selectedIItemInwardDetailList = this.selectedItemInward.inwardDetailList;
    this.getPartNames();
    this.getCompanyNames();
    this.getProductNames();
    this.itemInwardDetail.srNo = this.selectedIItemInwardDetailList.length + 1;
    this.selectedItemInward.inwardDate = new Date(this.datePipe.transform(this.selectedItemInward.inwardDate, 'dd-MMM-yyyy'));
  }

   //Method to get party names
   getPartNames(){
    try{
      this.partyService.getPartyData().subscribe((res: any) => {
        this.partyList = res as IParty[];
        this.selectedParty.partyId = this.selectedItemInward.partyId;
        this.selectedParty.partyName = this.selectedItemInward.partyName;
      });
    }
    catch(error){
      this.showMessageService.Error("getPartNames", 'Error while adding data : ' + error);
    }
  }

  //Method to get company names
  getCompanyNames(){
    try{
      this.companyService.getCompanyList().subscribe((res: any) => {
        this.companyList = res as ICompany[];
        this.selectedCompany.companyId = this.selectedItemInward.inwardDetailsList;
        this.selectedCompany.companyName = this.selectedItemInward.inwardDetailsList;
      });
    }
    catch(error){
      this.showMessageService.Error("getCompanyNames", 'Error while adding data : ' + error);
    }
  }

  //Method to get product names
  getProductNames(){
    try{
      this.productService.getProductData().subscribe((res: any) => {
        this.productList = res as IProduct[];
        this.selectedProduct.productId = this.itemInwardDetail.productId;
        this.selectedProduct.productName = this.itemInwardDetail.productName;
      });
    }
    catch(error){
      this.showMessageService.Error("getProductNames", 'Error while adding data : ' + error);
    }
  }

  //Method to add item details
  addItem(){
    try{                 
      this.itemInwardDetail.companyId = this.selectedCompany.companyId;
      this.itemInwardDetail.companyName = this.selectedCompany.companyName;
      this.itemInwardDetail.productId = this.selectedProduct.productId;
      this.itemInwardDetail.productName = this.selectedProduct.productName;

      let itmeTotalWithoutGST = this.itemInwardDetail.qty * this.itemInwardDetail.rate;
      let totalWithGST = (itmeTotalWithoutGST * this.itemInwardDetail.GST) / 100;

      this.itemInwardDetail.total = totalWithGST + itmeTotalWithoutGST;

      this.selectedIItemInwardDetailList.push(this.itemInwardDetail);   
      
      this.itemInwardDetail = {};      
      this.itemInwardDetail.srNo=this.selectedIItemInwardDetailList.length+1;
    }
    catch(error){
      this.showMessageService.Error("addItem", 'Error while adding data : ' + error);
    }
  }

  //Method to remove item
  removeItem(srNo: number){
    try{
      for(let i = 0; i < this.selectedIItemInwardDetailList.length; i++){
        if(this.selectedIItemInwardDetailList[i]["srNo"] == srNo){
          this.selectedIItemInwardDetailList.splice(i, 1);
        }
      }
      
      for(let i = 0; i < this.selectedIItemInwardDetailList.length; i++){
      this.selectedIItemInwardDetailList[i]["srNo"]=(i+1);
        }
        this.itemInwardDetail.srNo=this.selectedIItemInwardDetailList.length+1;
      
    }
    catch(error){
      this.showMessageService.Error("removeItem", 'Error while removing data : ' + error);
    }
  }

   // Validation of maxLength for number input field  
   onlyNumberKey(event) {
    if (event.target.value.length == 0 && event.which == 48) {
      return false;
    }
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 46 && event.charCode <= 57;
  }
  
   //Method to update data
   UpdateInward(){    
    try{
      debugger
      this.selectedItemInward.inwardDate = new Date(this.datePipe.transform(this.selectedItemInward.inwardDate, 'dd-MMM-yyyy'));
      this.selectedItemInward.partyId = this.selectedParty.partyId;
      this.selectedItemInward.inwardDetailsList = this.selectedIItemInwardDetailList;
      this.itemInwardService.UpdateInward(this.selectedItemInward).subscribe((res : any) => {
        this.showMessageService.Success("Inward", res);
              this.selectedItemInward = {};
              this.ref.close(true);
      }, err => {
              this.showMessageService.Error("Inward Update", 'Error : ' + JSON.stringify(err.error));  
      });
    }
    catch(error){
      this.showMessageService.Error("Inward", 'Error while adding data : ' + error);
    }
  }

}
