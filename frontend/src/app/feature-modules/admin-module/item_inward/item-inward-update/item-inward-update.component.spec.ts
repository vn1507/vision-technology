import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemInwardUpdateComponent } from './item-inward-update.component';

describe('ItemInwardUpdateComponent', () => {
  let component: ItemInwardUpdateComponent;
  let fixture: ComponentFixture<ItemInwardUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemInwardUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemInwardUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
