import { Component, OnInit } from '@angular/core';
import { IOutward } from '../shared/IOutward';
import { ItemOutwardService } from '../shared/item-outward.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { PrimengService } from '../../../../shared-service/primeng.service';
import { ItemOutwardSaveComponent } from '../item-outward-save/item-outward-save.component';
import { ItemOutwardUpdateComponent } from '../item-outward-update/item-outward-update.component';

@Component({
  selector: 'app-item-outward-list',
  templateUrl: './item-outward-list.component.html',
  styleUrls: ['./item-outward-list.component.scss'],  
  providers: [PrimengService],
  entryComponents: [ItemOutwardSaveComponent, ItemOutwardUpdateComponent]
})
export class ItemOutwardListComponent implements OnInit {

  public itemOutwardList: IOutward[];
  public columns: any[];
  public loading: boolean;

  constructor(private showMessageService: ShowMessageService, private primengService: PrimengService,
              private itemOutwardService: ItemOutwardService) { 
    this.columns = [
      { field: 'outwardId', header: 'Outward ID' },
      { field: 'outwardDate', header: 'Date Time' },
      { field: 'employeeName', header: 'Employee Name' },
      { field: 'outwardId', header: '' },
    ];
  }

  ngOnInit(): void {
    this.getOutwardList();
  }

  //method to get data
  getOutwardList(){
    try{
      this.loading = true;
      this.itemOutwardService.getOutwardList().subscribe((res : any) => {
        this.itemOutwardList = res as IOutward[];
        this.loading = false;     
      });
    }
    catch(error){
      this.showMessageService.Error("Outward", 'Error while getting data : ' + error);
    }
  }

   //method to open add dailog box
   AddDialog() {
    try {
      const ref = this.primengService.dialogService.open(ItemOutwardSaveComponent, {
        header: 'Add Outward',
        width: '70%',
        contentStyle: { "overflow": "visible" },
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getOutwardList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Outward", 'Error while opening add dialog : ' + error);
    }
  }

  //method to open update dailog box
  UpdateDialog(itemOutward: IOutward) {
    try {      
      let selectedOutward = {
        outwardId: itemOutward.outwardId,
        outwardDate: itemOutward.outwardDate,
        employeeId: itemOutward.employeeId,
        employeeName: itemOutward.employeeName,
        yearId: itemOutward.yearId,
        yearName: itemOutward.yearName,
        outwardDetailsList: itemOutward.outwardDetailsList
      };
      const ref = this.primengService.dialogService.open(ItemOutwardUpdateComponent, {
        header: 'Update Outward',
        width: '70%',
        data: selectedOutward,
        contentStyle: { "overflow": "visible" },
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getOutwardList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Outward", 'Error while opening update dialog : ' + error);
    }
  }

   //method to open delete dailog box
   DeleteDialog(outwardId: number){

  }

}
