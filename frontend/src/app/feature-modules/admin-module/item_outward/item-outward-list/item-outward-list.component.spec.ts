import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemOutwardListComponent } from './item-outward-list.component';

describe('ItemOutwardListComponent', () => {
  let component: ItemOutwardListComponent;
  let fixture: ComponentFixture<ItemOutwardListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemOutwardListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemOutwardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
