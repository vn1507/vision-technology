import { TestBed } from '@angular/core/testing';

import { ItemOutwardService } from './item-outward.service';

describe('ItemOutwardService', () => {
  let service: ItemOutwardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItemOutwardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
