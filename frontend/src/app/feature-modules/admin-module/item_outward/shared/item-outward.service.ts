import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IOutward } from '../shared/IOutward';
import { environment } from '../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ItemOutwardService {

  constructor(private http: HttpClient) { }

  //Method to get response
  getOutwardList(): Observable<IOutward> {
    return this.http.get<IOutward>(environment.webAPIURL + '/api/Outward/getOutwardList')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }  

  //Method to add
  InsertOutward(outward: IOutward): Observable<IOutward> {        
    return this.http.post<IOutward>(environment.webAPIURL + '/api/Outward/postOutward', outward)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to update
  UpdateOutward(selectedOutward: IOutward): Observable<IOutward> {    
    return this.http.post<IOutward>(environment.webAPIURL + '/api/Outward/updateOutward', selectedOutward)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

}
