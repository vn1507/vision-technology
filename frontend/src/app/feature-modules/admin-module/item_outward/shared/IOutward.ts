export interface IOutward{
    outwardId ?;
    outwardDate  ?;
    employeeId ?;
    employeeName ?;
    outwardDetailsList?;
    yearId?;
    yearName?;
}