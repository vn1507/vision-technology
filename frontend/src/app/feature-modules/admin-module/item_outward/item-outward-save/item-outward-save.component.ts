import { Component, OnInit } from '@angular/core';
import { IOutward } from '../shared/IOutward';
import { IOutwardDetail } from '../shared/IOutwardDetail';
import { ItemOutwardService } from '../shared/item-outward.service';
import { ItemInwardService } from '../../item_inward/shared/item-inward.service';
import { IEmployee } from '../../../employee-module/employee/shared/IEmployee';
import { EmployeeService } from '../../../employee-module/employee/shared/employee.service';
import { CompanyService } from '../../company/shared/company.service';
import { ICompany } from '../../company/shared/ICompany';
import { ProductService } from '../../product/shared/product.service';
import { IProduct } from '../../product/shared/IProduct';
import { YearService } from '../../year/shared/year.service';
import { IYear } from '../../year/shared/IYear';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-item-outward-save',
  templateUrl: './item-outward-save.component.html',
  styleUrls: ['./item-outward-save.component.scss']
})
export class ItemOutwardSaveComponent implements OnInit {

  public itemOutward: IOutward = {};

  public itemOutwardDetailList = [];
  public itemOutwardDetail: IOutwardDetail = {};

  public displayList: boolean;

  public employeeList: IEmployee[];
  public selectedEmployee: IEmployee = {};

  public companyList: ICompany[];
  public selectedCompany: ICompany = {};

  public productList: IProduct[];
  public selectedProduct: IProduct = {};

  public currentYear: IYear = {};
  public currentDate: any;

  public datePipe = new DatePipe('en-US');

  public disableProduct: boolean = true;

  public currentComapnyID: any;
  public avaialbeQty = 0;

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef,
    private employeeService: EmployeeService, private companyService: CompanyService,
    private productService: ProductService, private itemOutwardService: ItemOutwardService,
    private itemInwardService: ItemInwardService, private yearService: YearService) { }

  ngOnInit(): void {
    this.getEmployeeNames();
    this.getCompanyNames();
    this.getProductNames();
    this.itemOutwardDetail.srNo = 1;
    this.currentDate = new Date();
    this.currentDate = this.datePipe.transform(this.currentDate, 'yyyy-MM-dd');
    this.getYearByDate();  
  }

  //Method to get year ID
  getYearByDate() {
    try {
      this.yearService.getYearListByDate(this.currentDate).subscribe((res: any) => {        
        this.currentYear = res as IYear;
      });
    }
    catch (error) {
      this.showMessageService.Error("getYearByDate", 'Error while getting data : ' + error);
    }
  }

  //Method to get employee names
  getEmployeeNames() {
    try {
      this.employeeService.getEmployeeData().subscribe((res: any) => {
        this.employeeList = res as IEmployee[];
      });
    }
    catch (error) {
      this.showMessageService.Error("employeeList", 'Error while adding data : ' + error);
    }
  }

  //Method to get company names
  getCompanyNames() {
    try {
      this.companyService.getCompanyList().subscribe((res: any) => {
        this.companyList = res as ICompany[];
      });
    }
    catch (error) {
      this.showMessageService.Error("getCompanyNames", 'Error while adding data : ' + error);
    }
  }

  //Method to get product names
  getProductNames() {
    try {
      this.productService.getProductData().subscribe((res: any) => {
        this.productList = res as IProduct[];
      });
    }
    catch (error) {
      this.showMessageService.Error("getProductNames", 'Error while adding data : ' + error);
    }
  }

  //method to enable product
  onCompanyChange(companyId: number) {
    if (companyId) {
      this.disableProduct = false;
      this.currentComapnyID = companyId;
    } else {
      this.disableProduct = true;
    }
  }

  //method to get quantity
  getAvailableQty(productId: number) {
    debugger
    let companyID = this.currentComapnyID;
    let yearId = this.currentYear[0].yearId;
    this.itemInwardService.getInwardByCompanyAndProductIdWise(companyID, productId, yearId).subscribe((res: any) => {
      this.avaialbeQty = res;
    });

  }

  //Method to add item details
  addItem() {
    try {
      if (this.itemOutwardDetail.qty > this.avaialbeQty) {
        this.showMessageService.Error("Quantity", 'Required Qty is greated than Availabe Qty');
        return false;
      }
      this.displayList = true;
      this.itemOutwardDetail.companyId = this.selectedCompany.companyId;
      this.itemOutwardDetail.companyName = this.selectedCompany.companyName;
      this.itemOutwardDetail.productId = this.selectedProduct.productId;
      this.itemOutwardDetail.productname = this.selectedProduct.productName;

      this.avaialbeQty = this.avaialbeQty - this.itemOutwardDetail.qty;

      this.itemOutwardDetailList.push(this.itemOutwardDetail);
      

      this.itemOutwardDetail = {};
      this.itemOutwardDetail.srNo = this.itemOutwardDetailList.length + 1;


    }
    catch (error) {
      this.showMessageService.Error("addItemOutward", 'Error while adding data : ' + error);
    }
  }

  //Method to remove item
  removeItem(srNo: number) {
    try {
      for (let i = 0; i < this.itemOutwardDetailList.length; i++) {
        if (this.itemOutwardDetailList[i]["srNo"] == srNo) {
          this.itemOutwardDetailList.splice(i, 1);
        }
      }

      for (let i = 0; i < this.itemOutwardDetailList.length; i++) {
        this.itemOutwardDetailList[i]["srNo"] = (i + 1);
      }
      this.itemOutwardDetail.srNo = this.itemOutwardDetailList.length + 1;

    }
    catch (error) {
      this.showMessageService.Error("removeItem", 'Error while removing data : ' + error);
    }
  }

  // Validation of maxLength for number input field  
  onlyNumberKey(event) {
    if (event.target.value.length == 0 && event.which == 48) {
      return false;
    }
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 46 && event.charCode <= 57;
  }

  //Method to add data
  AddOutward() {
    try {      
      this.itemOutward.outwardDate = new Date(this.datePipe.transform(this.itemOutward.outwardDate, 'yyyy-MM-dd'));
      this.itemOutward.employeeId = this.selectedEmployee.employeeId;
      this.itemOutward.outwardDetailsList = this.itemOutwardDetailList;
      this.itemOutwardService.InsertOutward(this.itemOutward).subscribe((res: any) => {
        this.showMessageService.Success("Outward", res);
        this.itemOutward = {};
        this.ref.close(true);
      }, err => {
        this.showMessageService.Error("Outward Save", 'Error : ' + err.error);
      });
    }
    catch (error) {
      this.showMessageService.Error("Outward", 'Error while adding data : ' + error);
    }
  }

}
