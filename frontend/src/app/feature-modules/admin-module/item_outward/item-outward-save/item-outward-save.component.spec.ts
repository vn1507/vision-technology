import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemOutwardSaveComponent } from './item-outward-save.component';

describe('ItemOutwardSaveComponent', () => {
  let component: ItemOutwardSaveComponent;
  let fixture: ComponentFixture<ItemOutwardSaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemOutwardSaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemOutwardSaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
