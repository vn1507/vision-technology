import { Component, OnInit } from '@angular/core';
import { IOutward } from '../shared/IOutward';
import { IOutwardDetail } from '../shared/IOutwardDetail';
import { ItemOutwardService } from '../shared/item-outward.service';
import { ItemInwardService } from '../../item_inward/shared/item-inward.service';
import { IEmployee } from '../../../employee-module/employee/shared/IEmployee';
import { EmployeeService } from '../../../employee-module/employee/shared/employee.service';
import { CompanyService } from '../../company/shared/company.service';
import { ICompany } from '../../company/shared/ICompany';
import { ProductService } from '../../product/shared/product.service';
import { IProduct } from '../../product/shared/IProduct';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-item-outward-update',
  templateUrl: './item-outward-update.component.html',
  styleUrls: ['./item-outward-update.component.scss']
})
export class ItemOutwardUpdateComponent implements OnInit {

  public selectedItemOutward: IOutward = {};

  public selectedIItemOutwardDetailList = [];
  public itemOutwardDetail: IOutwardDetail = {};

  public displayList: boolean;

  public employeeList: IEmployee[];
  public selectedEmployee: IEmployee = {};

  public companyList: ICompany[];
  public selectedCompany: ICompany = {};

  public productList: IProduct[];
  public selectedProduct: IProduct = {};

  public datePipe = new DatePipe('en-US');

  public disableProduct: boolean = true;

  public currentComapnyID: any;
  public avaialbeQty = 0;

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, 
    private config: DynamicDialogConfig, private itemInwardService: ItemInwardService,
    private employeeService: EmployeeService, private companyService: CompanyService,
    private productService: ProductService, private itemOutwardService: ItemOutwardService) { }

  ngOnInit(): void {
    this.selectedItemOutward = this.config.data;
    this.selectedItemOutward.outwardDate = new Date(this.datePipe.transform(this.selectedItemOutward.outwardDate, 'dd-MMM-yyyy'));
    this.selectedIItemOutwardDetailList = this.selectedItemOutward.outwardDetailsList;
    this.getEmployeeNames();
    this.getCompanyNames();
    this.getProductNames();
    this.itemOutwardDetail.srNo = this.selectedIItemOutwardDetailList.length + 1;    
  }

  //Method to get employee names
  getEmployeeNames() {
    try {
      this.employeeService.getEmployeeData().subscribe((res: any) => {
        this.employeeList = res as IEmployee[];
        this.selectedEmployee.employeeId = this.selectedItemOutward.employeeId;
        this.selectedEmployee.employeeName = this.selectedItemOutward.employeeName;
      });
    }
    catch (error) {
      this.showMessageService.Error("employeeList", 'Error while adding data : ' + error);
    }
  }

  //Method to get company names
  getCompanyNames() {
    try {
      this.companyService.getCompanyList().subscribe((res: any) => {
        this.companyList = res as ICompany[];
        this.selectedCompany.companyId = this.selectedItemOutward.outwardDetailsList;
        this.selectedCompany.companyName = this.selectedItemOutward.outwardDetailsList;
      });
    }
    catch (error) {
      this.showMessageService.Error("getCompanyNames", 'Error while adding data : ' + error);
    }
  }

  //Method to get product names
  getProductNames() {
    try {
      this.productService.getProductData().subscribe((res: any) => {
        this.productList = res as IProduct[];
        this.selectedProduct.productId = this.itemOutwardDetail.productId;
        this.selectedProduct.productName = this.itemOutwardDetail.productname;
      });
    }
    catch (error) {
      this.showMessageService.Error("getProductNames", 'Error while adding data : ' + error);
    }
  }

  //method to enable product
  onCompanyChange(companyId: number) {
    if (companyId) {
      this.disableProduct = false;
      this.currentComapnyID = companyId;
    } else {
      this.disableProduct = true;
    }
  }

  //method to get quantity
  getAvailableQty(productId: number) {
    let companyID = this.currentComapnyID;
    let yearId = this.selectedItemOutward.yearId;
    this.itemInwardService.getInwardByCompanyAndProductIdWise(companyID, productId, yearId).subscribe((res: any) => {
      this.avaialbeQty = res;
    });

  }

  //Method to add item details
  addItem() {
    try {
      try {
        if (this.itemOutwardDetail.qty > this.avaialbeQty) {
          this.showMessageService.Error("Quantity", 'Required Qty is greated than Availabe Qty');
          return false;
        }
        this.displayList = true;
        this.itemOutwardDetail.companyId = this.selectedCompany.companyId;
        this.itemOutwardDetail.companyName = this.selectedCompany.companyName;
        this.itemOutwardDetail.productId = this.selectedProduct.productId;
        this.itemOutwardDetail.productname = this.selectedProduct.productName;
  
        this.avaialbeQty = this.avaialbeQty - this.itemOutwardDetail.qty;
  
        this.selectedIItemOutwardDetailList.push(this.itemOutwardDetail);
        
  
        this.itemOutwardDetail = {};
        this.itemOutwardDetail.srNo = this.selectedIItemOutwardDetailList.length + 1;
  
  
      }
      catch (error) {
        this.showMessageService.Error("addItemOutward", 'Error while adding data : ' + error);
      }
    }
    catch (error) {
      this.showMessageService.Error("addItem", 'Error while adding data : ' + error);
    }
  }

  //Method to remove item
  removeItem(srNo: number) {
    try {
      for (let i = 0; i < this.selectedIItemOutwardDetailList.length; i++) {
        if (this.selectedIItemOutwardDetailList[i]["srNo"] == srNo) {
          this.selectedIItemOutwardDetailList.splice(i, 1);
        }
      }

      for (let i = 0; i < this.selectedIItemOutwardDetailList.length; i++) {
        this.selectedIItemOutwardDetailList[i]["srNo"] = (i + 1);
      }
      this.itemOutwardDetail.srNo = this.selectedIItemOutwardDetailList.length + 1;

    }
    catch (error) {
      this.showMessageService.Error("removeItem", 'Error while removing data : ' + error);
    }
  }

  // Validation of maxLength for number input field  
  onlyNumberKey(event) {
    if (event.target.value.length == 0 && event.which == 48) {
      return false;
    }
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 46 && event.charCode <= 57;
  }

  //Method to update data
  UpdateOutward() {
    try {
      this.selectedItemOutward.outwardDate = new Date(this.datePipe.transform(this.selectedItemOutward.outwardDate, 'dd-MMM-yyyy HH:mm'));
      this.selectedItemOutward.employeeId = this.selectedEmployee.employeeId;
      this.selectedItemOutward.outwardDetailsList = this.selectedIItemOutwardDetailList;
      this.itemOutwardService.UpdateOutward(this.selectedItemOutward).subscribe((res: any) => {
        this.showMessageService.Success("Outward", res);
        this.selectedItemOutward = {};
        this.ref.close(true);
      }, err => {
        this.showMessageService.Error("Outward Update", 'Error : ' + JSON.stringify(err.error));
      });
    }
    catch (error) {
      this.showMessageService.Error("Outward", 'Error while adding data : ' + error);
    }
  }

}
