import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemOutwardUpdateComponent } from './item-outward-update.component';

describe('ItemOutwardUpdateComponent', () => {
  let component: ItemOutwardUpdateComponent;
  let fixture: ComponentFixture<ItemOutwardUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemOutwardUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemOutwardUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
