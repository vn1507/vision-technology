import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpliterListComponent } from './spliter-list.component';

describe('SpliterListComponent', () => {
  let component: SpliterListComponent;
  let fixture: ComponentFixture<SpliterListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpliterListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpliterListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
