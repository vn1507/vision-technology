import { Component, OnInit } from '@angular/core';
import { ISpliter } from '../shared/ISpliter';
import { SpliterService } from '../shared/spliter.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { PrimengService } from '../../../../shared-service/primeng.service';
import { SpliterSaveComponent } from '../spliter-save/spliter-save.component';
import { SpliterUpdateComponent } from '../spliter-update/spliter-update.component';

@Component({
  selector: 'app-spliter-list',
  templateUrl: './spliter-list.component.html',
  styleUrls: ['./spliter-list.component.scss'],
  providers: [PrimengService],
  entryComponents:[SpliterSaveComponent, SpliterUpdateComponent]
})
export class SpliterListComponent implements OnInit {

  public spliterList: ISpliter[];
  public columns: any[];
  public loading: boolean;

  constructor(private showMessageService: ShowMessageService, private primengService: PrimengService, private spliterService: SpliterService) { 
    this.columns = [
      { field: 'spliterName', header: 'Spliter Name' },
      { field: 'oltName', header: 'OLT Name' },
      { field: 'spliterLocation', header: 'Spliter Location' },
      { field: 'spliterLength', header: 'Spliter Length' },
      { field: 'spliterId', header: '' },
    ];
  }

  ngOnInit(): void {
    this.getSpliterList();
  }

  //method to get data
  getSpliterList(){
    try{
      this.loading = true;
      this.spliterService.getSpliterList().subscribe((res : any) => {
        this.spliterList = res as ISpliter[];
        this.loading = false;
      });
    }
    catch(error){
      this.showMessageService.Error("Spliter", 'Error while getting data : ' + error);
    }
  }

  //method to open add dailog box
  AddDialog() {
    try {

      const ref = this.primengService.dialogService.open(SpliterSaveComponent, {
        header: 'Add Slipter',
        width: "20%"
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getSpliterList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Slipter", 'Error while opening add dialog  : ' + error);
    }
  }

  //method to open update dailog box
  UpdateDialog(spliter: ISpliter) {
    try {
      let selectedSpliter = {
        spliterId: spliter.spliterId,
        spliterName: spliter.spliterName,
        oltId: spliter.oltId,
        oltName: spliter.oltName,
        spliterLength: spliter.spliterLength,
        spliterLocation: spliter.spliterLocation,
      };      

      const ref = this.primengService.dialogService.open(SpliterUpdateComponent, {
        header: 'Update Slipter',
        data: selectedSpliter,
        width: "20%"
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getSpliterList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Slipter", 'Error while opening add dialog : ' + error);
    }
  }

  //method to open delete dailog box
  DeleteDialog(spliterId: number){

  }

}
