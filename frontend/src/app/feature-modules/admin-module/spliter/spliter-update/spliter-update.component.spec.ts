import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpliterUpdateComponent } from './spliter-update.component';

describe('SpliterUpdateComponent', () => {
  let component: SpliterUpdateComponent;
  let fixture: ComponentFixture<SpliterUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpliterUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpliterUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
