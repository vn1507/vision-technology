import { Component, OnInit } from '@angular/core';
import { OtlService } from '../../olt/shared/olt.service';
import { IOLT } from '../../olt/shared/IOlt';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ISpliter } from '../shared/ISpliter';
import { SpliterService } from '../shared/spliter.service';

@Component({
  selector: 'app-spliter-update',
  templateUrl: './spliter-update.component.html',
  styleUrls: ['./spliter-update.component.scss']
})
export class SpliterUpdateComponent implements OnInit {

  public oltList: IOLT[];
  public selectedOLT: IOLT = {};
  
  public selectedSpliter: ISpliter = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, private config: DynamicDialogConfig,
    private otlService: OtlService, private spliterService: SpliterService) { }

  ngOnInit(): void {
    this.selectedSpliter = this.config.data;
    this.getOLTNames();
  }

  //method to fetch unitname
  getOLTNames(){
    try{
      this.otlService.getOLTNames().subscribe((res: any) => {
        this.oltList = res as IOLT[];
        this.selectedOLT.oltId = this.selectedSpliter.oltId;
        this.selectedOLT.oltName = this.selectedSpliter.oltName;  
      }, err => {
              this.showMessageService.Error("OLT Name", 'Error : ' + err.error);  
      });
    }
    catch(error){
      this.showMessageService.Error("OLT", 'Error while adding data : ' + error);
    }
  }

   //method to save data
   UpdateSpliter(){
    try{      
      this.selectedSpliter.oltId = this.selectedOLT.oltId;      
      this.spliterService.InsertSpliter(this.selectedSpliter).subscribe((res: any) => {
        this.showMessageService.Success("Spliter", res);
              this.selectedSpliter = {};
              this.ref.close(true);
      }, err => {
              this.showMessageService.Error("Spliter Update", 'Error : ' + JSON.stringify(err.error));  
      });
    }
    catch(error){
      this.showMessageService.Error("Spliter", 'Error while updating data : ' + error);
    }
  }

}
