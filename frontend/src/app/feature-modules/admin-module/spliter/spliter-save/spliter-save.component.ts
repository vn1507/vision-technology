import { Component, OnInit } from '@angular/core';
import { OtlService } from '../../olt/shared/olt.service';
import { IOLT } from '../../olt/shared/IOlt';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { ISpliter } from '../shared/ISpliter';
import { SpliterService } from '../shared/spliter.service';

@Component({
  selector: 'app-spliter-save',
  templateUrl: './spliter-save.component.html',
  styleUrls: ['./spliter-save.component.scss']
})
export class SpliterSaveComponent implements OnInit {

  public oltList: IOLT[];
  public selectedOLT: IOLT = {};
  
  public spliter: ISpliter = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, 
    private otlService: OtlService, private spliterService: SpliterService) { }

  ngOnInit(): void {
    this.getOLTNames();
  }

  //method to fetch unitname
  getOLTNames(){
    try{      
      this.otlService.getOLTNames().subscribe((res: any) => {
        this.oltList = res as IOLT[];    
      }, err => {
              this.showMessageService.Error("OLT Name", 'Error : ' + err.error);  
      });
    }
    catch(error){
      this.showMessageService.Error("OLT", 'Error while adding data : ' + error);
    }
  }

  //method to save data
  SaveSpliter(){
    try{
      this.spliter.oltId = this.selectedOLT.oltId;
      this.spliterService.InsertSpliter(this.spliter).subscribe((res: any) => {
        this.showMessageService.Success("Spliter", res);
              this.spliter = {};
              this.ref.close(true);
      }, err => {
              this.showMessageService.Error("Spliter Save", 'Error : ' + err.error);  
      });
    }
    catch(error){
      this.showMessageService.Error("Spliter", 'Error while adding data : ' + error);
    }
  }

}
