import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpliterSaveComponent } from './spliter-save.component';

describe('SpliterSaveComponent', () => {
  let component: SpliterSaveComponent;
  let fixture: ComponentFixture<SpliterSaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpliterSaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpliterSaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
