import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ISpliter } from '../shared/ISpliter';
import { environment } from '../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpliterService {

  constructor(private http: HttpClient) { }

  //Method to get response
  getSpliterList(): Observable<ISpliter> {
    return this.http.get<ISpliter>(environment.webAPIURL + '/api/Spliter/getSpliterList')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to single spliter by oltId response
  getSpliterByOltIdWise(OltId: number): Observable<ISpliter> {
    return this.http.get<ISpliter>(environment.webAPIURL + '/api/Spliter/getSpliterByOltIdWise?OltId=' + OltId)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }
  //

  //Method to add
  InsertSpliter(spliter: ISpliter): Observable<ISpliter> {
    return this.http.post<ISpliter>(environment.webAPIURL + '/api/Spliter/postSpliter', spliter)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to Update
  UpdateSpliter(selectedSpliter: ISpliter): Observable<ISpliter> {
    return this.http.post<ISpliter>(environment.webAPIURL + '/api/Spliter/updateSpliter', selectedSpliter)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

}
