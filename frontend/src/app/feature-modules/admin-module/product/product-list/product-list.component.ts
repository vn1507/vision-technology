import { Component, OnInit } from '@angular/core';
import { IProduct } from '../shared/IProduct';
import { ProductService } from '../shared/product.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { PrimengService } from '../../../../shared-service/primeng.service';
import { ProductSaveComponent } from '../product-save/product-save.component';
import { ProductUpdateComponent } from '../product-update/product-update.component';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
  providers: [PrimengService],
  entryComponents:[ProductSaveComponent, ProductUpdateComponent]
})
export class ProductListComponent implements OnInit {

  public productList: IProduct[];
  public columns: any[];
  public loading: boolean;

  constructor(private showMessageService: ShowMessageService, private primengService: PrimengService, private productService: ProductService) { 
    this.columns = [
      { field: 'productName', header: 'Product Name' },
      { field: 'unitName', header: 'Unit Name' },
      { field: 'productId', header: '' },
    ];
  }

  ngOnInit(): void {
    this.getProducttList();
  }

  //method to get data
  getProducttList(){
    try{
      this.loading = true;
      this.productService.getProductList().subscribe((res : any) => {
        this.productList = res as IProduct[];
        this.loading = false;
      });
    }
    catch(error){
      this.showMessageService.Error("Product", 'Error while getting data : ' + error);
    }
  }

  //method to open add dailog box
  AddDialog() {
    try {

      const ref = this.primengService.dialogService.open(ProductSaveComponent, {
        header: 'Add Product',
        width: "20%"
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getProducttList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Product", 'Error while opening add dialog  : ' + error);
    }
  }

  //method to open update dailog box
  UpdateDialog(product: IProduct) {
    try {
      let selectedProduct = {
        productId: product.productId,
        productName: product.productName,
        unitId: product.unitId,
        unitName: product.unitName,
      };      

      const ref = this.primengService.dialogService.open(ProductUpdateComponent, {
        header: 'Update Product',
        data: selectedProduct,
        width: "20%"
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getProducttList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Product", 'Error while opening add dialog : ' + error);
    }
  }

  //method to open delete dailog box
  DeleteDialog(productId: number){

  }

}
