import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IProduct } from '../shared/IProduct';
import { environment } from '../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  //Method to get response
  getProductList(): Observable<IProduct> {
    return this.http.get<IProduct>(environment.webAPIURL + '/api/Product/getProductList')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to get only names
  getProductData(): Observable<IProduct> {
    return this.http.get<IProduct>(environment.webAPIURL + '/api/Product/getProductData')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to add
  InsertProduct(product: IProduct): Observable<IProduct> {
    return this.http.post<IProduct>(environment.webAPIURL + '/api/Product/postProduct', product)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to Update
  UpdateProduct(selectedProduct: IProduct): Observable<IProduct> {
    return this.http.post<IProduct>(environment.webAPIURL + '/api/Product/updateProduct', selectedProduct)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

}
