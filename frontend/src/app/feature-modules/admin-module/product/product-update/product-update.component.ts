import { Component, OnInit } from '@angular/core';
import { UnitService } from '../../unit/shared/unit.service';
import { IUnit } from '../../unit/shared/IUnit';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { IProduct } from '../shared/IProduct';
import { ProductService } from '../shared/product.service';

@Component({
  selector: 'app-product-update',
  templateUrl: './product-update.component.html',
  styleUrls: ['./product-update.component.scss']
})
export class ProductUpdateComponent implements OnInit {

  public unitList: IUnit[];
  public selectedUnit: IUnit = {};
  
  public selectedProduct: IProduct = {};

  constructor(private showMessageService: ShowMessageService, private config: DynamicDialogConfig, 
    private ref: DynamicDialogRef, private unitService: UnitService,  private productService: ProductService) { }

  ngOnInit(): void {
    this.selectedProduct = this.config.data;
    this.getUnitNames();
  }

  //method to fetch unitname
  getUnitNames(){
    try{
      this.unitService.getUnitList().subscribe((res: any) => {
        this.unitList = res as IUnit[];
        this.selectedUnit.unitId = this.selectedProduct.unitId;
        this.selectedUnit.unitName = this.selectedProduct.unitName;
      }, err => {
              this.showMessageService.Error("Product Save", 'Error : ' + err.error);  
      });
    }
    catch(error){
      this.showMessageService.Error("Product", 'Error while adding data : ' + error);
    }
  }

  //method to update data
  UpdateProduct(){
    try{
      this.selectedProduct.unitId = this.selectedUnit.unitId;
      this.productService.UpdateProduct(this.selectedProduct).subscribe((res: any) => {
        this.showMessageService.Success("Product", res);
              this.selectedProduct = {};
              this.ref.close(true);
      }, err => {
              this.showMessageService.Error("Product Update", 'Error : ' + err.error);  
      });
    }
    catch(error){
      this.showMessageService.Error("Product", 'Error while updating data : ' + error);
    }
  }
}
