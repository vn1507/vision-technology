import { Component, OnInit } from '@angular/core';
import { UnitService } from '../../unit/shared/unit.service';
import { IUnit } from '../../unit/shared/IUnit';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { IProduct } from '../shared/IProduct';
import { ProductService } from '../shared/product.service';

@Component({
  selector: 'app-product-save',
  templateUrl: './product-save.component.html',
  styleUrls: ['./product-save.component.scss']
})
export class ProductSaveComponent implements OnInit {

  public unitList: IUnit[];
  public selectedUnit: IUnit = {};
  
  public product: IProduct = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, private unitService: UnitService,
     private productService: ProductService) { }

  ngOnInit(): void {
    this.getUnitNames();
  }

  //method to fetch unitname
  getUnitNames(){
    try{
      this.unitService.getUnitList().subscribe((res: any) => {
        this.unitList = res as IUnit[];
      }, err => {
              this.showMessageService.Error("Product Save", 'Error : ' + err.error);  
      });
    }
    catch(error){
      this.showMessageService.Error("Product", 'Error while adding data : ' + error);
    }
  }

  //method to save data
  SaveProduct(){
    try{
      this.product.unitId = this.selectedUnit.unitId;
      this.productService.InsertProduct(this.product).subscribe((res: any) => {
        this.showMessageService.Success("Product", res);
              this.product = {};
              this.ref.close(true);
      }, err => {
              this.showMessageService.Error("Product Save", 'Error : ' + err.error);  
      });
    }
    catch(error){
      this.showMessageService.Error("Product", 'Error while adding data : ' + error);
    }
  }

}
