import { Component, OnInit } from '@angular/core';
import { IDistrict } from '../shared/IDistrict';
import { DistrictService } from '../shared/district.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { PrimengService } from '../../../../shared-service/primeng.service';
import { DistrictSaveComponent } from '../district-save/district-save.component';
import { DistrictUpdateComponent } from '../district-update/district-update.component';

@Component({
  selector: 'app-district-list',
  templateUrl: './district-list.component.html',
  styleUrls: ['./district-list.component.scss'],
  providers: [PrimengService],
  entryComponents: [DistrictSaveComponent, DistrictUpdateComponent]
})
export class DistrictListComponent implements OnInit {

  public districtList: IDistrict[];
  public columns: any[];
  public loading: boolean;

  constructor(private showMessageService: ShowMessageService, private primengService: PrimengService, private districtService: DistrictService) { 
    this.columns = [
      { field: 'stateName', header: 'State Name' },
      { field: 'districtName', header: 'District Name' },
      { field: 'districtId', header: '' },
    ];
  }

  ngOnInit(): void {
    this.getDistrictList();
  }

  //method to get data
  getDistrictList(){
    try{
      this.loading = true;
      this.districtService.getDistrictList().subscribe((res : any) => {
        this.districtList = res as IDistrict[];
        this.loading = false;
      });
    }
    catch(error){
      this.showMessageService.Error("State", 'Error while getting data : ' + error);
    }
  }

  //method to open add dailog box
  AddDialog() {
    try {

      const ref = this.primengService.dialogService.open(DistrictSaveComponent, {
        header: 'Add District',
        width: "20%"
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getDistrictList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("District", 'Error while opening add dialog District : ' + error);
    }
  }

  //method to open update dailog box
  UpdateDialog(district: IDistrict) {
    try {
      let selectedDistrict = {
        districtId: district.districtId,
        districtName: district.districtName,
        stateId: district.stateId,
        stateName: district.stateName,
      };      

      const ref = this.primengService.dialogService.open(DistrictUpdateComponent, {
        header: 'Update District',
        data: selectedDistrict,
        width: "20%"
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getDistrictList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("District", 'Error while opening update dialog District : ' + error);
    }
  }

  //method to open delete dailog box
  DeleteDialog(districtId: number){

  }  

}
