import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IDistrict } from '../shared/IDistrict';
import { environment } from '../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DistrictService {

  constructor(private http: HttpClient) { }

  //Method to get response
  getDistrictList(): Observable<IDistrict> {
    return this.http.get<IDistrict>(environment.webAPIURL + '/api/District/getDistrictList')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to get names and id
  getDistrictData(): Observable<IDistrict> {
    return this.http.get<IDistrict>(environment.webAPIURL + '/api/District/getDistrictData')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to get district name by id
  getDistrictListByStateId(StateId: number): Observable<IDistrict> {
    return this.http.get<IDistrict>(environment.webAPIURL + '/api/District/getDistrictListByStateId?=' + StateId)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to add
  InsertDistrict(district: IDistrict): Observable<IDistrict> {
    return this.http.post<IDistrict>(environment.webAPIURL + '/api/District/postDistrict', district)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to update
  UpdateDistrict(selectedDistrict: IDistrict): Observable<IDistrict> {
    return this.http.post<IDistrict>(environment.webAPIURL + '/api/District/updateDistrict', selectedDistrict)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

}
