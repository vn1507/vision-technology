import { Component, OnInit } from '@angular/core';
import { DistrictService } from '../shared/district.service';
import { IDistrict } from '../shared/IDistrict';
import { StateService } from '../../state/shared/state.service';
import { IState } from '../../state/shared/IState';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-district-update',
  templateUrl: './district-update.component.html',
  styleUrls: ['./district-update.component.scss']
})
export class DistrictUpdateComponent implements OnInit {

  public selectedDistrict: IDistrict = {};

  public stateList: IState[];
  public selectedState: IState = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, private config: DynamicDialogConfig, 
    private districtService: DistrictService, private stateService: StateService) { }

  ngOnInit(): void {
    this.selectedDistrict = this.config.data;
    this.getStateNames();
  }

  //method to fetch countryname
  getStateNames(){
    try{
      this.stateService.getStateData().subscribe((res: any) => {
        this.stateList = res as IDistrict[];
        this.selectedState.stateId = this.selectedDistrict.stateId;
        this.selectedState.stateName = this.selectedDistrict.stateName;
      });
    }
    catch(error){
      this.showMessageService.Error("District", 'Error while feteching data : ' + error);
    }
  }

  //method to update data
  UpdateDistrict(){
    try{
      this.selectedDistrict.stateId = this.selectedState.stateId;
      this.districtService.InsertDistrict(this.selectedDistrict).subscribe((res: any) => {
        this.showMessageService.Success("District", res);
              this.selectedDistrict = {};
              this.ref.close(true);
      }, err => {
              if(err.status == 409){
                this.showMessageService.Error("Duplicate District Name" ,  "District name already exists");  
              } else{
                this.showMessageService.Error("District Update", 'Error : ' + err.error);  
              }
      });
    }
    catch(error){
      this.showMessageService.Error("District", 'Error while adding data : ' + error);
    }
  }

}
