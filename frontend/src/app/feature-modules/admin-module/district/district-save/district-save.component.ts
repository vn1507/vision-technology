import { Component, OnInit } from '@angular/core';
import { DistrictService } from '../shared/district.service';
import { IDistrict } from '../shared/IDistrict';
import { StateService } from '../../state/shared/state.service';
import { IState } from '../../state/shared/IState';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-district-save',
  templateUrl: './district-save.component.html',
  styleUrls: ['./district-save.component.scss']
})
export class DistrictSaveComponent implements OnInit {

  public district: IDistrict = {};

  public stateList: IState[];
  public selectedState: IState = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, 
    private districtService: DistrictService, private stateService: StateService) { }

  ngOnInit(): void {
    this.getStateNames();
  }

  //method to fetch countryname
  getStateNames(){
    try{
      this.stateService.getStateData().subscribe((res: any) => {
        this.stateList = res as IDistrict[];
      });
    }
    catch(error){
      this.showMessageService.Error("State", 'Error while feteching data : ' + error);
    }
  }

  //method to save data
  SaveDistrict(){
    try{
      this.district.stateId = this.selectedState.stateId;
      this.districtService.InsertDistrict(this.district).subscribe((res: any) => {
        this.showMessageService.Success("District", res);
              this.district = {};
              this.ref.close(true);
      }, err => {
              if(err.status == 409){
                this.showMessageService.Error("Duplicate District Name" ,  "District name already exists");  
              } else{
                this.showMessageService.Error("District Save", 'Error : ' + err.error);  
              }
      });
    }
    catch(error){
      this.showMessageService.Error("District", 'Error while adding data : ' + error);
    }
  }

}
