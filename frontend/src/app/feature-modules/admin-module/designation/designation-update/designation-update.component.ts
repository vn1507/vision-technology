import { Component, OnInit } from '@angular/core';
import { DesignationService } from '../shared/designation.service';
import { IDesignation } from '../shared/IDesignation';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-designation-update',
  templateUrl: './designation-update.component.html',
  styleUrls: ['./designation-update.component.scss']
})
export class DesignationUpdateComponent implements OnInit {

  public selectedDesignation: IDesignation = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef,
    private config: DynamicDialogConfig, private designationService: DesignationService) { }

  ngOnInit(): void {
    this.selectedDesignation = this.config.data;
  }

  //method to update data
  UpdateDesignation() {
    try {
      this.designationService.InsertDesignation(this.selectedDesignation).subscribe((res: any) => {
        this.showMessageService.Success("Designation", res);
        this.selectedDesignation = {};
        this.ref.close(true);
      }, err => {
        if (err.status == 409) {
          this.showMessageService.Error("Designation", "Designation already exists");
        } else {
          this.showMessageService.Error("Designation Save", 'Error : ' + err.error);
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Designation", 'Error while upating data : ' + error);
    }
  }

}
