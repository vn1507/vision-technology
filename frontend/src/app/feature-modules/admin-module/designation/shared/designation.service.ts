import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IDesignation } from '../shared/IDesignation';
import { environment } from '../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DesignationService {

  constructor(private http: HttpClient) { }

  //Method to get response
  getDesignationList(): Observable<IDesignation> {
    return this.http.get<IDesignation>(environment.webAPIURL + '/api/Designation/getDesignationList')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to add
  InsertDesignation(designation: IDesignation): Observable<IDesignation> {
    return this.http.post<IDesignation>(environment.webAPIURL + '/api/Designation/postDesignation', designation)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to update
  UpdateDesignation(selectedDesignation: IDesignation): Observable<IDesignation> {
    return this.http.post<IDesignation>(environment.webAPIURL + '/api/Designation/updateDesignation', selectedDesignation)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

}
