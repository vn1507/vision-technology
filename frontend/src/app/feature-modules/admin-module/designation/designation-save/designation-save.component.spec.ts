import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignationSaveComponent } from './designation-save.component';

describe('DesignationSaveComponent', () => {
  let component: DesignationSaveComponent;
  let fixture: ComponentFixture<DesignationSaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesignationSaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignationSaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
