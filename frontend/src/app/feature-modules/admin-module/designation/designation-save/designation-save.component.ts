import { Component, OnInit } from '@angular/core';
import { DesignationService } from '../shared/designation.service';
import { IDesignation } from '../shared/IDesignation';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-designation-save',
  templateUrl: './designation-save.component.html',
  styleUrls: ['./designation-save.component.scss']
})
export class DesignationSaveComponent implements OnInit {

  public designation: IDesignation = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef,
    private designationService: DesignationService) { }

  ngOnInit(): void {
  }

  //method to save data
  SaveDesignation() {
    try {
      this.designationService.InsertDesignation(this.designation).subscribe((res: any) => {
        this.showMessageService.Success("Designation", res);
        this.designation = {};
        this.ref.close(true);
      }, err => {
        if (err.status == 409) {
          this.showMessageService.Error("Designation", "Designation already exists");
        } else {
          this.showMessageService.Error("Designation Save", 'Error : ' + err.error);
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Designation", 'Error while adding data : ' + error);
    }
  }

}
