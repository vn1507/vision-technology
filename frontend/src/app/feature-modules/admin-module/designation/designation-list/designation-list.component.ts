import { Component, OnInit } from '@angular/core';
import { IDesignation } from '../shared/IDesignation';
import { DesignationService } from '../shared/designation.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { PrimengService } from '../../../../shared-service/primeng.service';
import { DesignationSaveComponent } from '../designation-save/designation-save.component';
import { DesignationUpdateComponent } from '../designation-update/designation-update.component';

@Component({
  selector: 'app-designation-list',
  templateUrl: './designation-list.component.html',
  styleUrls: ['./designation-list.component.scss'],
  providers: [PrimengService],
  entryComponents: [DesignationSaveComponent, DesignationUpdateComponent]
})
export class DesignationListComponent implements OnInit {

  public designationList: IDesignation[];
  public columns: any[];
  public loading: boolean;

  constructor(private showMessageService: ShowMessageService, private primengService: PrimengService, private designationService: DesignationService) { 
    this.columns = [      
      { field: 'designationName', header: 'Designation Name' },
      { field: 'designationId', header: '' },
    ];
  }

  ngOnInit(): void {
    this.getDesignationList();
  }

  //method to get data
  getDesignationList(){
    try{
      this.loading = true;
      this.designationService.getDesignationList().subscribe((res : any) => {
        this.designationList = res as IDesignation[];
        this.loading = false;
      });
    }
    catch(error){
      this.showMessageService.Error("Designation", 'Error while getting data : ' + error);
    }
  }

  //method to open add dailog box
  AddDialog() {
    try {

      const ref = this.primengService.dialogService.open(DesignationSaveComponent, {
        header: 'Add Designation',
        width: "20%"
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getDesignationList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Designation", 'Error while opening add dialog Designation : ' + error);
    }
  }

  //method to open update dailog box
  UpdateDialog(designation: IDesignation) {
    try {
      let selectedDistrict = {
        designationId: designation.designationId,
        designationName: designation.designationName,
      };      

      const ref = this.primengService.dialogService.open(DesignationUpdateComponent, {
        header: 'Update Designation',
        data: selectedDistrict,
        width: "20%"
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getDesignationList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Designation", 'Error while opening update dialog Designation : ' + error);
    }
  }

  //method to open delete dailog box
  DeleteDialog(designationId: number){

  }  

}
