import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { SharedModule } from '../../shared-module/shared.module';
import { SharedComponentsModule } from '../../shared-components/shared-components.module';

import { AdminMenuComponent } from './admin-menu/admin-menu.component';
import { UnitListComponent } from './unit/unit-list/unit-list.component';
import { UnitSaveComponent } from './unit/unit-save/unit-save.component';
import { UnitUpdateComponent } from './unit/unit-update/unit-update.component';
import { ProductListComponent } from './product/product-list/product-list.component';
import { ProductSaveComponent } from './product/product-save/product-save.component';
import { ProductUpdateComponent } from './product/product-update/product-update.component';
import { PartyListComponent } from './party/party-list/party-list.component';
import { PartySaveComponent } from './party/party-save/party-save.component';
import { PartyUpdateComponent } from './party/party-update/party-update.component';
import { ItemInwardSaveComponent } from './item_inward/item-inward-save/item-inward-save.component';
import { CompanyListComponent } from './company/company-list/company-list.component';
import { CompanySaveComponent } from './company/company-save/company-save.component';
import { CompanyUpdateComponent } from './company/company-update/company-update.component';
import { ItemInwardListComponent } from './item_inward/item-inward-list/item-inward-list.component';
import { ItemInwardUpdateComponent } from './item_inward/item-inward-update/item-inward-update.component';
import { ModemListComponent } from './modem/modem-list/modem-list.component';
import { ModemSaveComponent } from './modem/modem-save/modem-save.component';
import { ModemUpdateComponent } from './modem/modem-update/modem-update.component';
import { OltListComponent } from './olt/olt-list/olt-list.component';
import { OltSaveComponent } from './olt/olt-save/olt-save.component';
import { OltUpdateComponent } from './olt/olt-update/olt-update.component';
import { SpliterListComponent } from './spliter/spliter-list/spliter-list.component';
import { SpliterSaveComponent } from './spliter/spliter-save/spliter-save.component';
import { SpliterUpdateComponent } from './spliter/spliter-update/spliter-update.component';
import { CountryListComponent } from './country/country-list/country-list.component';
import { CountrySaveComponent } from './country/country-save/country-save.component';
import { CountryUpdateComponent } from './country/country-update/country-update.component';
import { StateListComponent } from './state/state-list/state-list.component';
import { StateSaveComponent } from './state/state-save/state-save.component';
import { StateUpdateComponent } from './state/state-update/state-update.component';
import { DistrictListComponent } from './district/district-list/district-list.component';
import { DistrictSaveComponent } from './district/district-save/district-save.component';
import { DistrictUpdateComponent } from './district/district-update/district-update.component';
import { CityListComponent } from './city/city-list/city-list.component';
import { CitySaveComponent } from './city/city-save/city-save.component';
import { CityUpdateComponent } from './city/city-update/city-update.component';
import { DesignationListComponent } from './designation/designation-list/designation-list.component';
import { DesignationSaveComponent } from './designation/designation-save/designation-save.component';
import { DesignationUpdateComponent } from './designation/designation-update/designation-update.component';
import { ItemOutwardListComponent } from './item_outward/item-outward-list/item-outward-list.component';
import { ItemOutwardSaveComponent } from './item_outward/item-outward-save/item-outward-save.component';
import { ItemOutwardUpdateComponent } from './item_outward/item-outward-update/item-outward-update.component';
import { CustomerUsedMaterialListComponent } from './customer-used-material/customer-used-material-list/customer-used-material-list.component';
import { CustomerUsedMaterialSaveComponent } from './customer-used-material/customer-used-material-save/customer-used-material-save.component';
import { CustomerUsedMaterialUpdateComponent } from './customer-used-material/customer-used-material-update/customer-used-material-update.component';
import { PlanListComponent } from './plan/plan-list/plan-list.component';
import { PlanSaveComponent } from './plan/plan-save/plan-save.component';
import { PlanUpdateComponent } from './plan/plan-update/plan-update.component';
import { ComapnyModelMappingListComponent } from './company-model-mapping/comapny-model-mapping-list/comapny-model-mapping-list.component';
import { ComapnyModelMappingSaveComponent } from './company-model-mapping/comapny-model-mapping-save/comapny-model-mapping-save.component';
import { ComapnyModelMappingUpdateComponent } from './company-model-mapping/comapny-model-mapping-update/comapny-model-mapping-update.component';
import { AreaListComponent } from './area/area-list/area-list.component';
import { AreaSaveComponent } from './area/area-save/area-save.component';
import { AreaUpdateComponent } from './area/area-update/area-update.component';


@NgModule({  
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule,
    SharedComponentsModule
  ],
  declarations: [
    AdminMenuComponent,
    UnitListComponent,
    UnitSaveComponent,
    UnitUpdateComponent,
    ProductListComponent,
    ProductSaveComponent,
    ProductUpdateComponent,
    PartyListComponent,
    PartySaveComponent,
    PartyUpdateComponent,
    ItemInwardSaveComponent,
    CompanyListComponent,
    CompanySaveComponent,
    CompanyUpdateComponent,
    ItemInwardListComponent,
    ItemInwardUpdateComponent,
    ModemListComponent,
    ModemSaveComponent,
    ModemUpdateComponent,
    OltListComponent,
    OltSaveComponent,
    OltUpdateComponent,
    SpliterListComponent,
    SpliterSaveComponent,
    SpliterUpdateComponent,
    CountryListComponent,
    CountrySaveComponent,
    CountryUpdateComponent,
    StateListComponent,
    StateSaveComponent,
    StateUpdateComponent,
    DistrictListComponent,
    DistrictSaveComponent,
    DistrictUpdateComponent,
    CityListComponent,
    CitySaveComponent,
    CityUpdateComponent,
    DesignationListComponent,
    DesignationSaveComponent,
    DesignationUpdateComponent,
    ItemOutwardListComponent,
    ItemOutwardSaveComponent,
    ItemOutwardUpdateComponent,
    CustomerUsedMaterialListComponent,
    CustomerUsedMaterialSaveComponent,
    CustomerUsedMaterialUpdateComponent,
    PlanListComponent,
    PlanSaveComponent,
    PlanUpdateComponent,
    ComapnyModelMappingListComponent,
    ComapnyModelMappingSaveComponent,
    ComapnyModelMappingUpdateComponent,
    AreaListComponent,
    AreaSaveComponent,
    AreaUpdateComponent
  ]
})
export class AdminModule { }
