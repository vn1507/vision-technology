import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IArea } from '../shared/IArea';
import { environment } from '../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AreaService {

  constructor(private http: HttpClient) { }

  //Method to get response
  getAreaList(): Observable<IArea> {
    return this.http.get<IArea>(environment.webAPIURL + '/api/Area/getAreaList')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }   

  //Method to get names and id
  getAreaData(): Observable<IArea> {
    return this.http.get<IArea>(environment.webAPIURL + '/api/Area/getAreaData')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to get area name by id
  getAreaListByCityId(cityId: number): Observable<IArea> {
    return this.http.get<IArea>(environment.webAPIURL + '/api/Area/getAreaListByCityId?CityId='+cityId)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to add
  InsertArea(area: IArea): Observable<IArea> {    
    return this.http.post<IArea>(environment.webAPIURL + '/api/Area/postArea', area)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to update
  UpdateArea(selectedArea: IArea): Observable<IArea> {
    return this.http.post<IArea>(environment.webAPIURL + '/api/Area/updateArea', selectedArea)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

}
