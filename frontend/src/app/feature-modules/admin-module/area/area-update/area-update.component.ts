import { Component, OnInit } from '@angular/core';
import { AreaService } from '../shared/area.service';
import { IArea } from '../shared/IArea';
import { CityService } from '../../city/shared/city.service';
import { ICity } from '../../city/shared/ICity';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-area-update',
  templateUrl: './area-update.component.html',
  styleUrls: ['./area-update.component.scss']
})
export class AreaUpdateComponent implements OnInit {

  public selectedArea: IArea = {};

  public cityList: ICity[];
  public selectedCity: ICity = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, private config: DynamicDialogConfig,
    private areaService: AreaService, private cityService: CityService) { }

  ngOnInit(): void {
    this.selectedArea = this.config.data;
    this.getDistrictNames();
  }

  //method to fetch cityname
  getDistrictNames(){
    try{
      this.cityService.getCityData().subscribe((res: any) => {
        this.cityList = res as ICity[];
        this.selectedCity.cityId = this.selectedArea.cityId;
        this.selectedCity.cityName = this.selectedArea.cityName;
      });
    }
    catch(error){
      this.showMessageService.Error("City", 'Error while feteching data : ' + error);
    }
  }

  //method to save data
  UpdateArea(){
    try{      
      this.selectedArea.cityId = this.selectedCity.cityId;
      this.areaService.UpdateArea(this.selectedArea).subscribe((res: any) => {
        this.showMessageService.Success("Area", res);
              this.selectedArea = {};
              this.ref.close(true);
      }, err => {
              if(err.status == 400){
                this.showMessageService.Error("City" ,  "Please select city to save the record");  
              } else if(err.status == 409){
                this.showMessageService.Error("Duplicate Area Name" ,  "Area name already exists");  
              } else{
                this.showMessageService.Error("Area Update", 'Error : ' + err.error);  
              }
      });
    }
    catch(error){
      this.showMessageService.Error("Area", 'Error while updating data : ' + error);
    }
  }

}
