import { Component, OnInit } from '@angular/core';
import { IArea } from '../shared/IArea';
import { AreaService } from '../shared/area.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { PrimengService } from '../../../../shared-service/primeng.service';
import { AreaSaveComponent } from '../area-save/area-save.component';
import { AreaUpdateComponent } from '../area-update/area-update.component';

@Component({
  selector: 'app-area-list',
  templateUrl: './area-list.component.html',
  styleUrls: ['./area-list.component.scss'],
  providers: [PrimengService],
  entryComponents: [AreaSaveComponent, AreaUpdateComponent]
})
export class AreaListComponent implements OnInit {

  public areaList: IArea[];
  public columns: any[];
  public loading: boolean;

  constructor(private showMessageService: ShowMessageService, private primengService: PrimengService, 
    private areaService: AreaService) { 
    this.columns = [
      { field: 'areaName', header: 'Area Name' },
      { field: 'areaId', header: '' },
    ];
  }

  ngOnInit(): void {
    this.getAreaList();
  }

  //method to get data
  getAreaList(){
    try{
      this.loading = true;
      this.areaService.getAreaList().subscribe((res : any) => {
        this.areaList = res as IArea[];
        this.loading = false;
      });
    }
    catch(error){
      this.showMessageService.Error("Area", 'Error while getting data : ' + error);
    }
  }

  //method to open add dailog box
  AddDialog() {
    try {

      const ref = this.primengService.dialogService.open(AreaSaveComponent, {
        header: 'Add Area',
        width: "20%"
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getAreaList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Area", 'Error while opening add dialog Area : ' + error);
    }
  }

  //method to open update dailog box
  UpdateDialog(area: IArea) {
    try {
      let selectedArea = {
        areaId: area.areaId,
        areaName: area.areaName,
        cityId: area.cityId,
        cityName: area.cityName,
      };      

      const ref = this.primengService.dialogService.open(AreaUpdateComponent, {
        header: 'Update Area',
        data: selectedArea,
        width: "20%"
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getAreaList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Area", 'Error while opening update dialog Area : ' + error);
    }
  }

  //method to open delete dailog box
  DeleteDialog(areaId: number){

  }  

}
