import { Component, OnInit } from '@angular/core';
import { AreaService } from '../shared/area.service';
import { IArea } from '../shared/IArea';
import { CityService } from '../../city/shared/city.service';
import { ICity } from '../../city/shared/ICity';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-area-save',
  templateUrl: './area-save.component.html',
  styleUrls: ['./area-save.component.scss']
})
export class AreaSaveComponent implements OnInit {

  public area: IArea = {};

  public cityList: ICity[];
  public selectedCity: ICity = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, 
    private areaService: AreaService, private cityService: CityService) { }

  ngOnInit(): void {
    this.getDistrictNames();
  }

  //method to fetch cityname
  getDistrictNames(){
    try{
      this.cityService.getCityData().subscribe((res: any) => {
        this.cityList = res as ICity[];
      });
    }
    catch(error){
      this.showMessageService.Error("City", 'Error while feteching data : ' + error);
    }
  }

  //method to save data
  SaveArea(){
    try{      
      this.area.cityId = this.selectedCity.cityId;
      this.areaService.InsertArea(this.area).subscribe((res: any) => {
        this.showMessageService.Success("Area", res);
              this.area = {};
              this.ref.close(true);
      }, err => {
              if(err.status == 400){
                this.showMessageService.Error("City" ,  "Please select city to save the record");  
              } else if(err.status == 409){
                this.showMessageService.Error("Duplicate Area Name" ,  "Area name already exists");  
              } else{
                this.showMessageService.Error("Area Save", 'Error : ' + err.error);  
              }
      });
    }
    catch(error){
      this.showMessageService.Error("Area", 'Error while adding data : ' + error);
    }
  }

}
