import { Component, OnInit } from '@angular/core';
import { IParty } from '../shared/IParty';
import { PartyService } from '../shared/party.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { PrimengService } from '../../../../shared-service/primeng.service';
import { PartySaveComponent } from '../party-save/party-save.component';
import { PartyUpdateComponent } from '../party-update/party-update.component';

@Component({
  selector: 'app-party-list',
  templateUrl: './party-list.component.html',
  styleUrls: ['./party-list.component.scss'],
  providers:[PrimengService],
  entryComponents:[PartySaveComponent, PartyUpdateComponent]
})
export class PartyListComponent implements OnInit {

  public partyList: IParty[];
  public columns: any[];
  public loading: boolean;

  constructor(private showMessageService: ShowMessageService, private primengService: PrimengService, private partyService: PartyService) { 
    this.columns = [
      { field: 'partyName', header: 'Party Name' },
      { field: 'mobileNo', header: 'Mobile No.  ' },
      { field: 'emailAddress', header: 'Email ID' },
      { field: 'partyGst', header: 'GST' },
      { field: 'partyAddress', header: 'Address ' },
      { field: 'partyId', header: '' },
    ];    
  }

  ngOnInit(): void {
    this.getPartyList();
  }

  //method to get data
  getPartyList(){
    try{
      this.loading = true;
      this.partyService.getPartyList().subscribe((res : any) => {
        this.partyList = res as IParty[];
        this.loading = false;
      });
    }
    catch(error){
      this.showMessageService.Error("Party", 'Error while getting data : ' + error);
    }
  }  

  //method to open add dailog box
  AddDialog() {
    try {

      const ref = this.primengService.dialogService.open(PartySaveComponent, {
        header: 'Add Party',
        width: '20%'
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getPartyList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Party", 'Error while opening add dialog  : ' + error);
    }
  }

  //method to open update dailog box
  UpdateDialog(party: IParty) {
    try {
      let selectedUnit = {
        partyId: party.partyId,
        partyName: party.partyName,
        mobileNo: party.mobileNo,
        emailAddress: party.emailAddress,
        partyAddress: party.partyAddress,
      };      

      const ref = this.primengService.dialogService.open(PartyUpdateComponent, {
        header: 'Update Party',
        width: '20%',
        data: selectedUnit
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getPartyList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Party", 'Error while opening add dialog : ' + error);
    }
  }

  //method to open delete dailog box
  DeleteDialog(unitId: number){

  }

}
