import { Component, OnInit } from '@angular/core';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { IParty } from '../shared/IParty';
import { PartyService } from '../shared/party.service';

@Component({
  selector: 'app-party-save',
  templateUrl: './party-save.component.html',
  styleUrls: ['./party-save.component.scss']
})
export class PartySaveComponent implements OnInit {

  public party: IParty = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, private partyService: PartyService) { }

  ngOnInit(): void {
  }

  // Validation of maxLength for number input field  
  onlyNumberKey(event) {
    if (event.target.value.length == 0 && event.which == 48) {
      return false;
    }
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 46 && event.charCode <= 57;
  }

  //method to save data
  SaveParty(){
    try{      
      this.partyService.InsertParty(this.party).subscribe((res: any) => {
        this.showMessageService.Success("Party", res);
              this.party = {};
              this.ref.close(true);
      }, err => {
              this.showMessageService.Error("Party Save", 'Error : ' + err.error);  
      });
    }
    catch(error){
      this.showMessageService.Error("Party", 'Error while adding data : ' + error);
    }
  }

}
