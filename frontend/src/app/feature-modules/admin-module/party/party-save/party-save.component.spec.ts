import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartySaveComponent } from './party-save.component';

describe('PartySaveComponent', () => {
  let component: PartySaveComponent;
  let fixture: ComponentFixture<PartySaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartySaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartySaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
