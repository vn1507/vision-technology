import { Component, OnInit } from '@angular/core';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { IParty } from '../shared/IParty';
import { PartyService } from '../shared/party.service';

@Component({
  selector: 'app-party-update',
  templateUrl: './party-update.component.html',
  styleUrls: ['./party-update.component.scss']
})
export class PartyUpdateComponent implements OnInit {

  public selectedParty: IParty = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, 
    private config: DynamicDialogConfig, private partyService: PartyService) { }

  ngOnInit(): void {
    this.selectedParty = this.config.data;
  }

  // Validation of maxLength for number input field  
  onlyNumberKey(event) {
    if (event.target.value.length == 0 && event.which == 48) {
      return false;
    }
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 46 && event.charCode <= 57;
  }

  //method to update data
  UpdateParty(){
    try{            
      this.partyService.UpdateParty(this.selectedParty).subscribe((res: any) => {
        this.showMessageService.Success("Party", res);
              this.selectedParty = {};
              this.ref.close(true);
      }, err => {
              this.showMessageService.Error("Party update", 'Error : ' + err.error);  
      });
    }
    catch(error){
      this.showMessageService.Error("Party", 'Error while updating data : ' + error);
    }
  }

}
