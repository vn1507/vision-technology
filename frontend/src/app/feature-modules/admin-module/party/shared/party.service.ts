import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IParty } from '../shared/IParty';
import { environment } from '../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PartyService {

  constructor(private http: HttpClient) { }

  //Method to get response
  getPartyList(): Observable<IParty> {
    return this.http.get<IParty>(environment.webAPIURL + '/api/Party/getPartyList')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to get only names
  getPartyData(): Observable<IParty> {
    return this.http.get<IParty>(environment.webAPIURL + '/api/Party/getPartyData')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to add
  InsertParty(party: IParty): Observable<IParty> {
    return this.http.post<IParty>(environment.webAPIURL + '/api/Party/postParty', party)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to Update
  UpdateParty(selectedParty: IParty): Observable<IParty> {    
    return this.http.post<IParty>(environment.webAPIURL + '/api/Party/updateParty', selectedParty)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }
}
