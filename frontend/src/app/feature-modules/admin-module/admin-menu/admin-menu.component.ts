import { Component, OnInit } from '@angular/core';
import { LayoutComponent } from '../../../layout/layout.component';

@Component({
  selector: 'app-admin-menu',
  templateUrl: './admin-menu.component.html',
  styleUrls: ['./admin-menu.component.scss']
})
export class AdminMenuComponent implements OnInit {
  

  constructor(public layoutComponent: LayoutComponent) {
    this.layoutComponent.moduleName = "Admin";
  }

  ngOnInit(): void {
   
  }

}
