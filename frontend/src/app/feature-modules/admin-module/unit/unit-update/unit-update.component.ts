import { Component, OnInit } from '@angular/core';
import { UnitService } from '../shared/unit.service';
import { IUnit } from '../shared/IUnit';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-unit-update',
  templateUrl: './unit-update.component.html',
  styleUrls: ['./unit-update.component.scss']
})
export class UnitUpdateComponent implements OnInit {

  public selectedUnit: IUnit = {};

  constructor(private showMessageService: ShowMessageService, private config: DynamicDialogConfig, 
    private ref: DynamicDialogRef, private unitService: UnitService) {       
    }

  ngOnInit(): void {
    this.selectedUnit = this.config.data;
  }

  //method to update data
  UpdateUnit(){
    try{
      this.unitService.UpdateUnit(this.selectedUnit).subscribe((res: any) => {
        this.showMessageService.Success("Unit", res);
              this.selectedUnit = {};
              this.ref.close(true);
      }, err => {
              this.showMessageService.Error("Unit Update", 'Error : ' + err.error);  
      });
    }
    catch(error){
      this.showMessageService.Error("Unit", 'Error while Updating data : ' + error);
    }
  }

}
