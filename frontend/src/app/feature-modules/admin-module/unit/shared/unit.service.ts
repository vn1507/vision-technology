import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IUnit } from '../shared/IUnit';
import { environment } from '../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UnitService {

  constructor(private http: HttpClient) { }

  //Method to get response
  getUnitList(): Observable<IUnit> {
    return this.http.get<IUnit>(environment.webAPIURL + '/api/Unit/getUnitList')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to add
  InsertUnit(unit: IUnit): Observable<IUnit> {
    return this.http.post<IUnit>(environment.webAPIURL + '/api/Unit/postUnit', unit)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to add
  UpdateUnit(selectedUnit: IUnit): Observable<IUnit> {
    return this.http.post<IUnit>(environment.webAPIURL + '/api/Unit/updateUnit', selectedUnit)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

}
