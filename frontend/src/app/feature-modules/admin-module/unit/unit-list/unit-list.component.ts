import { Component, OnInit } from '@angular/core';
import { IUnit } from '../shared/IUnit';
import { UnitService } from '../shared/unit.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { PrimengService } from '../../../../shared-service/primeng.service';
import { UnitSaveComponent } from '../unit-save/unit-save.component';
import { UnitUpdateComponent } from '../unit-update/unit-update.component';

@Component({
  selector: 'app-unit-list',
  templateUrl: './unit-list.component.html',
  styleUrls: ['./unit-list.component.scss'],
  providers: [PrimengService],
  entryComponents: [UnitSaveComponent, UnitUpdateComponent]
})
export class UnitListComponent implements OnInit {

  public unitList: IUnit[];
  public columns: any[];
  public loading: boolean;

  constructor(private showMessageService: ShowMessageService, private primengService: PrimengService, private unitService: UnitService) { 
    this.columns = [
      { field: 'unitName', header: 'Unit Name' },
      { field: 'unitId', header: '' },
    ];
  }

  ngOnInit(): void {
    this.getUnitList();
  }

  //method to get data
  getUnitList(){
    try{
      this.loading = true;
      this.unitService.getUnitList().subscribe((res : any) => {
        this.unitList = res as IUnit[];
        this.loading = false;
      });
    }
    catch(error){
      this.showMessageService.Error("Unit", 'Error while getting data : ' + error);
    }
  }

  //method to open add dailog box
  AddDialog() {
    try {

      const ref = this.primengService.dialogService.open(UnitSaveComponent, {
        header: 'Add Unit',
        width: "20%"
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getUnitList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Unit", 'Error while opening add dialog Hardcode : ' + error);
    }
  }

  //method to open update dailog box
  UpdateDialog(unit: IUnit) {
    try {
      let selectedUnit = {
        unitId: unit.unitId,
        unitName: unit.unitName
      };      

      const ref = this.primengService.dialogService.open(UnitUpdateComponent, {
        header: 'Update Unit',
        data: selectedUnit,
        width: "20%"
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getUnitList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Unit", 'Error while opening add dialog Unit : ' + error);
    }
  }

  //method to open delete dailog box
  DeleteDialog(unitId: number){

  }


}
