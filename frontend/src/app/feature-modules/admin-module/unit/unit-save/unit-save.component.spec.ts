import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnitSaveComponent } from './unit-save.component';

describe('UnitSaveComponent', () => {
  let component: UnitSaveComponent;
  let fixture: ComponentFixture<UnitSaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnitSaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnitSaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
