import { Component, OnInit } from '@angular/core';
import { UnitService } from '../shared/unit.service';
import { IUnit } from '../shared/IUnit';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-unit-save',
  templateUrl: './unit-save.component.html',
  styleUrls: ['./unit-save.component.scss']
})
export class UnitSaveComponent implements OnInit {

  public unit: IUnit = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, private unitService: UnitService) { }

  ngOnInit(): void {
  }

  //method to save data
  SaveUnit(){
    try{
      this.unitService.InsertUnit(this.unit).subscribe((res: any) => {        
        this.showMessageService.Success("Unit", res);
              this.unit = {};
              this.ref.close(true);
      }, err => {
              if(err.status == 409){
                this.showMessageService.Error("Duplicate Unit Name" ,  "Unit name already exists");  
              } else{
                this.showMessageService.Error("Unit Save", 'Error : ' + err.error);  
              }
      });
    }
    catch(error){
      this.showMessageService.Error("Unit", 'Error while adding data : ' + error);
    }
  }

}
