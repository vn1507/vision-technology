import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComapnyModelMappingSaveComponent } from './comapny-model-mapping-save.component';

describe('ComapnyModelMappingSaveComponent', () => {
  let component: ComapnyModelMappingSaveComponent;
  let fixture: ComponentFixture<ComapnyModelMappingSaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComapnyModelMappingSaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComapnyModelMappingSaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
