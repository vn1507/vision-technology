import { Component, OnInit } from '@angular/core';
import { ComapnyModelMappingService } from '../shared/comapny-model-mapping.service';
import { IComapnyModelMapping } from '../shared/IComapnyModelMapping';
import { CompanyService } from '../../company/shared/company.service';
import { ICompany } from '../../company/shared/ICompany';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-comapny-model-mapping-save',
  templateUrl: './comapny-model-mapping-save.component.html',
  styleUrls: ['./comapny-model-mapping-save.component.scss']
})
export class ComapnyModelMappingSaveComponent implements OnInit {

  public companyModelMapping: IComapnyModelMapping = {};
  public companyList: ICompany[];
  public selectedCompany: ICompany = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, 
    private comapnyModelMappingService: ComapnyModelMappingService, private companyService: CompanyService) { }

  ngOnInit(): void {
    this.getCompanyNames();
  }

  //method to get company names and ids
  getCompanyNames(){
    try{
      this.companyService.getCompanyList().subscribe((res) => {
        this.companyList = res as ICompany[];
      });

    }
    catch(error){
      this.showMessageService.Error("Company Name", 'Error while getting data : ' + error);
    }
  }

  //method to save data
  SaveCompanyModelMapping(){
    try{
      this.companyModelMapping.companyId = this.selectedCompany.companyId;
      this.comapnyModelMappingService.InsertCompanyModelMapping(this.companyModelMapping).subscribe((res: any) => {        
        this.showMessageService.Success("Company Model Mapping", res);
              this.companyModelMapping = {};
              this.ref.close(true);
      }, err => {             
                this.showMessageService.Error("Company Model Mapping Save", 'Error : ' + err.error);  
      });
    }
    catch(error){
      this.showMessageService.Error("Company Model Mapping", 'Error while adding data : ' + error);
    }
  }

}
