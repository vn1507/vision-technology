import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComapnyModelMappingUpdateComponent } from './comapny-model-mapping-update.component';

describe('ComapnyModelMappingUpdateComponent', () => {
  let component: ComapnyModelMappingUpdateComponent;
  let fixture: ComponentFixture<ComapnyModelMappingUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComapnyModelMappingUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComapnyModelMappingUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
