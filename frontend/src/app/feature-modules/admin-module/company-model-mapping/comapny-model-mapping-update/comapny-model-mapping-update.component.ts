import { Component, OnInit } from '@angular/core';
import { ComapnyModelMappingService } from '../shared/comapny-model-mapping.service';
import { IComapnyModelMapping } from '../shared/IComapnyModelMapping';
import { CompanyService } from '../../company/shared/company.service';
import { ICompany } from '../../company/shared/ICompany';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { data } from 'jquery';

@Component({
  selector: 'app-comapny-model-mapping-update',
  templateUrl: './comapny-model-mapping-update.component.html',
  styleUrls: ['./comapny-model-mapping-update.component.scss']
})
export class ComapnyModelMappingUpdateComponent implements OnInit {

  public selectedCompanyModelMapping: IComapnyModelMapping = {};
  public companyList: ICompany[];
  public selectedCompany: ICompany = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, private config: DynamicDialogConfig, 
    private comapnyModelMappingService: ComapnyModelMappingService, private companyService: CompanyService) { }

  ngOnInit(): void {
    this.selectedCompanyModelMapping = this.config.data;
    this.getCompanyNames();
  }

  //method to get company names and ids
  getCompanyNames(){
    try{
      this.companyService.getCompanyList().subscribe((res) => {
        this.companyList = res as ICompany[];
        this.selectedCompany.companyId = this.selectedCompanyModelMapping.companyId;
        this.selectedCompany.companyName = this.selectedCompanyModelMapping.companyName;
      });

    }
    catch(error){
      this.showMessageService.Error("Company Name", 'Error while getting data : ' + error);
    }
  }

  //method to save data
  UpdateCompanyModelMapping(){
    try{
      this.selectedCompanyModelMapping.companyId = this.selectedCompany.companyId;
      this.comapnyModelMappingService.UpdateCompanyModelMapping(this.selectedCompanyModelMapping).subscribe((res: any) => {        
        this.showMessageService.Success("Company Model Mapping", res);
              this.selectedCompanyModelMapping = {};
              this.ref.close(true);
      }, err => {             
                this.showMessageService.Error("Company Model Mapping Update", 'Error : ' + err.error);  
      });
    }
    catch(error){
      this.showMessageService.Error("Company Model Mapping", 'Error while updating data : ' + error);
    }
  }

}
