import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IComapnyModelMapping } from '../shared/IComapnyModelMapping';
import { environment } from '../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ComapnyModelMappingService {

  constructor(private http: HttpClient) { }

  //Method to get response
  getCompanyAndModelNoMappingList(): Observable<IComapnyModelMapping> {
    return this.http.get<IComapnyModelMapping>(environment.webAPIURL + '/api/CompanyAndModelNoMapping/getCompanyAndModelNoMappingList')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to get single model response
  getCompanyAndModelNoMappingByCompanyIdWise(companyID: number): Observable<IComapnyModelMapping> {
    return this.http.get<IComapnyModelMapping>(environment.webAPIURL + '/api/CompanyAndModelNoMapping/getCompanyAndModelNoMappingByCompanyIdWise?CompanyId='+ companyID)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to add
  InsertCompanyModelMapping(companyModelMapping: IComapnyModelMapping): Observable<IComapnyModelMapping> {
    return this.http.post<IComapnyModelMapping>(environment.webAPIURL + '/api/CompanyAndModelNoMapping/postCompanyAndModelNoMapping', companyModelMapping)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to add
  UpdateCompanyModelMapping(selectedCompanyModelMapping: IComapnyModelMapping): Observable<IComapnyModelMapping> {
    return this.http.post<IComapnyModelMapping>(environment.webAPIURL + '/api/CompanyAndModelNoMapping/updateCompanyAndModelNoMapping', selectedCompanyModelMapping)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }


}
