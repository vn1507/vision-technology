import { TestBed } from '@angular/core/testing';

import { ComapnyModelMappingService } from './comapny-model-mapping.service';

describe('ComapnyModelMappingService', () => {
  let service: ComapnyModelMappingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ComapnyModelMappingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
