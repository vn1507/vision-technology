import { Component, OnInit } from '@angular/core';
import { IComapnyModelMapping } from '../shared/IComapnyModelMapping';
import { ComapnyModelMappingService } from '../shared/comapny-model-mapping.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { PrimengService } from '../../../../shared-service/primeng.service';
import { ComapnyModelMappingSaveComponent } from '../comapny-model-mapping-save/comapny-model-mapping-save.component';
import { ComapnyModelMappingUpdateComponent } from '../comapny-model-mapping-update/comapny-model-mapping-update.component';

@Component({
  selector: 'app-comapny-model-mapping-list',
  templateUrl: './comapny-model-mapping-list.component.html',
  styleUrls: ['./comapny-model-mapping-list.component.scss'],
  providers: [PrimengService],
  entryComponents: [ComapnyModelMappingSaveComponent, ComapnyModelMappingUpdateComponent]
})
export class ComapnyModelMappingListComponent implements OnInit {

  public companyModelMappingList: IComapnyModelMapping[];
  public columns: any[];
  public loading: boolean;

  constructor(private showMessageService: ShowMessageService, private primengService: PrimengService, 
    private comapnyModelMappingService: ComapnyModelMappingService) { 
    this.columns = [
      { field: 'modelNo', header: 'Model No.' },
      { field: 'companyName', header: 'Company Name' },
      { field: 'mappingId', header: '' },
    ];
  }

  ngOnInit(): void {
    this.getCompanyAndModelNoMappingList();
  }

  //method to get data
  getCompanyAndModelNoMappingList(){
    try{
      this.loading = true;
      this.comapnyModelMappingService.getCompanyAndModelNoMappingList().subscribe((res : any) => {
        this.companyModelMappingList = res as IComapnyModelMapping[];
        this.loading = false;
      });
    }
    catch(error){
      this.showMessageService.Error("Company Model Mapping", 'Error while getting data : ' + error);
    }
  }

  //method to open add dailog box
  AddDialog() {
    try {
      const ref = this.primengService.dialogService.open(ComapnyModelMappingSaveComponent, {
        header: 'Add Model Mapping',
        width: '25%',
        contentStyle: { "overflow": "visible" },
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getCompanyAndModelNoMappingList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Company Model Mapping", 'Error while opening add dialog : ' + error);
    }
  }

  //method to open update dailog box
  UpdateDialog(companyModelMapping: IComapnyModelMapping) {
    try {      
      let selectedCompanyModelMapping = {
        companyId: companyModelMapping.companyId,
        companyName: companyModelMapping.companyName,
        mappingId: companyModelMapping.mappingId,
        modelNo: companyModelMapping.modelNo,
      };
      const ref = this.primengService.dialogService.open(ComapnyModelMappingUpdateComponent, {
        header: 'Update Model Mapping',
        width: '25%',
        data: selectedCompanyModelMapping,
        contentStyle: { "overflow": "visible" },
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getCompanyAndModelNoMappingList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Company Model Mapping", 'Error while opening update dialog : ' + error);
    }
  }

   //method to open delete dailog box
   DeleteDialog(outwardId: number){

  }

}
