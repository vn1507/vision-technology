import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComapnyModelMappingListComponent } from './comapny-model-mapping-list.component';

describe('ComapnyModelMappingListComponent', () => {
  let component: ComapnyModelMappingListComponent;
  let fixture: ComponentFixture<ComapnyModelMappingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComapnyModelMappingListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComapnyModelMappingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
