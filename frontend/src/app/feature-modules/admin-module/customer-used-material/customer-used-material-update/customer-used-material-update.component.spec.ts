import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerUsedMaterialUpdateComponent } from './customer-used-material-update.component';

describe('CustomerUsedMaterialUpdateComponent', () => {
  let component: CustomerUsedMaterialUpdateComponent;
  let fixture: ComponentFixture<CustomerUsedMaterialUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerUsedMaterialUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerUsedMaterialUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
