import { Component, OnInit } from '@angular/core';
import { ICustomerUsedMaterial } from '../shared/ICustomerUsedMaterial';
import { IMaterialDetailsList } from '../shared/IMaterialDetailsList';
import { CustomerUsedMaterialService } from '../shared/customer-used-material.service';
import { ICustomer } from '../../../customer-module/customer/shared/ICustomer';
import { CustomerService } from '../../../customer-module/customer/shared/customer.service';
import { IEmployee } from '../../../employee-module/employee/shared/IEmployee';
import { EmployeeService } from '../../../employee-module/employee/shared/employee.service';
import { CompanyService } from '../../company/shared/company.service';
import { ICompany } from '../../company/shared/ICompany';
import { ProductService } from '../../product/shared/product.service';
import { IProduct } from '../../product/shared/IProduct';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-customer-used-material-update',
  templateUrl: './customer-used-material-update.component.html',
  styleUrls: ['./customer-used-material-update.component.scss']
})
export class CustomerUsedMaterialUpdateComponent implements OnInit {

  public selectedCustomerUsedMaterial: ICustomerUsedMaterial = {};

  public selectedMaterialList = [];
  public selectedMaterial: IMaterialDetailsList = {};  

  public customerList: ICustomer[];
  public selectedCustomer: ICustomer = {};

  public employeeList: IEmployee[];
  public selectedEmployee: IEmployee = {};

  public companyList: ICompany[];
  public selectedCompany: ICompany = {};

  public productList: IProduct[];
  public selectedProduct: IProduct = {};

  public datePipe = new DatePipe('en-US');

  public disableProduct: boolean = true;

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, private config: DynamicDialogConfig,
    private employeeService: EmployeeService, private companyService: CompanyService,
    private productService: ProductService, private customerService: CustomerService,
    private customerUsedMaterialService: CustomerUsedMaterialService) { }

  ngOnInit(): void {        
    this.selectedCustomerUsedMaterial = this.config.data;
    this.selectedCustomerUsedMaterial.materialUsedDate = new Date(this.datePipe.transform(this.selectedCustomerUsedMaterial.materialUsedDate, 'dd-MMM-yyyy'));
    this.selectedMaterialList = this.selectedCustomerUsedMaterial.customerUsedMaterialDetailsList;
    this.getEmployeeNames();
    this.getCustomerNames();
    this.getCompanyNames();
    this.getProductNames();
    this.selectedMaterial.srNo = this.selectedMaterialList.length + 1;
  }

   //Method to get employee names
   getEmployeeNames() {
    try {
      this.employeeService.getEmployeeData().subscribe((res: any) => {
        this.employeeList = res as IEmployee[];
        this.selectedEmployee.employeeId = this.selectedCustomerUsedMaterial.employeeId;
        this.selectedEmployee.employeeName = this.selectedCustomerUsedMaterial.employeeName;
      });
    }
    catch (error) {
      this.showMessageService.Error("employeeList", 'Error while adding data : ' + error);
    }
  }

  //Method to get customer names
  getCustomerNames() {
    try {
      this.customerService.getCustomerData().subscribe((res: any) => {
        this.customerList = res as ICustomer[];
        this.selectedCustomer.customerId = this.selectedCustomerUsedMaterial.customerId ;
        this.selectedCustomer.customerName = this.selectedCustomerUsedMaterial.customerName;
      });
    }
    catch (error) {
      this.showMessageService.Error("employeeList", 'Error while adding data : ' + error);
    }
  }

  //Method to get company names
  getCompanyNames() {
    try {
      this.companyService.getCompanyList().subscribe((res: any) => {
        this.companyList = res as ICompany[];
        this.selectedCompany.companyId = this.selectedMaterial.companyId;
        this.selectedCompany.companyName = this.selectedMaterial.companyName;
      });
    }
    catch (error) {
      this.showMessageService.Error("getCompanyNames", 'Error while adding data : ' + error);
    }
  }

  //Method to get product names
  getProductNames() {
    try {
      this.productService.getProductData().subscribe((res: any) => {
        this.productList = res as IProduct[];
        this.selectedProduct.productId = this.selectedMaterial.productId;
        this.selectedProduct.productName = this.selectedMaterial.productName;

      });
    }
    catch (error) {
      this.showMessageService.Error("getProductNames", 'Error while adding data : ' + error);
    }
  }

  //method to enable product
  onCompanyChange(companyId: number) {
    if (companyId) {
      this.disableProduct = false;      
    } else {
      this.disableProduct = true;
    }
  }

  //Method to add item details
  addItem() {
    try {        
      this.selectedMaterial.companyId = this.selectedCompany.companyId;
      this.selectedMaterial.companyName = this.selectedCompany.companyName;
      this.selectedMaterial.productId = this.selectedProduct.productId;
      this.selectedMaterial.productName = this.selectedProduct.productName;   
      this.selectedMaterial.qty = this.selectedMaterial.qty;   
      this.selectedMaterialList.push(this.selectedMaterial);      
      this.selectedMaterial = {};
      this.selectedMaterial.srNo = this.selectedMaterialList.length + 1;
    }
    catch (error) {
      this.showMessageService.Error("selectedMaterial", 'Error while adding data : ' + error);
    }
  }

  //Method to remove item
  removeItem(srNo: number) {
    try {
      for (let i = 0; i < this.selectedMaterialList.length; i++) {
        if (this.selectedMaterialList[i]["srNo"] == srNo) {
          this.selectedMaterialList.splice(i, 1);
        }
      }

      for (let i = 0; i < this.selectedMaterialList.length; i++) {
        this.selectedMaterialList[i]["srNo"] = (i + 1);
      }
      this.selectedMaterial.srNo = this.selectedMaterialList.length + 1;

    }
    catch (error) {
      this.showMessageService.Error("removeItem", 'Error while removing data : ' + error);
    }
  }

  // Validation of maxLength for number input field  
  onlyNumberKey(event) {
    if (event.target.value.length == 0 && event.which == 48) {
      return false;
    }
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 46 && event.charCode <= 57;
  }

  //Method to add data
  UpdateCustomerMaterial() {
    try {            
      this.selectedCustomerUsedMaterial.materialUsedDate = this.datePipe.transform(this.selectedCustomerUsedMaterial.materialUsedDate, 'yyyy-MM-dd');
      this.selectedCustomerUsedMaterial.customerId = this.selectedCustomer.customerId;
      this.selectedCustomerUsedMaterial.employeeId = this.selectedEmployee.employeeId;      
      this.selectedCustomerUsedMaterial.customerUsedMaterialDetailsList = this.selectedMaterialList;
      this.customerUsedMaterialService.UpdateCustomerUsedMaterial(this.selectedCustomerUsedMaterial).subscribe((res: any) => {
        this.showMessageService.Success("AddCustomerMaterial", res);
        this.selectedCustomerUsedMaterial = {};
        this.ref.close(true);
      }, err => {
        this.showMessageService.Error("Material", 'Error : ' + err.error);
      });
    }
    catch (error) {
      this.showMessageService.Error("Material", 'Error while updating data : ' + error);
    }
  }


}
