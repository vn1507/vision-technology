import { TestBed } from '@angular/core/testing';

import { CustomerUsedMaterialService } from './customer-used-material.service';

describe('CustomerUsedMaterialService', () => {
  let service: CustomerUsedMaterialService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CustomerUsedMaterialService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
