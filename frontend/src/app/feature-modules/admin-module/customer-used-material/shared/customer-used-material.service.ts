import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICustomerUsedMaterial } from '../shared/ICustomerUsedMaterial';
import { environment } from '../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomerUsedMaterialService {

  constructor(private http: HttpClient) { }

  //Method to get response
  getCustomerUsedMaterialList(): Observable<ICustomerUsedMaterial> {
    return this.http.get<ICustomerUsedMaterial>(environment.webAPIURL + '/api/CustomerUsedMaterial/getCustomerUsedMaterialList')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }  

  //Method to add
  InsertCustomerUsedMaterial(material: ICustomerUsedMaterial): Observable<ICustomerUsedMaterial> {        
    return this.http.post<ICustomerUsedMaterial>(environment.webAPIURL + '/api/CustomerUsedMaterial/postCustomerUsedMaterial', material)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to update
  UpdateCustomerUsedMaterial(selectedMaterial: ICustomerUsedMaterial): Observable<ICustomerUsedMaterial> {    
    return this.http.post<ICustomerUsedMaterial>(environment.webAPIURL + '/api/CustomerUsedMaterial/updateCustomerUsedMaterial', selectedMaterial)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

}
