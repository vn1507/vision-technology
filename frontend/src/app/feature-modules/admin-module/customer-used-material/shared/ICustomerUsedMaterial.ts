export interface ICustomerUsedMaterial {
    materialId?;
    customerId?;
    customerName?;
    employeeId?;
    employeeName?;
    materialUsedDate ?;
    customerUsedMaterialDetailsList ?;
}