export interface IMaterialDetailsList {
    materialId?;
    srNo?;
    companyId?;
    companyName?;
    productId?;
    productName?;
    qty?;    
}