import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerUsedMaterialSaveComponent } from './customer-used-material-save.component';

describe('CustomerUsedMaterialSaveComponent', () => {
  let component: CustomerUsedMaterialSaveComponent;
  let fixture: ComponentFixture<CustomerUsedMaterialSaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerUsedMaterialSaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerUsedMaterialSaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
