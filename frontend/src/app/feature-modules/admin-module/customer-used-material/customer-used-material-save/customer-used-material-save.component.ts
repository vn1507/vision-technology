import { Component, OnInit } from '@angular/core';
import { ICustomerUsedMaterial } from '../shared/ICustomerUsedMaterial';
import { IMaterialDetailsList } from '../shared/IMaterialDetailsList';
import { CustomerUsedMaterialService } from '../shared/customer-used-material.service';
import { ICustomer } from '../../../customer-module/customer/shared/ICustomer';
import { CustomerService } from '../../../customer-module/customer/shared/customer.service';
import { IEmployee } from '../../../employee-module/employee/shared/IEmployee';
import { EmployeeService } from '../../../employee-module/employee/shared/employee.service';
import { CompanyService } from '../../company/shared/company.service';
import { ICompany } from '../../company/shared/ICompany';
import { ProductService } from '../../product/shared/product.service';
import { IProduct } from '../../product/shared/IProduct';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-customer-used-material-save',
  templateUrl: './customer-used-material-save.component.html',
  styleUrls: ['./customer-used-material-save.component.scss']
})
export class CustomerUsedMaterialSaveComponent implements OnInit {

  public customerUsedMaterial: ICustomerUsedMaterial = {};

  public addedMaterial = [];
  public selectedMaterial: IMaterialDetailsList = {};

  public displayList: boolean;

  public customerList: ICustomer[];
  public selectedCustomer: ICustomer = {};

  public employeeList: IEmployee[];
  public selectedEmployee: IEmployee = {};

  public companyList: ICompany[];
  public selectedCompany: ICompany = {};

  public productList: IProduct[];
  public selectedProduct: IProduct = {};

  public datePipe = new DatePipe('en-US');

  public disableProduct: boolean = true;

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef,
    private employeeService: EmployeeService, private companyService: CompanyService,
    private productService: ProductService, private customerService: CustomerService,
    private customerUsedMaterialService: CustomerUsedMaterialService) { }


  ngOnInit(): void {
    this.getEmployeeNames();
    this.getCustomerNames();
    this.getCompanyNames();
    this.getProductNames();
    this.selectedMaterial.srNo = 1;
  }

   //Method to get employee names
   getEmployeeNames() {
    try {
      this.employeeService.getEmployeeData().subscribe((res: any) => {
        this.employeeList = res as IEmployee[];
      });
    }
    catch (error) {
      this.showMessageService.Error("employeeList", 'Error while adding data : ' + error);
    }
  }

  //Method to get customer names
  getCustomerNames() {
    try {
      this.customerService.getCustomerData().subscribe((res: any) => {
        this.customerList = res as ICustomer[];
      });
    }
    catch (error) {
      this.showMessageService.Error("employeeList", 'Error while adding data : ' + error);
    }
  }

  //Method to get company names
  getCompanyNames() {
    try {
      this.companyService.getCompanyList().subscribe((res: any) => {
        this.companyList = res as ICompany[];
      });
    }
    catch (error) {
      this.showMessageService.Error("getCompanyNames", 'Error while adding data : ' + error);
    }
  }

  //Method to get product names
  getProductNames() {
    try {
      this.productService.getProductData().subscribe((res: any) => {
        this.productList = res as IProduct[];
      });
    }
    catch (error) {
      this.showMessageService.Error("getProductNames", 'Error while adding data : ' + error);
    }
  }

  //method to enable product
  onCompanyChange(companyId: number) {
    if (companyId) {
      this.disableProduct = false;      
    } else {
      this.disableProduct = true;
    }
  }

  //Method to add item details
  addItem() {
    try {     
      this.displayList = true;            
      this.selectedMaterial.companyId = this.selectedCompany.companyId;
      this.selectedMaterial.companyName = this.selectedCompany.companyName;
      this.selectedMaterial.productId = this.selectedProduct.productId;
      this.selectedMaterial.productName = this.selectedProduct.productName;   
      this.selectedMaterial.qty = this.selectedMaterial.qty;   
      this.addedMaterial.push(this.selectedMaterial);      
      this.selectedMaterial = {};
      this.selectedMaterial.srNo = this.addedMaterial.length + 1;
    }
    catch (error) {
      this.showMessageService.Error("selectedMaterial", 'Error while adding data : ' + error);
    }
  }

  //Method to remove item
  removeItem(srNo: number) {
    try {
      for (let i = 0; i < this.addedMaterial.length; i++) {
        if (this.addedMaterial[i]["srNo"] == srNo) {
          this.addedMaterial.splice(i, 1);
        }
      }

      for (let i = 0; i < this.addedMaterial.length; i++) {
        this.addedMaterial[i]["srNo"] = (i + 1);
      }
      this.selectedMaterial.srNo = this.addedMaterial.length + 1;

    }
    catch (error) {
      this.showMessageService.Error("removeItem", 'Error while removing data : ' + error);
    }
  }

  // Validation of maxLength for number input field  
  onlyNumberKey(event) {
    if (event.target.value.length == 0 && event.which == 48) {
      return false;
    }
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 46 && event.charCode <= 57;
  }

  //Method to add data
  AddCustomerMaterial() {
    try {            
      this.customerUsedMaterial.materialUsedDate = new Date(this.datePipe.transform(this.customerUsedMaterial.materialUsedDate, 'yyyy-MM-dd'));
      this.customerUsedMaterial.customerId = this.selectedCustomer.customerId;
      this.customerUsedMaterial.employeeId = this.selectedEmployee.employeeId;      
      this.customerUsedMaterial.customerUsedMaterialDetailsList = this.addedMaterial;
      this.customerUsedMaterialService.InsertCustomerUsedMaterial(this.customerUsedMaterial).subscribe((res: any) => {
        this.showMessageService.Success("AddCustomerMaterial", res);
        this.customerUsedMaterial = {};
        this.ref.close(true);
      }, err => {
        this.showMessageService.Error("Material", 'Error : ' + err.error);
      });
    }
    catch (error) {
      this.showMessageService.Error("Material", 'Error while adding data : ' + error);
    }
  }

  

}
