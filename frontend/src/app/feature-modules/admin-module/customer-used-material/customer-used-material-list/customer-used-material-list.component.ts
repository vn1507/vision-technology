import { Component, OnInit } from '@angular/core';
import { ICustomerUsedMaterial } from '../shared/ICustomerUsedMaterial';
import { CustomerUsedMaterialService } from '../shared/customer-used-material.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { PrimengService } from '../../../../shared-service/primeng.service';
import { CustomerUsedMaterialSaveComponent } from '../customer-used-material-save/customer-used-material-save.component';
import { CustomerUsedMaterialUpdateComponent } from '../customer-used-material-update/customer-used-material-update.component';

@Component({
  selector: 'app-customer-used-material-list',
  templateUrl: './customer-used-material-list.component.html',
  styleUrls: ['./customer-used-material-list.component.scss'],  
  providers: [PrimengService],
  entryComponents: [CustomerUsedMaterialSaveComponent, CustomerUsedMaterialUpdateComponent]
})
export class CustomerUsedMaterialListComponent implements OnInit {

  public customerUsedMaterialList: ICustomerUsedMaterial[];
  public columns: any[];
  public loading: boolean;

  constructor(private showMessageService: ShowMessageService, private primengService: PrimengService,
              private customerUsedMaterialService: CustomerUsedMaterialService) { 
    this.columns = [
      { field: 'materialId', header: 'Material ID' },
      { field: 'customerName', header: 'Customer Name' },
      { field: 'materialUsedDate', header: 'Date Time' },
      { field: 'employeeName', header: 'Employee Name' },
      { field: 'materialId', header: '' },
    ];
  }

  ngOnInit(): void {
    this.getCustomerUsedMaterialList();
  }

  //method to get data
  getCustomerUsedMaterialList(){
    try{
      this.loading = true;
      this.customerUsedMaterialService.getCustomerUsedMaterialList().subscribe((res : any) => {
        this.customerUsedMaterialList = res as ICustomerUsedMaterial[];
        this.loading = false;
      });
    }
    catch(error){
      this.showMessageService.Error("Customer Used Material", 'Error while getting data : ' + error);
    }
  }

   //method to open add dailog box
   AddDialog() {
    try {
      const ref = this.primengService.dialogService.open(CustomerUsedMaterialSaveComponent, {
        header: 'Add Customer Used Material',
        width: '50%',
        contentStyle: { "overflow": "visible" },
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getCustomerUsedMaterialList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Customer Used Material", 'Error while opening add dialog : ' + error);
    }
  }

  //method to open update dailog box
  UpdateDialog(material: ICustomerUsedMaterial) {
    try {           
      let selectedMaterial = {
        materialId: material.materialId,        
        employeeId: material.employeeId,
        employeeName: material.employeeName,
        customerId: material.customerId,
        customerName: material.customerName,
        materialUsedDate: material.materialUsedDate,
        customerUsedMaterialDetailsList: material.customerUsedMaterialDetailsList
      };
      const ref = this.primengService.dialogService.open(CustomerUsedMaterialUpdateComponent, {
        header: 'Update Customer Used Material',
        width: '50%',
        data: selectedMaterial,
        contentStyle: { "overflow": "visible" },
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getCustomerUsedMaterialList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Customer Used Material", 'Error while opening update dialog : ' + error);
    }
  }

   //method to open delete dailog box
   DeleteDialog(materialId: number){

  }

}
