import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerUsedMaterialListComponent } from './customer-used-material-list.component';

describe('CustomerUsedMaterialListComponent', () => {
  let component: CustomerUsedMaterialListComponent;
  let fixture: ComponentFixture<CustomerUsedMaterialListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerUsedMaterialListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerUsedMaterialListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
