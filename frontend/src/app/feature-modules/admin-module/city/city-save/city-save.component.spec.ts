import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CitySaveComponent } from './city-save.component';

describe('CitySaveComponent', () => {
  let component: CitySaveComponent;
  let fixture: ComponentFixture<CitySaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CitySaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CitySaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
