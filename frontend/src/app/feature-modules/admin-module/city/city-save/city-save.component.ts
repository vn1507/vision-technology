import { Component, OnInit } from '@angular/core';
import { CityService } from '../shared/city.service';
import { ICity } from '../shared/ICity';
import { DistrictService } from '../../district/shared/district.service';
import { IDistrict } from '../../district/shared/IDistrict';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-city-save',
  templateUrl: './city-save.component.html',
  styleUrls: ['./city-save.component.scss']
})
export class CitySaveComponent implements OnInit {

  public city: ICity = {};

  public districtList: IDistrict[];
  public selectedDistrict: IDistrict = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, 
    private cityService: CityService, private districtService: DistrictService) { }

  ngOnInit(): void {
    this.getDistrictNames();
  }

  //method to fetch countryname
  getDistrictNames(){
    try{
      this.districtService.getDistrictData().subscribe((res: any) => {
        this.districtList = res as IDistrict[];
      });
    }
    catch(error){
      this.showMessageService.Error("District", 'Error while feteching data : ' + error);
    }
  }

  //method to save data
  SaveCity(){
    try{
      this.city.districtId = this.selectedDistrict.districtId;
      this.cityService.InsertCity(this.city).subscribe((res: any) => {
        this.showMessageService.Success("City", res);
              this.city = {};
              this.ref.close(true);
      }, err => {
              if(err.status == 409){
                this.showMessageService.Error("Duplicate City Name" ,  "City name already exists");  
              } else{
                this.showMessageService.Error("City Save", 'Error : ' + err.error);  
              }
      });
    }
    catch(error){
      this.showMessageService.Error("City", 'Error while adding data : ' + error);
    }
  }

}
