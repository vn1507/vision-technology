import { Component, OnInit } from '@angular/core';
import { ICity } from '../shared/ICity';
import { CityService } from '../shared/city.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { PrimengService } from '../../../../shared-service/primeng.service';
import { CitySaveComponent } from '../city-save/city-save.component';
import { CityUpdateComponent } from '../city-update/city-update.component';

@Component({
  selector: 'app-city-list',
  templateUrl: './city-list.component.html',
  styleUrls: ['./city-list.component.scss'],
  providers: [PrimengService],
  entryComponents: [CitySaveComponent, CityUpdateComponent]
})
export class CityListComponent implements OnInit {

  public cityList: ICity[];
  public columns: any[];
  public loading: boolean;

  constructor(private showMessageService: ShowMessageService, private primengService: PrimengService, private cityService: CityService) { 
    this.columns = [
      { field: 'districtName', header: 'District Name' },
      { field: 'cityName', header: 'City Name' },
      { field: 'cityId', header: '' },
    ];
  }

  ngOnInit(): void {
    this.getCityList();
  }

  //method to get data
  getCityList(){
    try{
      this.loading = true;
      this.cityService.getCityList().subscribe((res : any) => {
        this.cityList = res as ICity[];
        this.loading = false;
      });
    }
    catch(error){
      this.showMessageService.Error("State", 'Error while getting data : ' + error);
    }
  }

  //method to open add dailog box
  AddDialog() {
    try {

      const ref = this.primengService.dialogService.open(CitySaveComponent, {
        header: 'Add City',
        width: "20%"
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getCityList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("City", 'Error while opening add dialog City : ' + error);
    }
  }

  //method to open update dailog box
  UpdateDialog(city: ICity) {
    try {
      let selectedCity = {
        cityId: city.cityId,
        cityName: city.cityName,
        districtId: city.districtId,
        districtName: city.districtName,
      };      

      const ref = this.primengService.dialogService.open(CityUpdateComponent, {
        header: 'Update City',
        data: selectedCity,
        width: "20%"
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getCityList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("City", 'Error while opening update dialog City : ' + error);
    }
  }

  //method to open delete dailog box
  DeleteDialog(cityId: number){

  }  

}
