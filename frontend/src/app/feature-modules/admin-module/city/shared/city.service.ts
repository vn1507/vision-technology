import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICity } from '../shared/ICity';
import { environment } from '../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  constructor(private http: HttpClient) { }

  //Method to get response
  getCityList(): Observable<ICity> {
    return this.http.get<ICity>(environment.webAPIURL + '/api/City/getCityList')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

   //Method to get names and id
   getCityData(): Observable<ICity> {
    return this.http.get<ICity>(environment.webAPIURL + '/api/City/getCityData')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to get city name by id
  getCityListByDistrictId(CityId: number): Observable<ICity> {
    return this.http.get<ICity>(environment.webAPIURL + '/api/City/getCityListByDistrictId?=' + CityId)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to add
  InsertCity(city: ICity): Observable<ICity> {
    return this.http.post<ICity>(environment.webAPIURL + '/api/City/postCity', city)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to update
  UpdateCity(selectedCity: ICity): Observable<ICity> {
    return this.http.post<ICity>(environment.webAPIURL + '/api/City/updateCity', selectedCity)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }
}
