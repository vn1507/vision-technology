import { Component, OnInit } from '@angular/core';
import { CityService } from '../shared/city.service';
import { ICity } from '../shared/ICity';
import { DistrictService } from '../../district/shared/district.service';
import { IDistrict } from '../../district/shared/IDistrict';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-city-update',
  templateUrl: './city-update.component.html',
  styleUrls: ['./city-update.component.scss']
})
export class CityUpdateComponent implements OnInit {

  public selectedCity: ICity = {};

  public districtList: IDistrict[];
  public selectedDistrict: IDistrict = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, private config: DynamicDialogConfig, 
    private cityService: CityService, private districtService: DistrictService) { }

  ngOnInit(): void {
    this.selectedCity = this.config.data;
    this.getDistrictNames();
  }

  //method to fetch countryname
  getDistrictNames(){
    try{
      this.districtService.getDistrictData().subscribe((res: any) => {
        this.districtList = res as IDistrict[];
        this.selectedDistrict.districtId = this.selectedCity.districtId;
        this.selectedDistrict.districtName = this.selectedCity.districtName;
      });
    }
    catch(error){
      this.showMessageService.Error("District", 'Error while feteching data : ' + error);
    }
  }

  //method to save data
  UpdateCity(){
    try{
      this.selectedCity.districtId = this.selectedDistrict.districtId;
      this.cityService.UpdateCity(this.selectedCity).subscribe((res: any) => {
        this.showMessageService.Success("City", res);
              this.selectedCity = {};
              this.ref.close(true);
      }, err => {
              if(err.status == 409){
                this.showMessageService.Error("Duplicate City Name" ,  "City name already exists");  
              } else{
                this.showMessageService.Error("City Update", 'Error : ' + err.error);  
              }
      });
    }
    catch(error){
      this.showMessageService.Error("City", 'Error while adding data : ' + error);
    }
  }

}
