import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminMenuComponent } from './admin-menu/admin-menu.component';

import { UnitListComponent } from './unit/unit-list/unit-list.component';
import { ProductListComponent } from './product/product-list/product-list.component';
import { PartyListComponent } from './party/party-list/party-list.component';
import { CompanyListComponent } from './company/company-list/company-list.component';
import { ComapnyModelMappingListComponent } from './company-model-mapping/comapny-model-mapping-list/comapny-model-mapping-list.component';
import { ModemListComponent } from './modem/modem-list/modem-list.component';
import { PlanListComponent } from './plan/plan-list/plan-list.component';
import { OltListComponent } from './olt/olt-list/olt-list.component';
import { SpliterListComponent } from './spliter/spliter-list/spliter-list.component';
import { DesignationListComponent } from './designation/designation-list/designation-list.component';

import { CountryListComponent } from './country/country-list/country-list.component';
import { StateListComponent } from './state/state-list/state-list.component';
import { DistrictListComponent } from './district/district-list/district-list.component';
import { CityListComponent } from './city/city-list/city-list.component';
import { AreaListComponent } from './area/area-list/area-list.component';

import { ItemInwardListComponent } from './item_inward/item-inward-list/item-inward-list.component';
import { ItemOutwardListComponent } from './item_outward/item-outward-list/item-outward-list.component';
import { CustomerUsedMaterialListComponent } from './customer-used-material/customer-used-material-list/customer-used-material-list.component';

const routes: Routes = [
  {
    path: '', component: AdminMenuComponent,
    children: [
      {path: 'unit', component: UnitListComponent},
      {path: 'product', component: ProductListComponent},
      {path: 'party', component: PartyListComponent},
      {path: 'company', component: CompanyListComponent},
      {path: 'modelMapping', component: ComapnyModelMappingListComponent},
      {path: 'modem', component: ModemListComponent},
      {path: 'plan', component: PlanListComponent},
      {path: 'olt', component: OltListComponent},
      {path: 'spliter', component: SpliterListComponent},
      {path: 'designation', component: DesignationListComponent},
      {path: 'country', component: CountryListComponent},
      {path: 'state', component: StateListComponent},
      {path: 'district', component: DistrictListComponent},
      {path: 'city', component: CityListComponent},
      {path: 'area', component: AreaListComponent},
      {path: 'inward', component: ItemInwardListComponent},
      {path: 'outward', component: ItemOutwardListComponent},
      {path: 'customerUsedMaterial', component: CustomerUsedMaterialListComponent},
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
