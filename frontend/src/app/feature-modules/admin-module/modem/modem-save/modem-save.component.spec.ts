import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModemSaveComponent } from './modem-save.component';

describe('ModemSaveComponent', () => {
  let component: ModemSaveComponent;
  let fixture: ComponentFixture<ModemSaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModemSaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModemSaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
