import { Component, OnInit } from '@angular/core';
import { IModem } from '../shared/IModem';
import { ModemService } from '../shared/modem.service';
import { ICompany } from '../../company/shared/ICompany';
import { CompanyService } from '../../company/shared/company.service';
import { IComapnyModelMapping } from '../../company-model-mapping/shared/IComapnyModelMapping';
import { ComapnyModelMappingService } from '../../company-model-mapping/shared/comapny-model-mapping.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-modem-save',
  templateUrl: './modem-save.component.html',
  styleUrls: ['./modem-save.component.scss']
})
export class ModemSaveComponent implements OnInit {

  public modem: IModem = {};

  public companyList : ICompany[];
  public selectedCompany: ICompany = {};

  public modelList : IComapnyModelMapping[];
  public selectedModel: IComapnyModelMapping = {};

  public enableModel: boolean = true;

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef,
    private companyService :CompanyService, private comapnyModelMappingService: ComapnyModelMappingService, 
    private modemService: ModemService) { }

  ngOnInit(): void {
    this.getCompanyNames();     
  }

  //Method to get company names
  getCompanyNames(){
    try{
      this.companyService.getCompanyList().subscribe((res: any) => {
        this.companyList = res as ICompany[];
      });
    }
    catch(error){
      this.showMessageService.Error("getCompanyNames", 'Error while adding data : ' + error);
    }
  }

  //Enable Model method on company select
  onChangeCompanySelect(companyId: number){    
    try{
      if(companyId){
        this.enableModel = false;
        this.comapnyModelMappingService.getCompanyAndModelNoMappingByCompanyIdWise(companyId).subscribe((res: any) => {
          this.modelList = res as IComapnyModelMapping[];          
        });
      }else{
        this.enableModel = true;
        this.modelList = [];
      }
      
    }
    catch(error){
      this.showMessageService.Error("getProductNames", 'Error while adding data : ' + error);
    }
  }  

  //Method to add data
  AddModem(){
    try{     
      this.modem.mappingId = this.selectedModel.mappingId;
      this.modem.companyId = this.selectedCompany.companyId;      
      this.modemService.InsertModem(this.modem).subscribe((res : any) => {
        this.showMessageService.Success("Modem", res);
              this.modem = {};
              this.ref.close(true);
      }, err => {
              if(err.status == 409){
                this.showMessageService.Error("Duplicate PONS No", '' + err.error);  
              }else{
                this.showMessageService.Error("Modem Save", 'Error : ' + err.error);  
              }
      });
    }
    catch(error){
      this.showMessageService.Error("Inward", 'Error while adding data : ' + error);
    }
  }
  

}
