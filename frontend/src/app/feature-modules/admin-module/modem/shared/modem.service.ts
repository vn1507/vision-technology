import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IModem } from '../shared/IModem';
import { environment } from '../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ModemService {

  public PONS_no = [];

  constructor(private http: HttpClient) { }

  //Method to get response
  getModemList(): Observable<IModem> {
    return this.http.get<IModem>(environment.webAPIURL + '/api/ModemDesc/getModemDescList')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to get response
  getModemDescData(): Observable<IModem> {
    return this.http.get<IModem>(environment.webAPIURL + '/api/ModemDesc/getModemDescData')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to add
  InsertModem(modem: IModem): Observable<IModem> {    
    return this.http.post<IModem>(environment.webAPIURL + '/api/ModemDesc/postModemDesc', modem)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

   //Method to update
   UpdateModem(selectedModem: IModem): Observable<IModem> {    
    return this.http.post<IModem>(environment.webAPIURL + '/api/ModemDesc/updateModemDesc', selectedModem)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

}
