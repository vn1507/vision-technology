import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModemListComponent } from './modem-list.component';

describe('ModemListComponent', () => {
  let component: ModemListComponent;
  let fixture: ComponentFixture<ModemListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModemListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModemListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
