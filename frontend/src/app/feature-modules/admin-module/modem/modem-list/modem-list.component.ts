import { Component, OnInit } from '@angular/core';
import { IModem } from '../shared/IModem';
import { ModemService } from '../shared/modem.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { PrimengService } from '../../../../shared-service/primeng.service';
import { ModemSaveComponent } from '../modem-save/modem-save.component';
import { ModemUpdateComponent } from '../modem-update/modem-update.component';

@Component({
  selector: 'app-modem-list',
  templateUrl: './modem-list.component.html',
  styleUrls: ['./modem-list.component.scss'],
  providers:[PrimengService],
  entryComponents:[ModemSaveComponent, ModemUpdateComponent]
})
export class ModemListComponent implements OnInit {

  public modemList: IModem[];
  public columns: any[];
  public loading: boolean;  

  constructor(private showMessageService: ShowMessageService, private primengService: PrimengService, 
    private modemService: ModemService) { 
    this.columns = [
      { field: 'companyName', header: 'Company Name' },
      { field: 'modelNo', header: 'Model No.' },
      { field: 'ipAddress', header: 'IP Address' },
      { field: 'macAddress', header: 'Mac Address ' },
      { field: 'ponSno', header: 'PONS no. ' },
      { field: 'sno', header: 'Sno' },
      { field: 'modemId', header: '' },
    ];    
  }

  ngOnInit(): void {
    this.getModemList();
  }

  //method to get data
  getModemList(){
    try{
      this.loading = true;
      this.modemService.getModemList().subscribe((res : any) => {
        this.modemList = res as IModem[];
        this.loading = false;
        for(let i = 0 ; this.modemList.length > i; i++){          
          this.modemService.PONS_no.push(this.modemList[i].ponSno);          
        }
      });
    }
    catch(error){
      this.showMessageService.Error("Modem", 'Error while getting data : ' + error);
    }
  }  

   //method to open add dailog box
   AddDialog() {
    try {
      const ref = this.primengService.dialogService.open(ModemSaveComponent, {
        header: 'Add Modem',
        width: '35%',
        contentStyle: { "overflow": "visible" },
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getModemList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Inward", 'Error while opening add dialog : ' + error);
    }
  }

  //method to open update dailog box
  UpdateDialog(modem: IModem) {
    try {
      let selectedModem = {
        modemId: modem.modemId,
        companyId: modem.companyId,
        companyName: modem.companyName,
        mappingId: modem.mappingId,
        modelNo: modem.modelNo,
        ipAddress: modem.ipAddress,
        macAddress: modem.macAddress,        
        ponSno: modem.ponSno,
        sno: modem.sno,
        status: modem.status,
      };      

      const ref = this.primengService.dialogService.open(ModemUpdateComponent, {
        header: 'Update Modem',
        width: '35%',
        data: selectedModem
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getModemList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Modem", 'Error while opening add dialog : ' + error);
    }
  }

  //method to open delete dailog box
  DeleteDialog(modemId: number){

  }


}
