import { Component, OnInit } from '@angular/core';
import { IModem } from '../shared/IModem';
import { ModemService } from '../shared/modem.service';
import { ICompany } from '../../company/shared/ICompany';
import { CompanyService } from '../../company/shared/company.service';
import { IComapnyModelMapping } from '../../company-model-mapping/shared/IComapnyModelMapping';
import { ComapnyModelMappingService } from '../../company-model-mapping/shared/comapny-model-mapping.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-modem-update',
  templateUrl: './modem-update.component.html',
  styleUrls: ['./modem-update.component.scss']
})
export class ModemUpdateComponent implements OnInit {

  public selectedModem: IModem = {};

  public companyList: ICompany[];
  public selectedCompany: ICompany = {};

  public modelList: IComapnyModelMapping[];
  public selectedModel: IComapnyModelMapping = {};

  public enableModel: boolean = true;

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef,
    private companyService: CompanyService, private comapnyModelMappingService: ComapnyModelMappingService,
    private modemService: ModemService, private config: DynamicDialogConfig) { }

  ngOnInit(): void {
    this.selectedModem = this.config.data;
    this.getCompanyNames();
    this.onChangeCompanySelect(this.selectedModem.companyId);
  }

  //Method to get company names
  getCompanyNames() {
    try {
      this.companyService.getCompanyList().subscribe((res: any) => {
        this.companyList = res as ICompany[];
        this.selectedCompany.companyId = this.selectedModem.companyId;
        this.selectedCompany.companyName = this.selectedModem.companyName;
      });
    }
    catch (error) {
      this.showMessageService.Error("getCompanyNames", 'Error while adding data : ' + error);
    }
  }

  //Enable Model method on company select
  onChangeCompanySelect(companyId: number) {
    try {
      if (companyId) {
        this.enableModel = false;
        this.comapnyModelMappingService.getCompanyAndModelNoMappingByCompanyIdWise(companyId).subscribe((res: any) => {
          this.modelList = res as IComapnyModelMapping[];
          this.selectedModel.mappingId = this.selectedModem.mappingId;
          this.selectedModel.modelNo = this.selectedModem.modelNo;
        });
      } else {
        this.enableModel = true;
        this.modelList = [];
      }

    }
    catch (error) {
      this.showMessageService.Error("getProductNames", 'Error while adding data : ' + error);
    }
  }
  //Method to update data
  UpdateModem() {
    try {
      if (this.modemService.PONS_no.includes(this.selectedModem.ponSno)) {
        this.showMessageService.Error("Duplicate PONS No", '');
      }
      else {
        this.selectedModem.mappingId = this.selectedModem.mappingId;
        this.selectedModem.companyId = this.selectedCompany.companyId;
        this.modemService.UpdateModem(this.selectedModem).subscribe((res: any) => {
          this.showMessageService.Success("Modem", res);
          this.selectedModem = {};
          this.ref.close(true);
        }, err => {
          this.showMessageService.Error("Modem Update", 'Error : ' + err.error);
        });
      }
    }
    catch (error) {
      this.showMessageService.Error("Modem", 'Error while updating data : ' + error);
    }
  }

}
