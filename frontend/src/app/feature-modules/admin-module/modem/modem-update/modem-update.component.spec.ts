import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModemUpdateComponent } from './modem-update.component';

describe('ModemUpdateComponent', () => {
  let component: ModemUpdateComponent;
  let fixture: ComponentFixture<ModemUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModemUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModemUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
