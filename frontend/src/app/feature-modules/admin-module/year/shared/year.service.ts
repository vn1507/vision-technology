import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IYear } from './IYear';
import { environment } from '../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class YearService {

  constructor(private http: HttpClient) { }

  //get Year List by date
  getYearListByDate(selectedYear: string):Observable<IYear[]>{    
    return this.http.get<IYear[]>(environment.webAPIURL+ '/api/Year/getYearListByDate?yearDate='+ selectedYear)
    .pipe(
      catchError((error: any) => {
        return throwError(error);
      })
    );
  }
 

}
