import { Component, OnInit } from '@angular/core';
import { StateService } from '../shared/state.service';
import { IState } from '../shared/IState';
import { CountryService } from '../../country/shared/country.service';
import { ICountry } from '../../country/shared/ICountry';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-state-save',
  templateUrl: './state-save.component.html',
  styleUrls: ['./state-save.component.scss']
})
export class StateSaveComponent implements OnInit {

  public state: IState = {};

  public countryList: ICountry[];
  public selectedCountry: ICountry = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, 
    private countryService: CountryService, private stateService: StateService) { }

  ngOnInit(): void {
    this.getCountryNames();
  }

  //method to fetch countryname
  getCountryNames(){
    try{
      this.countryService.getCountryList().subscribe((res: any) => {
        this.countryList = res as ICountry[];
      });
    }
    catch(error){
      this.showMessageService.Error("County", 'Error while feteching data : ' + error);
    }
  }

  //method to save data
  SaveState(){
    try{
      this.state.countryId = this.selectedCountry.countryId;
      this.stateService.InsertState(this.state).subscribe((res: any) => {
        this.showMessageService.Success("State", res);
              this.state = {};
              this.ref.close(true);
      }, err => {
              if(err.status == 409){
                this.showMessageService.Error("Duplicate State Name" ,  "State name already exists");  
              } else{
                this.showMessageService.Error("State Save", 'Error : ' + err.error);  
              }
      });
    }
    catch(error){
      this.showMessageService.Error("State", 'Error while adding data : ' + error);
    }
  }

}
