import { Component, OnInit } from '@angular/core';
import { IState } from '../shared/IState';
import { StateService } from '../shared/state.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { PrimengService } from '../../../../shared-service/primeng.service';
import { StateSaveComponent } from '../state-save/state-save.component';
import { StateUpdateComponent } from '../state-update/state-update.component';

@Component({
  selector: 'app-state-list',
  templateUrl: './state-list.component.html',
  styleUrls: ['./state-list.component.scss'],
  providers: [PrimengService],
  entryComponents: [StateSaveComponent, StateUpdateComponent]
})
export class StateListComponent implements OnInit {

  public stateList: IState[];
  public columns: any[];
  public loading: boolean;

  constructor(private showMessageService: ShowMessageService, private primengService: PrimengService, private stateService: StateService) { 
    this.columns = [
      { field: 'countryName', header: 'Country Name' },
      { field: 'stateName', header: 'State Name' },
      { field: 'stateId', header: '' },
    ];
  }

  ngOnInit(): void {
    this.getStateList();
  }

  //method to get data
  getStateList(){
    try{
      this.loading = true;
      this.stateService.getStateList().subscribe((res : any) => {
        this.stateList = res as IState[];
        this.loading = false;
      });
    }
    catch(error){
      this.showMessageService.Error("State", 'Error while getting data : ' + error);
    }
  }

  //method to open add dailog box
  AddDialog() {
    try {

      const ref = this.primengService.dialogService.open(StateSaveComponent, {
        header: 'Add State',
        width: "20%"
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getStateList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("State", 'Error while opening add dialog State : ' + error);
    }
  }

  //method to open update dailog box
  UpdateDialog(state: IState) {
    try {
      let selectedState = {
        stateId: state.stateId,
        stateName: state.stateName,
        countryId: state.countryId,
        countryName: state.countryName
      };      

      const ref = this.primengService.dialogService.open(StateUpdateComponent, {
        header: 'Update State',
        data: selectedState,
        width: "20%"
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getStateList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("State", 'Error while opening update dialog State : ' + error);
    }
  }

  //method to open delete dailog box
  DeleteDialog(stateId: number){

  }  

}
