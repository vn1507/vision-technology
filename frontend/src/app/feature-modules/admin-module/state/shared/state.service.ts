import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IState } from '../shared/IState';
import { environment } from '../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class StateService {

  constructor(private http: HttpClient) { }

  //Method to get response
  getStateList(): Observable<IState> {
    return this.http.get<IState>(environment.webAPIURL + '/api/State/getStateList')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to get state name and id
  getStateData(): Observable<IState> {
    return this.http.get<IState>(environment.webAPIURL + '/api/State/getStateData')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to get state name by id
  getStateListByCountryId(CountryId: number): Observable<IState> {
    return this.http.get<IState>(environment.webAPIURL + '/api/State/getStateListByCountryId?=' + CountryId)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to add
  InsertState(state: IState): Observable<IState> {
    return this.http.post<IState>(environment.webAPIURL + '/api/State/postState', state)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to update
  UpdateState(selectedState: IState): Observable<IState> {
    return this.http.post<IState>(environment.webAPIURL + '/api/State/updateState', selectedState)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

}
