import { Component, OnInit } from '@angular/core';
import { StateService } from '../shared/state.service';
import { IState } from '../shared/IState';
import { CountryService } from '../../country/shared/country.service';
import { ICountry } from '../../country/shared/ICountry';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-state-update',
  templateUrl: './state-update.component.html',
  styleUrls: ['./state-update.component.scss']
})
export class StateUpdateComponent implements OnInit {

  public selectedState: IState = {};

  public countryList: ICountry[];
  public selectedCountry: ICountry = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, private config: DynamicDialogConfig,
    private countryService: CountryService, private stateService: StateService) { }

  ngOnInit(): void {
    this.getCountryNames();
    this.selectedState = this.config.data;
  }

  //method to fetch countryname
  getCountryNames(){
    try{
      this.countryService.getCountryList().subscribe((res: any) => {
        this.countryList = res as ICountry[];
        console.log('this.countryList',this.countryList);
        this.selectedCountry.countryId = this.selectedState.countryId;
        console.log('this.selectedCountry.countryId',this.selectedCountry.countryId);
        this.selectedCountry.countryName = this.selectedState.countryName;
        console.log('this.selectedCountry.countryName',this.selectedCountry.countryName);
      });
    }
    catch(error){
      this.showMessageService.Error("County", 'Error while feteching data : ' + error);
    }
  }

  //method to update data
  UpdateState(){
    try{
      this.selectedState.countryId = this.selectedCountry.countryId;
      this.stateService.UpdateState(this.selectedState).subscribe((res: any) => {
        this.showMessageService.Success("State", res);
              this.selectedState = {};
              this.ref.close(true);
      }, err => {
              if(err.status == 409){
                this.showMessageService.Error("Duplicate State Name" ,  "State name already exists");  
              } else{
                this.showMessageService.Error("State Update", 'Error : ' + err.error);  
              }
      });
    }
    catch(error){
      this.showMessageService.Error("State", 'Error while updating data : ' + error);
    }
  }

}
