import { Component, OnInit } from '@angular/core';
import { CountryService } from '../shared/country.service';
import { ICountry } from '../shared/ICountry';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef,DynamicDialogConfig } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-country-update',
  templateUrl: './country-update.component.html',
  styleUrls: ['./country-update.component.scss']
})
export class CountryUpdateComponent implements OnInit {

  public selectedCountry: ICountry = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, private config: DynamicDialogConfig, private countryService: CountryService) { }

  ngOnInit(): void {
    this.selectedCountry = this.config.data;
  }

  //method to update data
  UpdateCountry(){
    try{      
      this.countryService.UpdateCountry(this.selectedCountry).subscribe((res: any) => {        
        this.showMessageService.Success("Country", res);
              this.selectedCountry = {};
              this.ref.close(true);
      }, err => {
              if(err.status == 409){
                this.showMessageService.Error("Duplicate Country Name" ,  "Country name already exists");  
              } else{
                this.showMessageService.Error("Country Update", 'Error : ' + err.error);  
              }
      });
    }
    catch(error){
      this.showMessageService.Error("Country", 'Error while adding data : ' + error);
    }
  }

}
