import { Component, OnInit } from '@angular/core';
import { ICountry } from '../shared/ICountry';
import { CountryService } from '../shared/country.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { PrimengService } from '../../../../shared-service/primeng.service';
import { CountrySaveComponent } from '../country-save/country-save.component';
import { CountryUpdateComponent } from '../country-update/country-update.component';

@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.scss'],
  providers: [PrimengService],
  entryComponents: [CountrySaveComponent, CountryUpdateComponent]
})
export class CountryListComponent implements OnInit {

  public countryList: ICountry[];
  public columns: any[];
  public loading: boolean;

  constructor(private showMessageService: ShowMessageService, private primengService: PrimengService, private countryService: CountryService) { 
    this.columns = [
      { field: 'countryName', header: 'Country Name' },
      { field: 'countryId', header: '' },
    ];
  }

  ngOnInit(): void {
    this.getCountryList();
  }

  //method to get data
  getCountryList(){
    try{
      this.loading = true;
      this.countryService.getCountryList().subscribe((res : any) => {
        this.countryList = res as ICountry[];
        this.loading = false;
      });
    }
    catch(error){
      this.showMessageService.Error("Country", 'Error while getting data : ' + error);
    }
  }

  //method to open add dailog box
  AddDialog() {
    try {

      const ref = this.primengService.dialogService.open(CountrySaveComponent, {
        header: 'Add Country',
        width: "20%"
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getCountryList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Country", 'Error while opening add dialog Country : ' + error);
    }
  }

  //method to open update dailog box
  UpdateDialog(country: ICountry) {
    try {
      let selectedCountry = {
        countryId: country.countryId,
        countryName: country.countryName
      };      

      const ref = this.primengService.dialogService.open(CountryUpdateComponent, {
        header: 'Update Country',
        data: selectedCountry,
        width: "20%"
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getCountryList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Country", 'Error while opening update dialog Country : ' + error);
    }
  }

  //method to open delete dailog box
  DeleteDialog(countryId: number){

  }

}
