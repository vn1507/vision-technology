import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICountry } from '../shared/ICountry';
import { environment } from '../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor(private http: HttpClient) { }

  //Method to get response
  getCountryList(): Observable<ICountry> {
    return this.http.get<ICountry>(environment.webAPIURL + '/api/Country/getCountryList')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to add
  InsertCountry(country: ICountry): Observable<ICountry> {
    return this.http.post<ICountry>(environment.webAPIURL + '/api/Country/postCountry', country)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to add
  UpdateCountry(selectedCountry: ICountry): Observable<ICountry> {
    return this.http.post<ICountry>(environment.webAPIURL + '/api/Country/updateCountry', selectedCountry)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

}
