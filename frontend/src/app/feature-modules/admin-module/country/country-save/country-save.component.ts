import { Component, OnInit } from '@angular/core';
import { CountryService } from '../shared/country.service';
import { ICountry } from '../shared/ICountry';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-country-save',
  templateUrl: './country-save.component.html',
  styleUrls: ['./country-save.component.scss']
})
export class CountrySaveComponent implements OnInit {

  public country: ICountry = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, private countryService: CountryService) { }

  ngOnInit(): void {
  }

  //method to save data
  SaveCountry(){
    try{      
      this.countryService.InsertCountry(this.country).subscribe((res: any) => {        
        this.showMessageService.Success("Country", res);
              this.country = {};
              this.ref.close(true);
      }, err => {
              if(err.status == 409){
                this.showMessageService.Error("Duplicate Country Name" ,  "Country name already exists");  
              } else{
                this.showMessageService.Error("Country Save", 'Error : ' + err.error);  
              }
      });
    }
    catch(error){
      this.showMessageService.Error("Country", 'Error while adding data : ' + error);
    }
  }

}
