import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountrySaveComponent } from './country-save.component';

describe('CountrySaveComponent', () => {
  let component: CountrySaveComponent;
  let fixture: ComponentFixture<CountrySaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountrySaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountrySaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
