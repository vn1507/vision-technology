import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../shared/company.service';
import { ICompany } from '../shared/ICompany';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-company-save',
  templateUrl: './company-save.component.html',
  styleUrls: ['./company-save.component.scss']
})
export class CompanySaveComponent implements OnInit {

  public company: ICompany = {};

  constructor(private showMessageService: ShowMessageService, private ref: DynamicDialogRef, private companyService: CompanyService) { }

  ngOnInit(): void {
  }

  //method to save data
  SaveCompany(){
    try{
      this.companyService.InsertCompany(this.company).subscribe((res: any) => {        
        this.showMessageService.Success("Company", res);
              this.company = {};
              this.ref.close(true);
      }, err => {             
                this.showMessageService.Error("Company Save", 'Error : ' + err.error);  
      });
    }
    catch(error){
      this.showMessageService.Error("company", 'Error while adding data : ' + error);
    }
  }
}
