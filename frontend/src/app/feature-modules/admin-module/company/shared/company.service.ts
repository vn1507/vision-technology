import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICompany } from '../shared/ICompany';
import { environment } from '../../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private http: HttpClient) { }

  //Method to get response
  getCompanyList(): Observable<ICompany> {
    return this.http.get<ICompany>(environment.webAPIURL + '/api/Company/getCompanyList')
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to add
  InsertCompany(company: ICompany): Observable<ICompany> {
    return this.http.post<ICompany>(environment.webAPIURL + '/api/Company/postCompany', company)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to add
  UpdateCompany(selectedCompany: ICompany): Observable<ICompany> {
    return this.http.post<ICompany>(environment.webAPIURL + '/api/Company/updateCompany', selectedCompany)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

}
