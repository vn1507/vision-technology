import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../shared/company.service';
import { ICompany } from '../shared/ICompany';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-company-update',
  templateUrl: './company-update.component.html',
  styleUrls: ['./company-update.component.scss']
})
export class CompanyUpdateComponent implements OnInit {

  public selectedCompany: ICompany = {};

  constructor(private showMessageService: ShowMessageService, private config: DynamicDialogConfig, 
    private ref: DynamicDialogRef, private companyService: CompanyService) {       
    }

  ngOnInit(): void {
    this.selectedCompany = this.config.data;
  }

  //method to update data
  UpdateCompany(){
    try{
      this.companyService.UpdateCompany(this.selectedCompany).subscribe((res: any) => {
        this.showMessageService.Success("Company", res);
              this.selectedCompany = {};
              this.ref.close(true);
      }, err => {
              this.showMessageService.Error("Company Update", 'Error : ' + err.error);  
      });
    }
    catch(error){
      this.showMessageService.Error("Company", 'Error while Updating data : ' + error);
    }
  }

}
