import { Component, OnInit } from '@angular/core';
import { ICompany } from '../shared/ICompany';
import { CompanyService } from '../shared/company.service';
import { ShowMessageService } from '../../../../shared-service/show-message.service';
import { PrimengService } from '../../../../shared-service/primeng.service';
import { CompanySaveComponent } from '../company-save/company-save.component';
import { CompanyUpdateComponent } from '../company-update/company-update.component';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.scss'],
  providers: [PrimengService],
  entryComponents: [CompanySaveComponent, CompanyUpdateComponent]
})
export class CompanyListComponent implements OnInit {

  public companyList: ICompany[];
  public columns: any[];
  public loading: boolean;

  constructor(private showMessageService: ShowMessageService, private primengService: PrimengService, private companyService: CompanyService) { 
    this.columns = [
      { field: 'companyName', header: 'Company Name' },
      { field: 'companyId', header: '' },
    ];
  }

  ngOnInit(): void {
    this.getCompanyList();
  }

  //method to get data
  getCompanyList(){
    try{
      this.loading = true;
      this.companyService.getCompanyList().subscribe((res : any) => {
        this.companyList = res as ICompany[];
        this.loading = false;
      });
    }
    catch(error){
      this.showMessageService.Error("Company", 'Error while getting data : ' + error);
    }
  }

  //method to open add dailog box
  AddDialog() {
    try {

      const ref = this.primengService.dialogService.open(CompanySaveComponent, {
        header: 'Add Company',
        width: "20%"
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getCompanyList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Company", 'Error while opening add dialog Company : ' + error);
    }
  }

  //method to open update dailog box
  UpdateDialog(company: ICompany) {
    try {
      let selectedCompany = {
        companyId: company.companyId,
        companyName: company.companyName
      };      

      const ref = this.primengService.dialogService.open(CompanyUpdateComponent, {
        header: 'Update Company',
        data: selectedCompany,
        width: "20%"
      });

      ref.onClose.subscribe((status: any) => {
        if (status) {
          this.getCompanyList();
        }
      });
    }
    catch (error) {
      this.showMessageService.Error("Company", 'Error while opening update dialog Company : ' + error);
    }
  }

  //method to open delete dailog box
  DeleteDialog(companyId: number){

  }

}
