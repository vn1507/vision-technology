import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared-module/shared.module';
import { TabMenusComponent } from './tab-menus/tab-menus.component';


@NgModule({
  declarations: [
    TabMenusComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule
  ],
  exports: [
    TabMenusComponent,
  ]
})
export class SharedComponentsModule { }
