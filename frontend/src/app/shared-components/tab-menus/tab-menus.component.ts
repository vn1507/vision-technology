import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tab-menus',
  templateUrl: './tab-menus.component.html',
  styleUrls: ['./tab-menus.component.scss']
})
export class TabMenusComponent implements OnInit {

  @Input('Tabs') TabMenus: any = [];

  constructor() { }

  ngOnInit(): void {
  }

}
