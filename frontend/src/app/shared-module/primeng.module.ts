import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { DialogModule } from 'primeng/dialog';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { ListboxModule } from 'primeng/listbox';
import { CalendarModule } from 'primeng/calendar';
import { MultiSelectModule } from 'primeng/multiselect';
import {RadioButtonModule} from 'primeng/radiobutton';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TableModule,
    ButtonModule,
    ToastModule,
    DialogModule,
    ConfirmDialogModule,
    DropdownModule,
    InputTextModule,
    ListboxModule,
    CalendarModule,
    MultiSelectModule,
    RadioButtonModule
  ],
  exports: [
    TableModule,
    ButtonModule,
    ToastModule,
    DialogModule,
    ConfirmDialogModule,
    DropdownModule,
    InputTextModule,    
    ListboxModule,
    CalendarModule,
    MultiSelectModule,
    RadioButtonModule
  ]
})
export class PrimengModule { }
