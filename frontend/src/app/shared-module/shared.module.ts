import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PrimengModule } from './primeng.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,    
    RouterModule,
    CollapseModule.forRoot(),
    BsDropdownModule.forRoot(),
    FormsModule,
    PrimengModule
  ],
  exports: [
    RouterModule,
    FormsModule,
    PrimengModule,
  ]
})
export class SharedModule { }
