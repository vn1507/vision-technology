import { Component, OnInit } from '@angular/core';
import { LayoutService } from '../shared/layout.service';
import { IUserDetails } from '../../login/shared/IUserDetails';
import { ShowMessageService } from '../../shared-service/show-message.service';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {

  public UserDetails: IUserDetails;

  public menus: any = [];

  constructor(public layoutService: LayoutService, private showMessageService: ShowMessageService) {
    // this.UserDetails = this.layoutService.currentUserDetails;
  }

  ngOnInit(): void {
    //this.FetchMenu();
  }

  // FetchMenu() {
  //   try {
  //     this.layoutService.GetActiveMenus(this.UserDetails.userid)
  //       .subscribe((res: any) => {
  //         this.menus = res as any[];
  //       });
  //   }
  //   catch (error) {
  //     this.showMessageService.Error("FetchMenu", 'Error while getting FetchMenu : ' + error);
  //   }
  // }
}
