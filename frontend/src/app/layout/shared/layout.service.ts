import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IUserDetails } from '../../login/shared/IUserDetails';
import { EncryptDecryptService } from '../../shared-service/encrypt-decrypt.service';
import { environment } from '../../../environments/environment';
import { IUserLog } from '../../login/shared/IUserLog';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LayoutService {

  //public currentUserDetails: IUserDetails;
  //public menu: any[];

  constructor(private http: HttpClient) {
    //this.currentUserDetails = EncryptDecryptService.Decrypt(environment.secretKey, localStorage.getItem('userDetails'));
  }

  // GetActiveMenus(userId: number) {
  //   return this.http.get(environment.webAPIURL + '/api/UserRights/GetModuleNames?userid=' + userId)
  //     .pipe(
  //       catchError((err: any) => {
  //         return throwError(err)
  //       })
  //     );
  // }

  // saveUserLogoutTime(userLog: IUserLog) {
  //   return this.http.put(environment.webAPIURL + '/api/Userlog/UpdateUserlog?userId=' + userLog.User_Id, userLog)
  //     .pipe(
  //       catchError((error: any) => {
  //         return throwError(error);
  //       }));
  // }

  // getUserID() {
  //   return this.currentUserDetails.userid;    
  // }

}
