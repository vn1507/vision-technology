export interface IMenu {
  Menu_id?;
  Module_id?;
  Menuname?;
  Childnodeflag_Type?;
  Menuactiveflag_Type?;
  Interfaces_id?;
  Menutag?;
  Parentmenu_id?;
  Priorityno?;
  MenuiconUrl?;

}
