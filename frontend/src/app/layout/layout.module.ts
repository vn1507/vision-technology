import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared-module/shared.module';
import { HeaderComponent } from './header/header.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { FooterComponent } from './footer/footer.component';
import { LayoutComponent } from './layout.component';



@NgModule({
  declarations: [
    HeaderComponent,
    SideBarComponent,
    FooterComponent,
    LayoutComponent
    ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class LayoutModule { }
