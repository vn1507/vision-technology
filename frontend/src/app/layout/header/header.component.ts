import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { IUserDetails } from '../../login/shared/IUserDetails';
import { LayoutService } from '../shared/layout.service';
import { IUserLog } from '../../login/shared/IUserLog';
import { ShowMessageService } from '../../shared-service/show-message.service';
import { ILogin } from 'src/app/login/shared/ilogin';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

  @Input('menuName') moduleName: string = '';

  // public UserDetails: IUserDetails;
  // public UserName: any;

  constructor(private layoutService: LayoutService, private router: Router, private showMessageService: ShowMessageService) {
    //this.UserDetails = this.layoutService.currentUserDetails;
  }

  ngOnInit(): void {
    console.log('moduleName ', this.moduleName);
    //this.UserName = this.UserDetails.username;
  }

  logout() {
    try {
      // let userLog: IUserLog = {};
      // userLog.User_Id = this.UserDetails.userid;
      // userLog.Logintime = this.UserDetails.logintime;
      // userLog.Logout_Type = 0;
      // this.layoutService.saveUserLogoutTime(userLog).subscribe(res => {
      //   localStorage.clear();
      //   this.router.navigate(['']);
      // });

      // let userLog: ILogin = {};
      // userLog.UserName = this.UserDetails.UserName;
      // userLog.Password = this.UserDetails.Password;

      localStorage.clear();
      this.router.navigate(['']);

    }
    catch (error) {
      this.showMessageService.Error("User Log", 'Error while saving userLogutTime : ' + error);
    }
  }


}
