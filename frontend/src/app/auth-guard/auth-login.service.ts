import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ILogin } from '../login/shared/ilogin';
//import { IUserLog } from '../login/shared/IUserLog';
import { environment } from '../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthLoginService {

  //public UIPrivileges: any = [];

  constructor(private http: HttpClient) { }

  //Method to get response as a token
  userLogin(user: ILogin): Observable<ILogin> {    
    //let currentUser = "grant_type=password" + "&username=" + user.UserName + "&password=" + user.Password;
    return this.http.post<ILogin>(environment.webAPIURL + '/api/Token/createToken', user)
      .pipe(
        catchError((error: any) => {
          return throwError(error);
        })
      );
  }

  //Method to return token in boolean format
  getAccessToken() {
    return !!localStorage.getItem('token');
  }

  //Method to get token
  getHeadersToken() {
    return localStorage.getItem('token');
  }

  //Method to insert user details in userlog table
  // InsertUserlog(userlog: IUserLog): Observable<IUserLog> {    
  //   return this.http.post<IUserLog>(environment.webAPIURL + '/api/Userlog/InsertUserlog', userlog)
  //     .pipe(
  //       catchError((err: any) => {
  //         return throwError(err);
  //       })
  //     );
  // }

}
