import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http';
import { AuthLoginService } from './auth-login.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor(private injector: Injector) { }

  intercept(req, next) {
    let headerToken = this.injector.get(AuthLoginService);
    
    let token = req.clone({
      setHeaders: {
        Authorization: `Bearer ${headerToken.getHeadersToken()}`
      }
    });
    return next.handle(token);
  }

}
