import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthLoginService } from './auth-login.service';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {

  constructor(private router: Router, private authLoginService: AuthLoginService) { }

  canActivate(): boolean {
    if (this.authLoginService.getAccessToken()) {
      return true;
    }
    else {
      this.router.navigate(['']);
      return false;
    }
  }
  
}
