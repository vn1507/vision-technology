import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LayoutComponent } from './layout/layout.component';
import { AuthGuard } from './auth-guard/auth.guard';

const routes: Routes = [
  { path: '', component: LoginComponent },
  {
    path: 'layout', component: LayoutComponent, canActivate: [AuthGuard], children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./feature-modules/dashboard-module/dashboard.module').then(m => m.DashboardModule),
        canActivate: [AuthGuard]
      },
      {
        path: 'admin',
        loadChildren: () => import('./feature-modules/admin-module/admin.module').then(m => m.AdminModule),
        canActivate: [AuthGuard]
      },
      {
        path: 'customers',
        loadChildren: () => import('./feature-modules/customer-module/customer.module').then(m => m.CustomerModule),
        canActivate: [AuthGuard]
      },
      {
        path: 'employees',
        loadChildren: () => import('./feature-modules/employee-module/employee.module').then(m => m.EmployeeModule),
        canActivate: [AuthGuard]
      },
      {
        path: 'reports',
        loadChildren: () => import('./feature-modules/report-module/report.module').then(m => m.ReportModule),
        canActivate: [AuthGuard]
      },
    ]
  },
];


@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
