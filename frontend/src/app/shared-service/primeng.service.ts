import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
import { DialogService} from 'primeng/dynamicdialog';

@Injectable({
  providedIn: 'root'
})
export class PrimengService {

  constructor(public confirmationService: ConfirmationService, public dialogService: DialogService,
    public messageService: MessageService) { }  
}
