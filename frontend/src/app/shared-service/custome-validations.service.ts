import { Injectable } from '@angular/core';
import 'jquery';

@Injectable({
  providedIn: 'root'
})

export class CustomeValidationsService {

  constructor() { }

  //Below function for tooltip validation
  emailValidation(val, lblName: string, LblId: string, isReqFlag: number) {
    
    console.log("val=", val);
    $(document).ready(function () {
      $('#' + val).on('focusout', function () {
        if ($('#' + val).val() != "") {
          if (validateEmail($('#' + val).val())) {
            $('#' + LblId).fadeOut('slow');
          } else {
            $('#' + LblId).text('Invalid Email...!');
            $('#' + LblId).fadeIn('slow');
          }
        } else {
          if (isReqFlag == 1) {
            $('#' + LblId).text(lblName + ' is required..!');
            $('#' + LblId).fadeIn("slow");//
          }
        }
      });


    });

    function validateEmail(eVal) {
      let val = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
      if (val.test(eVal)) {
        return true;
      } else {
        return false;
      }
    }


  }

  mobileValidation(val, lblName: string, LblId: string, isReqFlag: number) {
    
    $(document).ready(function () {
      $('#' + val).on('focusout', function () {
        if ($('#' + val).val() != "") {
          if (validateMobile($('#' + val).val())) {
            $('#' + LblId).fadeOut('slow');
          } else {
            $('#' + LblId).text('Invalid Number...!');
            $('#' + LblId).fadeIn('slow');
          }
        } else {
          if (isReqFlag == 1) {
            $('#' + LblId).text(lblName + '. Required..!');
            $('#' + LblId).fadeIn("slow");
          }

        }
      });

    })
    function validateMobile(eVal) {
      let val = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
      if (val.test(eVal)) {
        return true;
      } else {
        return false;
      }
    }

  }

  requiredValidation(req, lblName: string, reqLblId: string, isReqFlag: number) {
    
    $(document).ready(function () {
      $('#' + req).on('focusout', function () {
        if ($('#' + req).val() != "") {
          $('#' + reqLblId).fadeOut('slow');
          return true;

        } else {
          if (isReqFlag == 1) {
            $('#' + reqLblId).text(lblName + ' is Required..!');
            $('#' + reqLblId).fadeIn("slow");
          }
        }
      });
    });
  }

  dropDownValidation(req, hidreq:string, lblName: string, reqLblId: string, isReqFlag: number) {

    $('#' + req).on('focusout', function () {
      if ($('#' + hidreq).val() != "") {
        $('#' + reqLblId).fadeOut('slow');
        return true;

      } else {
        if (isReqFlag == 1) {
          $('#' + reqLblId).text(lblName + ' is Required..!');
          $('#' + reqLblId).fadeIn("slow");
        }
      }
    });
 }

  gstinValidation(val, lblName: string, LblId: string, isReqFlag: number) {

    $(document).ready(function () {
      $('#' + val).on('focusout', function () {
        if ($('#' + val).val() != "") {
          if (validateGSTN($('#' + val).val())) {
            $('#' + LblId).fadeOut('slow');
          } else {
            $('#' + LblId).text('Invalid Number...!');
            $('#' + LblId).fadeIn('slow');
          }
        } else {
          if (isReqFlag == 1) {
            $('#' + LblId).text(lblName + '. Required..!');
            $('#' + LblId).fadeIn("slow");
          }

        }
      });

    })
    function validateGSTN(eVal) {
      let val  = /^([0]{1}[1-9]{1}|[1-2]{1}[0-9]{1}|[3]{1}[0-7]{1})([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$/;
      if (val.test(eVal)) {
        return true;
      } else {
        return false;
      }
    }

  }


  requiredValueCalendar(reqVal, lblName: string, reqLblId: string, isReqFlag: number) {
    $(document).ready(function () {
      if (reqVal != "") {
        $('#' + reqLblId).fadeOut('slow');
        return true;

      } else {
        if (isReqFlag == 1) {
          $('#' + reqLblId).text(lblName + ' is Required..!');
          $('#' + reqLblId).fadeIn("slow");
        }
      };
    });
  }


  requiredValidationCalendar(reqVal, req, lblName: string, reqLblId: string, isReqFlag: number) {

    $(document).ready(function () {
      $('#' + req).on('focusout', function () {
        if (reqVal != "") {
          $('#' + reqLblId).fadeOut('slow');
          return true;

        } else {
          if (isReqFlag == 1) {
            $('#' + reqLblId).text(lblName + ' is Required..!');
            $('#' + reqLblId).fadeIn("slow");
          }
        }
      });
    });
  }
 
}
