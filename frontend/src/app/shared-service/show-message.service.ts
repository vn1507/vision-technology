import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { PrimengService } from './primeng.service';
import { AuthLoginService } from '../auth-guard/auth-login.service';
//import { IApperrorlog } from '../feature-modules/admin-module/app-error-log/shared/IApperrorlog';
import { EncryptDecryptService } from './encrypt-decrypt.service';
import { environment } from '../../environments/environment';
//import { IUserDetails } from '../login/shared/iuser-details'
import { LocalStorageDataService } from './local-storage-data.service';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root',

})
export class ShowMessageService {
  userId: number = null;

  constructor(private router: Router, private loginService: AuthLoginService, private http: HttpClient, private primengService: PrimengService) {
    //try {
    //  let currentUserDetails: IUserDetails = EncryptDecryptService.Decrypt(environment.secretKey, localStorage.getItem('userDetails'));
    //  this.userId = (currentUserDetails != null) ? currentUserDetails.userid : this.userId;
    //}
    //catch (error) {
    //  this.userId = 0;
    //}
  }

  Success(interfaceName: string, message: string) {
    this.primengService.messageService.add({ severity: 'success', summary: interfaceName, detail: message });
  }

  Info(interfaceName: string, message: string) {
    this.primengService.messageService.add({ severity: 'info', summary: interfaceName, detail: message });
  }

  Warn(interfaceName: string, message: string) {
    this.primengService.messageService.add({ severity: 'warn', summary: interfaceName, detail: message });
  }

  DBError(interfaceName: string, message: string) {
    this.primengService.messageService.add({ severity: 'error', summary: interfaceName, detail: message });
  }

  Error(interfaceName: string, message: string) {

    this.primengService.messageService.add({ severity: 'error', summary: interfaceName, detail: message });

    //let apperrorlog: IApperrorlog = {};
    //apperrorlog.appname = interfaceName;
    //apperrorlog.custommessage = "";
    //apperrorlog.errorcode = message;
    //apperrorlog.errormessage = message;
    //apperrorlog.custommessage = message;
    //apperrorlog.user_id = this.userId;
    //apperrorlog.session_id = this.userId;


    //this.InsertApperrorlog(apperrorlog)
    //  .subscribe(resp => {

    //    switch (resp) {
    //      case 'RECALL':
    //        this.Error(interfaceName, message);
    //        break;
    //      case 'LOGOUT':

    //        this.router.navigate(['']);
    //        break;
    //      case '':
    //      default:
    //        //no need to display in toastr if want then display it.
    //        break;
    //    }


      //});

  }


  //InsertApperrorlog(apperrorlog: IApperrorlog): Observable<IApperrorlog> {
  //  return this.http.post<IApperrorlog>(environment.webAPIURL + '/api/apperrorlog/InsertApperrorlog', apperrorlog, { headers: LocalStorageDataService.GetHttpHeaderWithAccessToken() })
  //    .pipe(
  //      catchError((error: any) => {
  //        return throwError(error);
  //      }));
  //}
   
  async  GetError(source: string, error: any) {

    let errorMessage: string;
    switch (error.status) {
      case 400:
        errorMessage = 'Invalid username or password';
        break;
      case 401:

        //await this.RefreshToken().then((message: string) => {
        //  errorMessage = message;
        //});

        break;
      case 403:
        errorMessage = 'Application not registered';
        break;
      case 404:
        errorMessage = 'Unable to connect to server, please contact admin';
        break;
      case 405:
        errorMessage = 'Method Not Allowed/The resource doesnt support the specified HTTP verb';
        break;
      case 409:
        errorMessage = 'Conflict';
        break;
      case 411:
        errorMessage = 'The Content-Length header was not specified.';
        break;
      case 412:
        errorMessage = 'Precondition failed.';
        break;
      case 429:
        errorMessage = 'Too many request for rate limiting.';
        break;
      case 503:
        errorMessage = 'Service Unavailable';
        break;
      case 500:
      default:
        errorMessage = error.error;
        break;
    }

    if (errorMessage != 'RECALL' && errorMessage != 'LOGOUT') {
      this.DBError(source, 'Error : ' + errorMessage);
      errorMessage = '';
    }

    return errorMessage;

  }


  //async RefreshToken() {
  //  let message: string;

  //  try {

  //    await this.loginService.getAccessToken()
  //      .subscribe((data: any) => {
  //        this.SaveUserToken(data.access_token, data.refresh_token);
  //        message = 'RECALL';
  //      },
  //        error => {
  //          message = 'LOGOUT';
  //          // this.router.navigate(['']);
  //        })

  //    return message;

  //  }
  //  catch (error) {
  //    this.Error("Login", 'Error while login : ' + error);
  //  }
  //}


  SaveUserToken(accessToken: any, refreshToken: any) {
    try {

      let encryptedToken = EncryptDecryptService.Encrypt(environment.secretKey, accessToken);
      localStorage.setItem('userToken', encryptedToken);

      let encryptedrefreshToken = EncryptDecryptService.Encrypt(environment.secretKey, refreshToken);
      localStorage.setItem('refreshToken', encryptedrefreshToken);

    }
    catch (error) {
      this.Error("Login", 'Error while setting token : ' + error);
    }
  }


}
