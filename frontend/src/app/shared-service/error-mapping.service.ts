import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../login/shared/login.service';
import { ShowMessageService } from './show-message.service';
import { EncryptDecryptService } from './encrypt-decrypt.service';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ErrorMappingService {

  constructor(private router: Router, private loginService: LoginService, private showMessageService: ShowMessageService) { }

   async  GetError(source: string, error: any) {
 debugger
    let errorMessage: string;
    // console.log('GetError: ', error);

    switch (error.status) {
      case 400:
        errorMessage = 'Invalid username or password';
        break;
      case 401:

          await this.RefreshToken().then((message: string) => {
          errorMessage = message;
        });
        
        break;
      case 403:
        errorMessage = 'Application not registered';
        break;
      case 404:
        errorMessage = 'Unable to connect to server, please contact admin';
        break;
      case 405:
        errorMessage = 'Method Not Allowed/The resource doesnt support the specified HTTP verb';
        break;
      case 409:
        errorMessage = 'Conflict';
        break;
      case 411:
        errorMessage = 'The Content-Length header was not specified.';
        break;
      case 412:
        errorMessage = 'Precondition failed.';
        break;
      case 429:
        errorMessage = 'Too many request for rate limiting.';
        break;
      case 503:
        errorMessage = 'Service Unavailable';
        break;
      case 500:
        errorMessage = 'Internal Server Error';
      default:
        errorMessage = error.error;
        break;
     }
     
    // console.log('errorMessage: ', errorMessage);

     //if error message is not recall- means token is expired and taken new token using refresh token then recall same function foe which error was occured
     //and if errormessage is not logout - means refresh token also expired then forcefully logout.
     if (errorMessage != 'RECALL' && errorMessage != 'LOGOUT') {
       this.showMessageService.DBError(source, 'Error : ' + errorMessage);
       errorMessage = '';
     }

    return errorMessage;

  }


  async RefreshToken() {
    let message: string;

    try {

      await this.loginService.RefreshToken()
        .then((data: any) => {

          this.SaveUserToken(data.access_token, data.refresh_token);
          message = 'RECALL';
        },
          error => {
            message = 'LOGOUT';
           // this.router.navigate(['']);
          })

      return message;

    }
    catch (error) {
      this.showMessageService.Error("Login", 'Error while login : ' + error);
    }
  }


  SaveUserToken(accessToken: any, refreshToken: any) {
    try {

     // console.log('refreshed userToken :', accessToken);
      let encryptedToken = EncryptDecryptService.Encrypt(environment.secretKey, accessToken);
      localStorage.setItem('userToken', encryptedToken);

      let encryptedrefreshToken = EncryptDecryptService.Encrypt(environment.secretKey, refreshToken);
      localStorage.setItem('refreshToken', encryptedrefreshToken);

    }
    catch (error) {
      this.showMessageService.Error("Login", 'Error while setting token : ' + error);
    }
  }


}
