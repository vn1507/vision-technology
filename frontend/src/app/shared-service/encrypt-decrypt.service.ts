import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class EncryptDecryptService {

  constructor() { }
 
  //The set method is use for encrypt the value. 
  static Encrypt(encryptSecretKey: any, value: any) {
    try {
      return CryptoJS.AES.encrypt(JSON.stringify(value), encryptSecretKey).toString();
    } catch (e) {
      
    }
    
  }

  //The get method is use for decrypt the value.
  static Decrypt(encryptSecretKey, value) {
    try {
      const bytes = CryptoJS.AES.decrypt(value, encryptSecretKey);

      if (bytes.toString()) {
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      }
    } catch (e) {
      
    }
   

  }
}
